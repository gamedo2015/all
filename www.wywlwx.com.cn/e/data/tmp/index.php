<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>北京网乐无限科技有限公司</title>
<meta name="keywords" content="北京网乐无限科技有限公司,网乐无限,gamedo,掌上纵横,游戏IP" />
<meta name="description" content="北京网乐无限科技有限公司是一家移动互联网和电子游戏多个行业的高科技创新企业，专注于影视及娱乐明星版权运营，版权游戏研发和发行等业务。" />

<link type="text/css" rel="stylesheet" href="/style/style.css" />
<link type="text/css" rel="stylesheet" href="/style/public.css" />
<link type="text/css" rel="stylesheet" href="/style/flash.css" />
<script src="/js/jquery-1.10.2.js"></script>
<script src="/js/jcarousellite.js"></script>
<script src="/js/showpre.js"></script>
 <script type="text/javascript" src="/js/flipmenu-min.js"></script>
<!--[if IE 6]>
<script type="text/javascript" src="/js/DD_belatedPNG.js" ></script>
<script type="text/javascript">
DD_belatedPNG.fix('div, ul, img, li, input, a,.home,.import_link,user_plate,.mask,.bnet,.account');
</script>
<![endif]-->
<script src="/js/nav.js"></script>
<script>

	$(document).ready(function(){
	
	$('.upimg a').mouseover(function(){
	$(this).stop().animate({"top":"-57px"}, 200); 
	})
	$('.upimg a').mouseout(function(){
		$(this).stop().animate({"top":"0"}, 200); 
	})
	
	
	$('.gamelist dt').mouseover(function(){
	$(this).stop().animate({"bottom":"5px"}, 200);
	})
	$('.gamelist dt').mouseout(function(){
		$(this).stop().animate({"bottom":"0"}, 200); 
	})
	
	
     var menu1 = new Flipmenu("flip_menu1");
     var menu2 = new Flipmenu("flip_menu2");
	 

	//游戏展示
	$('.gamebox').jCarouselLite({
		btnNext: ".left_btn",
		btnPrev: ".right_btn",
		auto: 1000,
        speed: 800,
		visible: 6
	})
		
		
		
		
	})
</script>
</head>
<body>
<div class="head">
  <div class="w1005">
    <div class="logo"><a href="/"><img src="/images/logo.png" /></a></div>
    <div class="nav">
      <ul id="flip_menu2">
        <li class="cur"><a href="/">首 页</a></li>
        <li><a href="/news/">最新消息</a></li>
        <li><a href="/product/">游 戏</a></li>
        <li><a href="/about/aboutus.html">关于我们</a></li>
        <li><a href="/about/contactus.html">客服中心</a></li>
        <div class="clear"></div>
      </ul>
       <div class="curBg"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<!--/head-->
<div class="main w1005 mt30">
  <div class="m_l">
    <div class="flash">
      <!-- 代码 开始 -->
      <div class="banner_index"> <a href="javascript:void(0);" class="btn btnPre" id="banner_index_pre"></a> <a href="javascript:void(0);" class="btn btnNext" id="banner_index_next"></a>
        <ul class="banner_wrap" id="banner_index">
		<?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq(2,10,0,1);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?>
          <li><a href="<?=$bqsr[titleurl]?>" target="_blank"><img src="<?=$bqr[titlepic]?> "/></a></li>
		  <?php
}
}
?>		  
        </ul>
        <div class="indexBanner_num" id="index_numIco"></div>
      </div>
      <script type="text/javascript">
var ShowPre1 = new ShowPre({box:"banner_index",Pre:"banner_index_pre",Next:"banner_index_next",numIco:"index_numIco",loop:1,auto:1});
</script>
      <!-- 代码 结束 -->
    </div>
    <!--/flash-->
    <div class="gameshow mt15">
      <h1><a href="/product/">游戏展示</a></h1>
      <div class="btn left_btn"></div>
      <div class="btn right_btn"></div>
      <div class="gamebox">
        <ul>
		<?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq(4,15,0,1);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?>
          <li><a href="<?=$bqsr[titleurl]?>" title="<?=$bqr[title]?>"><img src="<?=$bqr[titlepic]?>" height="80" width="80" /></a></li>
         <?php
}
}
?>	
	   
        </ul>
      </div>
    </div>
    <!--/gameshow-->
    <div class="abouts mt15"> <a href="/joinus/" target="_blank"><img src="/images/about_pic.jpg" border="0"/></a>
      <p> 北京网乐成立于2010年5月，旗下包括3家开发公司和1家版权运营公司，主要人员均来自Gameloft，昆仑，趣游PPTV等知名公司。我们秉承“好游戏，gamedo！”的口号，致力于打造玩家喜爱的手机影视游戏。主营业务有影视版权运营、影视页游、手机网游、单机的发行、影视衍生产品的制作与发行，公司致力于打造全国领先的
影视版权游戏发行商…</p>
    <a class="more" href="/about/aboutus.html"></a> </div>
    <!--/abouts-->
  </div>
  <!--/m_l-->
  <div class="m_r">
    

<div style="margin-bottom:8px;"><a href="/jianhu/" target="_blank"><img src="/images/jianhu.png"></a></div>
<div class="login_box"><ul><li class="reg_btn"><a href="/e/member/register/ChangeRegister.php"></a></li><li class="login_btn"><a href="/e/member/login/"></a></li></ul></div>
<div class="gamelist">
      <h1><a href="/video/">游戏集锦</a></h1>
      <ul>
	  <?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq(5,6,0,1);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?>
        <li>
          <dl>
            <dd><a href="<?=$bqsr[titleurl]?>"><img src="<?=$bqr[titlepic]?>" height="90" width="120" /></a></dd>
            <dt><a href="<?=$bqsr[titleurl]?>"><?=esub($bqr[title],12,'…')?></a></dt>
          </dl>
        </li>
		 <?php
}
}
?>	
      
        <div class="clear"></div>
      </ul>
	  <div class="clear"></div>
</div>
    <!--/gamelist-->
    <div class="message mt15">
	
      <h1><a href="/news/">最新消息</a></h1>
	  
      <ul>
	  
	  <?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq(1,5,0,0);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?>
        <li><span><?=date('Y-m-d',$bqr[newstime])?></span><br />
          <a href="<?=$bqsr[titleurl]?>"><?=$bqr[title]?> </a></li>
		    <?php
}
}
?>	
     
      </ul>
    </div>
    <!--/message-->
  </div>
  <!--/m_r-->
  <div class="clear"></div>
</div>
<!--/main-->
<div class="partner w1005 mt15">
  <h1>合作伙伴</h1>
  <ul class="upimg">
  
    	<?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq(3,14,0,1);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?>
    <li><a href="<?=$bqsr[titleurl]?>" target="_blank"><img src="<?=$bqr[pic1]?> " height="55" width="130" /><img src="<?=$bqr[titlepic]?> " height="55" width="130" /></a></li>
      <?php
}
}
?>	
 
    <div class="clear"></div>
  </ul>
</div>
<!--/partner-->
<div class="footer mt30">
  <div class="w1005">
    <p><a href="/about/aboutus.html">关于我们</a>|<a href="/joinus/">加入我们</a>|<a href="/team.html">团队介绍</a></p>
	<p style="padding-left:15px;">版权所有：北京网乐无限科技有限公司&nbsp;|&nbsp;2011-2014 wywlwx,Ltd. All rights Reserved.</p>
<p style="padding-left:15px;">地址：北京市朝阳区东三环中路39号建外SOHO西区17号楼805室 | 电话：(010)59005861 | 京ICP备12007609号</p>
<p style="padding-left:15px;">健康游戏忠告：抵制不良游戏 拒绝盗版游戏 注意自我保护 谨防受骗上当 适度游戏益脑 沉迷游戏伤身 合理安排时间 享受健康生活.</p>
  </div>
</div>






<!--/footer-->

</body>
</html>
