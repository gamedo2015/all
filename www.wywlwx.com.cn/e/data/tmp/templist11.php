<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>[!--pagetitle--]</title>
<meta name="keywords" content="[!--class.keywords--]" />
<meta name="description" content="[!--class.intro--]" />

<link type="text/css" rel="stylesheet" href="/style/style.css" />
<link type="text/css" rel="stylesheet" href="/style/public.css" />
<link type="text/css" rel="stylesheet" href="/style/list.css" />
<script src="/js/jquery-1.10.2.js"></script>
<script src="/js/nav.js"></script>
<script type="text/javascript" src="/js/flipmenu-min.js"></script>
<script>

	$(document).ready(function(){
	
	$('.upimg a').mouseover(function(){
	$(this).stop().animate({"top":"-57px"}, 200); 
	})
	$('.upimg a').mouseout(function(){
		$(this).stop().animate({"top":"0"}, 200); 
	})
	
	
	$('.gamelist dt').mouseover(function(){
	$(this).stop().animate({"bottom":"5px"}, 200);
	})
	$('.gamelist dt').mouseout(function(){
		$(this).stop().animate({"bottom":"0"}, 200); 
	})
	
	
     var menu1 = new Flipmenu("flip_menu1");
     var menu2 = new Flipmenu("flip_menu2");
	 
	})
</script>
</head>
<body>
<div class="head">
  <div class="w1005">
    <div class="logo"><a href="/"><img src="/images/logo.png" /></a></div>
    <div class="nav">
      <ul id="flip_menu2">
        <li class="cur"><a href="/">首 页</a></li>
        <li><a href="/news/">最新消息</a></li>
        <li><a href="/product/">游 戏</a></li>
        <li><a href="/about/aboutus.html">关于我们</a></li>
        <li><a href="/about/contactus.html">客服中心</a></li>
        <div class="clear"></div>
      </ul>
       <div class="curBg"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<!--/head-->
<div class="main w1005 mt30">
  <div class="m_l">
    <div class="content">
      <h1>[!--class.name--]</h1>
      <div class="join_us">
        <div class="joblist">
          <h2><span class="job_name">职位名称</span><span class="job_place">工作地点</span><span class="branch">部门</span><span class="pubdate">发布时间</span>
            <div class="clear"></div>
          </h2>
          <ul>
		  
		[!--empirenews.listtemp--]
        <!--list.var1-->
        [!--empirenews.listtemp--]  
          </ul>
        </div>
        <!--/joblist-->
        <div class="pagelist"> [!--show.listpage--] </div>
        <!-- /pagelist-->
      </div>
      <!--/join_us-->
    </div>
  </div>
  <!--/m_l-->
  <div class="m_r">
    

<div style="margin-bottom:8px;"><a href="/jianhu/" target="_blank"><img src="/images/jianhu.png"></a></div>
<div class="login_box"><ul><li class="reg_btn"><a href="/e/member/register/ChangeRegister.php"></a></li><li class="login_btn"><a href="/e/member/login/"></a></li></ul></div>
<div class="gamelist">
      <h1><a href="/video/">游戏集锦</a></h1>
      <ul>
	  <?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq(5,6,0,1);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?>
        <li>
          <dl>
            <dd><a href="<?=$bqsr[titleurl]?>"><img src="<?=$bqr[titlepic]?>" height="90" width="120" /></a></dd>
            <dt><a href="<?=$bqsr[titleurl]?>"><?=esub($bqr[title],12,'…')?></a></dt>
          </dl>
        </li>
		 <?php
}
}
?>	
      
        <div class="clear"></div>
      </ul>
	  <div class="clear"></div>
</div>
    <!--/gamelist-->
    <div class="message mt15">
	
      <h1><a href="/news/">最新消息</a></h1>
	  
      <ul>
	  
	  <?php
$bqno=0;
$ecms_bq_sql=sys_ReturnEcmsLoopBq(1,5,0,0);
if($ecms_bq_sql){
while($bqr=$empire->fetch($ecms_bq_sql)){
$bqsr=sys_ReturnEcmsLoopStext($bqr);
$bqno++;
?>
        <li><span><?=date('Y-m-d',$bqr[newstime])?></span><br />
          <a href="<?=$bqsr[titleurl]?>"><?=$bqr[title]?> </a></li>
		    <?php
}
}
?>	
     
      </ul>
    </div>
    <!--/message-->
  </div>
  <!--/m_r-->
  <div class="clear"></div>
</div>
<!--/main-->
<div class="footer mt30">
  <div class="w1005">
    <p><a href="/about/aboutus.html">关于我们</a>|<a href="/joinus/">加入我们</a>|<a href="/team.html">团队介绍</a></p>
	<p style="padding-left:15px;">版权所有：北京网乐无限科技有限公司&nbsp;|&nbsp;2011-2014 wywlwx,Ltd. All rights Reserved.</p>
<p style="padding-left:15px;">地址：北京市朝阳区东三环中路39号建外SOHO西区17号楼805室 | 电话：(010)59005861 | 京ICP备12007609号</p>
<p style="padding-left:15px;">健康游戏忠告：抵制不良游戏 拒绝盗版游戏 注意自我保护 谨防受骗上当 适度游戏益脑 沉迷游戏伤身 合理安排时间 享受健康生活.</p>
  </div>
</div>




<!--/footer-->
</body>
</html>
