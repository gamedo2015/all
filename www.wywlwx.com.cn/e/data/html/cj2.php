<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>软件名称正则：</strong><br>
      (<input name="textfield" type="text" id="textfield" value="[!--title--]" size="20">)</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td><textarea name="add[zz_title]" cols="60" rows="10" id="textarea"><?=ehtmlspecialchars(stripSlashes($r[zz_title]))?></textarea></td>
        </tr>
        <tr> 
          <td><input name="add[z_title]" type="text" id="add[z_title]" value="<?=stripSlashes($r[z_title])?>">
            (如填写这里，将为字段的值)</td>
        </tr>
      </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>发布时间正则：</strong><br>
      (<input name="textfield" type="text" id="textfield" value="[!--newstime--]" size="20">)</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td><textarea name="add[zz_newstime]" cols="60" rows="10" id="textarea"><?=ehtmlspecialchars(stripSlashes($r[zz_newstime]))?></textarea></td>
        </tr>
        <tr> 
          <td><input name="add[z_newstime]" type="text" id="add[z_newstime]" value="<?=stripSlashes($r[z_newstime])?>">
            (如填写这里，将为字段的值)</td>
        </tr>
      </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>内容简介正则：</strong><br>
      (<input name="textfield" type="text" id="textfield" value="[!--smalltext--]" size="20">)</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td><textarea name="add[zz_smalltext]" cols="60" rows="10" id="textarea"><?=ehtmlspecialchars(stripSlashes($r[zz_smalltext]))?></textarea></td>
        </tr>
        <tr> 
          <td><input name="add[z_smalltext]" type="text" id="add[z_smalltext]" value="<?=stripSlashes($r[z_smalltext])?>">
            (如填写这里，将为字段的值)</td>
        </tr>
      </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>软件预览图正则：</strong><br>
      ( 
      <input name="textfield" type="text" id="textfield" value="[!--titlepic--]" size="20">
      )</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
      <td>附件前缀 
        <input name="add[qz_titlepic]" type="text" id="add[qz_titlepic]" value="<?=stripSlashes($r[qz_titlepic])?>"> 
        <input name="add[save_titlepic]" type="checkbox" id="add[save_titlepic]" value=" checked"<?=$r[save_titlepic]?>>
        远程保存 </td>
    </tr>
    <tr> 
      <td><textarea name="add[zz_titlepic]" cols="60" rows="10" id="add[zz_titlepic]"><?=ehtmlspecialchars(stripSlashes($r[zz_titlepic]))?></textarea></td>
    </tr>
    <tr> 
      <td><input name="add[z_titlepic]" type="text" id="titlepic5" value="<?=stripSlashes($r[z_titlepic])?>">
        (如填写这里，这就是字段的值)</td>
    </tr>
  </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>游戏资费正则：</strong><br>
      (<input name="textfield" type="text" id="textfield" value="[!--softzf--]" size="20">)</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td><textarea name="add[zz_softzf]" cols="60" rows="10" id="textarea"><?=ehtmlspecialchars(stripSlashes($r[zz_softzf]))?></textarea></td>
        </tr>
        <tr> 
          <td><input name="add[z_softzf]" type="text" id="add[z_softzf]" value="<?=stripSlashes($r[z_softzf])?>">
            (如填写这里，将为字段的值)</td>
        </tr>
      </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>最新版本正则：</strong><br>
      (<input name="textfield" type="text" id="textfield" value="[!--banben--]" size="20">)</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td><textarea name="add[zz_banben]" cols="60" rows="10" id="textarea"><?=ehtmlspecialchars(stripSlashes($r[zz_banben]))?></textarea></td>
        </tr>
        <tr> 
          <td><input name="add[z_banben]" type="text" id="add[z_banben]" value="<?=stripSlashes($r[z_banben])?>">
            (如填写这里，将为字段的值)</td>
        </tr>
      </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>软件语言正则：</strong><br>
      (<input name="textfield" type="text" id="textfield" value="[!--language--]" size="20">)</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td><textarea name="add[zz_language]" cols="60" rows="10" id="textarea"><?=ehtmlspecialchars(stripSlashes($r[zz_language]))?></textarea></td>
        </tr>
        <tr> 
          <td><input name="add[z_language]" type="text" id="add[z_language]" value="<?=stripSlashes($r[z_language])?>">
            (如填写这里，将为字段的值)</td>
        </tr>
      </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>游戏类型正则：</strong><br>
      (<input name="textfield" type="text" id="textfield" value="[!--softtype--]" size="20">)</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td><textarea name="add[zz_softtype]" cols="60" rows="10" id="textarea"><?=ehtmlspecialchars(stripSlashes($r[zz_softtype]))?></textarea></td>
        </tr>
        <tr> 
          <td><input name="add[z_softtype]" type="text" id="add[z_softtype]" value="<?=stripSlashes($r[z_softtype])?>">
            (如填写这里，将为字段的值)</td>
        </tr>
      </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>游戏大小正则：</strong><br>
      (<input name="textfield" type="text" id="textfield" value="[!--filesize--]" size="20">)</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td><textarea name="add[zz_filesize]" cols="60" rows="10" id="textarea"><?=ehtmlspecialchars(stripSlashes($r[zz_filesize]))?></textarea></td>
        </tr>
        <tr> 
          <td><input name="add[z_filesize]" type="text" id="add[z_filesize]" value="<?=stripSlashes($r[z_filesize])?>">
            (如填写这里，将为字段的值)</td>
        </tr>
      </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>适用固件正则：</strong><br>
      (<input name="textfield" type="text" id="textfield" value="[!--shiyong--]" size="20">)</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td><textarea name="add[zz_shiyong]" cols="60" rows="10" id="textarea"><?=ehtmlspecialchars(stripSlashes($r[zz_shiyong]))?></textarea></td>
        </tr>
        <tr> 
          <td><input name="add[z_shiyong]" type="text" id="add[z_shiyong]" value="<?=stripSlashes($r[z_shiyong])?>">
            (如填写这里，将为字段的值)</td>
        </tr>
      </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>苹果下载地址正则：</strong><br>
      ( 
      <input name="textfield" type="text" id="textfield" value="[!--iphoneads--]" size="20">
      )</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
      <td>附件前缀 
        <input name="add[qz_iphoneads]" type="text" id="add[qz_iphoneads]" value="<?=stripSlashes($r[qz_iphoneads])?>"> 
        <input name="add[save_iphoneads]" type="checkbox" id="add[save_iphoneads]" value=" checked"<?=$r[save_iphoneads]?>>
        远程保存 </td>
    </tr>
    <tr> 
      <td><textarea name="add[zz_iphoneads]" cols="60" rows="10" id="add[zz_iphoneads]"><?=ehtmlspecialchars(stripSlashes($r[zz_iphoneads]))?></textarea></td>
    </tr>
    <tr> 
      <td><input name="add[z_iphoneads]" type="text" id="iphoneads5" value="<?=stripSlashes($r[z_iphoneads])?>">
        (如填写这里，这就是字段的值)</td>
    </tr>
  </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>安卓下载地址正则：</strong><br>
      ( 
      <input name="textfield" type="text" id="textfield" value="[!--androidass--]" size="20">
      )</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
      <td>附件前缀 
        <input name="add[qz_androidass]" type="text" id="add[qz_androidass]" value="<?=stripSlashes($r[qz_androidass])?>"> 
        <input name="add[save_androidass]" type="checkbox" id="add[save_androidass]" value=" checked"<?=$r[save_androidass]?>>
        远程保存 </td>
    </tr>
    <tr> 
      <td><textarea name="add[zz_androidass]" cols="60" rows="10" id="add[zz_androidass]"><?=ehtmlspecialchars(stripSlashes($r[zz_androidass]))?></textarea></td>
    </tr>
    <tr> 
      <td><input name="add[z_androidass]" type="text" id="androidass5" value="<?=stripSlashes($r[z_androidass])?>">
        (如填写这里，这就是字段的值)</td>
    </tr>
  </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>苹果二维码正则：</strong><br>
      ( 
      <input name="textfield" type="text" id="textfield" value="[!--iphonecode--]" size="20">
      )</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
      <td>附件前缀 
        <input name="add[qz_iphonecode]" type="text" id="add[qz_iphonecode]" value="<?=stripSlashes($r[qz_iphonecode])?>"> 
        <input name="add[save_iphonecode]" type="checkbox" id="add[save_iphonecode]" value=" checked"<?=$r[save_iphonecode]?>>
        远程保存 </td>
    </tr>
    <tr> 
      <td><textarea name="add[zz_iphonecode]" cols="60" rows="10" id="add[zz_iphonecode]"><?=ehtmlspecialchars(stripSlashes($r[zz_iphonecode]))?></textarea></td>
    </tr>
    <tr> 
      <td><input name="add[z_iphonecode]" type="text" id="iphonecode5" value="<?=stripSlashes($r[z_iphonecode])?>">
        (如填写这里，这就是字段的值)</td>
    </tr>
  </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>安卓二维码正则：</strong><br>
      ( 
      <input name="textfield" type="text" id="textfield" value="[!--androidcode--]" size="20">
      )</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
      <td>附件前缀 
        <input name="add[qz_androidcode]" type="text" id="add[qz_androidcode]" value="<?=stripSlashes($r[qz_androidcode])?>"> 
        <input name="add[save_androidcode]" type="checkbox" id="add[save_androidcode]" value=" checked"<?=$r[save_androidcode]?>>
        远程保存 </td>
    </tr>
    <tr> 
      <td><textarea name="add[zz_androidcode]" cols="60" rows="10" id="add[zz_androidcode]"><?=ehtmlspecialchars(stripSlashes($r[zz_androidcode]))?></textarea></td>
    </tr>
    <tr> 
      <td><input name="add[z_androidcode]" type="text" id="androidcode5" value="<?=stripSlashes($r[z_androidcode])?>">
        (如填写这里，这就是字段的值)</td>
    </tr>
  </table></td>
  </tr>

  <tr bgcolor="#FFFFFF"> 
    <td height="22" valign="top"><strong>游戏特色正则：</strong><br>
      (<input name="textfield" type="text" id="textfield" value="[!--newstext--]" size="20">)</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr> 
          <td><textarea name="add[zz_newstext]" cols="60" rows="10" id="textarea"><?=ehtmlspecialchars(stripSlashes($r[zz_newstext]))?></textarea></td>
        </tr>
        <tr> 
          <td><input name="add[z_newstext]" type="text" id="add[z_newstext]" value="<?=stripSlashes($r[z_newstext])?>">
            (如填写这里，将为字段的值)</td>
        </tr>
      </table></td>
  </tr>
