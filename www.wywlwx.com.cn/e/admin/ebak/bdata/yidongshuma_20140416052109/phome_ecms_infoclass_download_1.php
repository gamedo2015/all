<?php
@include("../../inc/header.php");

/*
		SoftName : EmpireBak
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

E_D("DROP TABLE IF EXISTS `phome_ecms_infoclass_download`;");
E_C("CREATE TABLE `phome_ecms_infoclass_download` (
  `classid` int(10) unsigned NOT NULL default '0',
  `zz_title` mediumtext NOT NULL,
  `z_title` varchar(255) NOT NULL default '',
  `qz_title` varchar(255) NOT NULL default '',
  `save_title` varchar(10) NOT NULL default '',
  `zz_titlepic` mediumtext NOT NULL,
  `z_titlepic` varchar(255) NOT NULL default '',
  `qz_titlepic` varchar(255) NOT NULL default '',
  `save_titlepic` varchar(10) NOT NULL default '',
  `zz_newstime` mediumtext NOT NULL,
  `z_newstime` varchar(255) NOT NULL default '',
  `qz_newstime` varchar(255) NOT NULL default '',
  `save_newstime` varchar(10) NOT NULL default '',
  `zz_language` mediumtext NOT NULL,
  `z_language` varchar(255) NOT NULL default '',
  `qz_language` varchar(255) NOT NULL default '',
  `save_language` varchar(10) NOT NULL default '',
  `zz_softtype` mediumtext NOT NULL,
  `z_softtype` varchar(255) NOT NULL default '',
  `qz_softtype` varchar(255) NOT NULL default '',
  `save_softtype` varchar(10) NOT NULL default '',
  `zz_softzf` mediumtext NOT NULL,
  `z_softzf` varchar(255) NOT NULL,
  `qz_softzf` varchar(255) NOT NULL,
  `save_softzf` varchar(10) NOT NULL,
  `zz_banben` mediumtext NOT NULL,
  `z_banben` varchar(255) NOT NULL,
  `qz_banben` varchar(255) NOT NULL,
  `save_banben` varchar(10) NOT NULL,
  `zz_filesize` mediumtext NOT NULL,
  `z_filesize` varchar(255) NOT NULL default '',
  `qz_filesize` varchar(255) NOT NULL default '',
  `save_filesize` varchar(10) NOT NULL default '',
  `zz_softsay` text NOT NULL,
  `z_softsay` varchar(255) NOT NULL,
  `qz_softsay` varchar(255) NOT NULL,
  `save_softsay` varchar(10) NOT NULL,
  `zz_shiyong` mediumtext NOT NULL,
  `z_shiyong` varchar(255) NOT NULL,
  `qz_shiyong` varchar(255) NOT NULL,
  `save_shiyong` varchar(10) NOT NULL,
  `zz_iphoneads` mediumtext NOT NULL,
  `z_iphoneads` varchar(255) NOT NULL,
  `qz_iphoneads` varchar(255) NOT NULL,
  `save_iphoneads` varchar(10) NOT NULL,
  `zz_androidass` mediumtext NOT NULL,
  `z_androidass` varchar(255) NOT NULL,
  `qz_androidass` varchar(255) NOT NULL,
  `save_androidass` varchar(10) NOT NULL,
  `zz_androidcode` mediumtext NOT NULL,
  `z_androidcode` varchar(255) NOT NULL,
  `qz_androidcode` varchar(255) NOT NULL,
  `save_androidcode` varchar(10) NOT NULL,
  `zz_iphonecode` mediumtext NOT NULL,
  `z_iphonecode` varchar(255) NOT NULL,
  `qz_iphonecode` varchar(255) NOT NULL,
  `save_iphonecode` varchar(10) NOT NULL,
  `zz_smalltext` mediumtext NOT NULL,
  `z_smalltext` varchar(255) NOT NULL,
  `qz_smalltext` varchar(255) NOT NULL,
  `save_smalltext` varchar(10) NOT NULL,
  PRIMARY KEY  (`classid`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk");

@include("../../inc/footer.php");
?>