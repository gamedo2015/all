<?php
@include("../../inc/header.php");

/*
		SoftName : EmpireBak
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

E_D("DROP TABLE IF EXISTS `phome_ecms_infotmp_job`;");
E_C("CREATE TABLE `phome_ecms_infotmp_job` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `classid` int(10) unsigned NOT NULL default '0',
  `oldurl` varchar(200) NOT NULL default '',
  `checked` tinyint(1) NOT NULL default '0',
  `tmptime` datetime NOT NULL default '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL default '0',
  `username` varchar(20) NOT NULL default '',
  `truetime` int(10) unsigned NOT NULL default '0',
  `keyboard` varchar(100) NOT NULL default '',
  `title` varchar(100) NOT NULL default '',
  `newstime` datetime NOT NULL default '0000-00-00 00:00:00',
  `titlepic` varchar(120) NOT NULL default '',
  `smalltext` varchar(255) NOT NULL default '',
  `newstext` mediumtext NOT NULL,
  `zhiwei` varchar(255) NOT NULL default '',
  `bianhao` varchar(255) NOT NULL default '',
  `bumen` varchar(255) NOT NULL default '',
  `duixiang` varchar(255) NOT NULL default '',
  `didian` varchar(255) NOT NULL default '',
  `leixing` varchar(255) NOT NULL default '',
  `renshu` varchar(255) NOT NULL default '',
  `yuexing` varchar(255) NOT NULL default '',
  `zhaopinleixing` varchar(255) NOT NULL default '',
  `nianling` varchar(255) NOT NULL default '',
  `nianxian` varchar(255) NOT NULL default '',
  `yingyu` varchar(255) NOT NULL default '',
  `xueli` varchar(255) NOT NULL default '',
  `xingbie` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `classid` (`classid`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk");

@include("../../inc/footer.php");
?>