<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='配送地址列表';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>&nbsp;>&nbsp;会员登录";
require(ECMS_PATH.'e/template/incfile/top.php');
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/user.css" type="text/css">

<script src="<?=$public_r['newsurl']?>skin/member/images/jquery.js" type="text/javascript"></script>
</head>

<body>

<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>

<div class="fullBox">
<?php
require(ECMS_PATH.'e/template/incfile/menu.php');
?>
<div class="uRightBox">
<div class="minHeight"></div>
<div class="uContent">
<h2>配送地址列表</h2>
<div class="uTabCon uProfile1">
<div class="line4"></div>


<table width="600" border="0" align="center" cellpadding="3" cellspacing="1" class="tableborder">
  <tr>
    <td width="50%" height="30" bgcolor="#FFFFFF">&nbsp;</td>
    <td width="50%" bgcolor="#FFFFFF"><div align="right">[<a href="AddAddress.php?enews=AddAddress">增加配送地址</a>]&nbsp;&nbsp;</div></td>
  </tr>
</table>
<br>
<table width="600" border="0" align="center" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#eeeeee">
    <tr class="header">
      <td width="65%" height="35"><div align="center">地址名称</div></td>
      <td width="10%"><div align="center">默认</div></td>
      <td width="25%"><div align="center">操作</div></td>
    </tr>
    <?php
	while($r=$empire->fetch($sql))
	{
		if($r['isdefault'])
		{
			$isdefault='是';
		}
		else
		{
			$isdefault='--';
		}
	?>
    <tr bgcolor="#FFFFFF">
      <td height="35"><div align="center"><?=$r['addressname']?></div></td>
      <td><div align="center"><?=$isdefault?></div></td>
      <td><div align="center">[<a href="AddAddress.php?enews=EditAddress&addressid=<?=$r['addressid']?>">修改</a>] [<a href="../doaction.php?enews=DefAddress&addressid=<?=$r['addressid']?>" onclick="return confirm('确认要设为默认?');">默认</a>] [<a href="../doaction.php?enews=DelAddress&addressid=<?=$r['addressid']?>" onclick="return confirm('确认要删除?');">删除</a>]</div></td>
    </tr>
    <?php
	}
	?>
</table>


</div>
</div>
</div>
</div>		
		
<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>