<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='会员状态';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>";
require(ECMS_PATH.'e/template/incfile/top.php');
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/user.css" type="text/css">

<script src="<?=$public_r['newsurl']?>skin/member/images/jquery.js" type="text/javascript"></script>
</head>

<body>

<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>

<div class="fullBox">
<?php
require(ECMS_PATH.'e/template/incfile/menu.php');
?>


<div class="uRightBox">
<div class="minHeight"></div>
<div class="uContent">
<h2>帐号状态</h2>
<div class="uTabCon uProfile">
<table width="90%" border="0" align="center" cellpadding="8" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#eeeeee">
        <tr> 
          <td width="19%" height="35" align="right" bgcolor="#FFFFFF">用户ID：</td>
          <td width="81%" bgcolor="#FFFFFF">&nbsp;&nbsp;<?=$user[userid]?></td>
        </tr>
        <tr> 
          <td height="35" align="right" bgcolor="#FFFFFF">用户名：</td>
          <td bgcolor="#FFFFFF">&nbsp;&nbsp;<?=$user[username]?></td>
        </tr>
        <tr> 
          <td height="35" align="right" bgcolor="#FFFFFF">注册时间：</td>
          <td bgcolor="#FFFFFF">&nbsp;&nbsp;<?=$registertime?></td>
        </tr>
        <tr> 
          <td height="35" align="right" bgcolor="#FFFFFF">会员等级：</td>
          <td bgcolor="#FFFFFF">&nbsp;&nbsp;<?=$level_r[$r[groupid]][groupname]?></td>
        </tr>
        <tr> 
          <td height="35" align="right" bgcolor="#FFFFFF">剩余有效期：</td>
          <td bgcolor="#FFFFFF">&nbsp;&nbsp;<?=$userdate?> 天</td>
        </tr>
        <tr> 
          <td height="35" align="right" bgcolor="#FFFFFF">剩余点数：</td>
          <td bgcolor="#FFFFFF">&nbsp;&nbsp;<?=$r[userfen]?> 点</td>
        </tr>
        <tr> 
          <td height="35" align="right" bgcolor="#FFFFFF">帐户余额：</td>
          <td bgcolor="#FFFFFF">&nbsp;&nbsp;<?=$r[money]?> 元</td>
        </tr>
        <tr> 
          <td height="35" align="right" bgcolor="#FFFFFF">新短消息：</td>
          <td bgcolor="#FFFFFF">&nbsp;&nbsp;<?=$havemsg?></td>
        </tr>
      </table>
</div>
</div>
</div>
</div>

<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>