<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='会员中心';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>";
require(ECMS_PATH.'e/template/incfile/top.php');
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/user.css" type="text/css">

<script src="<?=$public_r['newsurl']?>skin/member/images/jquery.js" type="text/javascript"></script>
</head>

<body>

<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>

<div class="fullBox">
<?php
require(ECMS_PATH.'e/template/incfile/menu.php');
?>

<div class="uRightBox">
<div class="uContent">
<h2>会员中心</h2>
<div class="infoBox">
<div class="welcome"> <strong><?=$user[username]?></strong>  ID:<?=$user[userid]?>  <b>亲，欢迎您回来！</b>注册时间：<?=$registertime?></div>

<div class="detailInfo">
<div class="coupon">
<p>您的帐户余额：</p>
<div class="uIcon6"><b><?=$r[money]?></b>元</div>
<a href="<?=$public_r['newsurl']?>e/payapi/" class="uBtn2">在线支付</a> </div>
<div class="integral">
<p>会员等级：<b><?=$level_r[$r[groupid]][groupname]?></b></p>
<p>剩余点数：<b><?=$r[userfen]?></b>点</p>
<p>剩余有效期：<b><?=$userdate?></b>天</p>
<dl class="uCol">
<dt>新消息：<?=$havemsg?></dt>
<dd>
<div class="uIcon5"></div>
</dd>
</dl>
</div>
</div>
<dl class="bigFace">
<dt> <img src="<?=$userpic?>" width="145" height="145" alt="" /> </dt>
<dd> <a href="<?=$public_r['newsurl']?>e/member/EditInfo/" class="uBtn1">更改头像</a> </dd>
</dl>
</div>
<div class="indexTab">
<ul class="uTab">
<li><a href="../EditInfo/">修改资料</a></li>
<li><a href="../msg/">站内消息</a></li>
<li><a href="../fava/">管理收藏夹</a></li>

</ul>
</div>

</div>
</div>
</div>



<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>