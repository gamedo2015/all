<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php

//配置查询自定义字段列表,逗号开头，多个用逗号格开，格式“ui.字段名”
$useraddf=',ui.userpic';

//分页SQL
$query='select '.eReturnSelectMemberF('userid,username,email,registertime,groupid','u.').$useraddf.' from '.eReturnMemberTable().' u'.$add." order by u.".egetmf('userid')." desc limit $offset,$line";
$sql=$empire->query($query);

//导航
$public_diyr['pagetitle']='会员列表';
$url="<a href='../../../'>首页</a>&nbsp;>&nbsp;会员列表";
require(ECMS_PATH.'e/template/incfile/top.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/user.css" type="text/css">

<script src="<?=$public_r['newsurl']?>skin/member/images/jquery.js" type="text/javascript"></script>
</head>

<body>

<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>

<div class="fullBox">
<?php
require(ECMS_PATH.'e/template/incfile/menu.php');
?>


<div class="uRightBox">
<div class="minHeight"></div>
<div class="uContent">
<h2>会员列表</h2>
<div class="uTabCon uProfile1">
<div class="line4"></div>



<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#eeeeee">
  <form name="memberform" method="get" action="index.php">
    <input type="hidden" name="sear" value="1">
    <input type="hidden" name="groupid" value="<?=$groupid?>">
    <tr class="header"> 
      <td width="10%"><div align="center">ID</div></td>
      <td width="38%" height="25"><div align="left">&nbsp;&nbsp;用户名</div></td>
      <td width="30%" height="25"><div align="center">注册时间</div></td>
      <td width="22%" height="25"><div align="center"></div></td>
    </tr>
    <?php
	while($r=$empire->fetch($sql))
	{
		//注册时间
		$registertime=eReturnMemberRegtime($r['registertime'],"Y-m-d H:i:s");
		//用户组
		$groupname=$level_r[$r['groupid']]['groupname'];
		//用户头像
		$userpic=$r['userpic']?$r['userpic']:$public_r[newsurl].'e/data/images/nouserpic.gif';
	?>
    <tr bgcolor="#FFFFFF"> 
      <td><div align="center"> 
          <?=$r['userid']?>
        </div></td>
      <td height="25"> 
        &nbsp;&nbsp;<?=$r['username']?>
       </td>
      <td height="25"><div align="center"> 
          <?=$registertime?>
        </div></td>
      <td height="25"><div align="center"> [<a href="<?=$public_r[newsurl]?>e/member/ShowInfo/?userid=<?=$r['userid']?>" target="_blank">会员资料</a>] 
         </div></td>
    </tr>
    <?
  	}
  	?>
    <tr bgcolor="#FFFFFF"> 
      <td height="25" colspan="3">  <div class="epages">
        <?=$returnpage?></div>
      </td>
      <td height="25"> <div align="center"> 
          <input name="keyboard" type="text" id="keyboard" size="10">
          <input type="submit" name="Submit" value="搜索">
        </div></td>
    </tr>
  </form>
</table>

</div>
</div>
</div>
</div>


<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>