<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='发送站内消息';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>";
require(ECMS_PATH.'e/template/incfile/top.php');
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/user.css" type="text/css">

<script src="<?=$public_r['newsurl']?>skin/member/images/jquery.js" type="text/javascript"></script>
</head>

<body>

<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>

<div class="fullBox">
<?php
require(ECMS_PATH.'e/template/incfile/menu.php');
?>
<div class="uRightBox">
<div class="minHeight"></div>
<div class="uContent">
<h2>发送消息</h2>
<div class="uTabCon uProfile1">
<div class="line4"></div>

        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" class="tableborder">
          <form action="../../doaction.php" method="post" name="sendmsg" id="sendmsg">
         
            <tr bgcolor="#FFFFFF"> 
              <td width="21%" height="45" align="right">标题&nbsp;&nbsp;</td>
              <td width="79%" height="45">&nbsp;&nbsp;<input name="title" type="text" id="title2" value="<?=ehtmlspecialchars(stripSlashes($title))?>" size="43" class="uInp1">
                *</td>
            </tr>
            <tr bgcolor="#FFFFFF"> 
              <td height="45" align="right">接收者&nbsp;&nbsp;</td>
              <td height="45">&nbsp;&nbsp;<input name="to_username" type="text" id="to_username2" value="<?=$username?>" class="uInp1">
                [<a href="#EmpireCMS" onclick="window.open('../../friend/change/?fm=sendmsg&f=to_username','','width=250,height=360');">选择好友</a>] 
                *</td>
            </tr>
            <tr bgcolor="#FFFFFF"> 
              <td height="45" align="right" valign="top">内容&nbsp;&nbsp;</td>
              <td height="45">&nbsp;&nbsp;<textarea name="msgtext" cols="60" rows="12" class="uInp1 uInp2" id="textarea"><?=ehtmlspecialchars(stripSlashes($msgtext))?></textarea>
                *</td>
            </tr>
            <tr bgcolor="#FFFFFF"> 
              <td height="45">&nbsp;</td>
              <td height="45">&nbsp;&nbsp;<input type="submit" name="Submit" value="发送" class="submitBtn">
                &nbsp;
                <input name="enews" type="hidden" id="enews" value="AddMsg">              </td>
            </tr>
          </form>
        </table>
		
</div>
</div>
</div>
</div>		
		
<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>