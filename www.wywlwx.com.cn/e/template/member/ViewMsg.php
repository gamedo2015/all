<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='查看站内消息';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>&nbsp;>&nbsp;消息列表&nbsp;&nbsp;(<a href='AddMsg/?enews=AddMsg'>发送消息</a>)";
require(ECMS_PATH.'e/template/incfile/top.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/user.css" type="text/css">

<script src="<?=$public_r['newsurl']?>skin/member/images/jquery.js" type="text/javascript"></script>
</head>

<body>

<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>

<div class="fullBox">
<?php
require(ECMS_PATH.'e/template/incfile/menu.php');
?>

<script>
function CheckAll(form)
  {
  for (var i=0;i<form.elements.length;i++)
    {
    var e = form.elements[i];
    if (e.name != 'chkall')
       e.checked = form.chkall.checked;
    }
  }
</script> 


<div class="uRightBox">
<div class="minHeight"></div>
<div class="uContent">
<h2>查看消息</h2>
<div class="uTabCon uProfile1">
<div class="line4"></div>
        <table width="100%" border="0" align="center" cellpadding="3"  cellspacing="1" bordercolor="#FFFFFF" bgcolor="#eeeeee">
          <form name="form1" method="post" action="../../doaction.php">
		  
		    <tr bgcolor="#FFFFFF"> 
              <td width="19%" height="35" align="right">标题：&nbsp;&nbsp;</td>
              <td width="81%" height="35">&nbsp;&nbsp;<?=stripSlashes($r[title])?> </td>
            </tr>
         
            <tr bgcolor="#FFFFFF"> 
              <td width="19%" height="35" align="right">发送者：&nbsp;&nbsp;</td>
              <td width="81%" height="35">&nbsp;&nbsp;<a href="../../ShowInfo/?userid=<?=$r[from_userid]?>"> 
                <?=$r[from_username]?>
                </a></td>
            </tr>
            <tr bgcolor="#FFFFFF"> 
              <td height="35" align="right">发送时间：&nbsp;&nbsp;</td>
              <td height="35">
               &nbsp;&nbsp; <?=$r[msgtime]?>              </td>
            </tr>
            <tr bgcolor="#FFFFFF"> 
              <td height="35" align="right" valign="top">内容：&nbsp;&nbsp;</td>
              <td height="35"> 
                &nbsp;&nbsp;<?=nl2br(stripSlashes($r[msgtext]))?>              </td>
            </tr>
            <tr bgcolor="#FFFFFF"> 
              <td height="35" valign="top">&nbsp;</td>
              <td height="35">&nbsp;&nbsp;[<a href="#ecms" onclick="javascript:history.go(-1);"><strong>返回</strong></a>] 
                [<a href="../AddMsg/?enews=AddMsg&re=1&mid=<?=$mid?>"><strong>回复</strong></a>] 
                [<a href="../AddMsg/?enews=AddMsg&mid=<?=$mid?>"><strong>转发</strong></a>] 
                [<a href="../../doaction.php?enews=DelMsg&mid=<?=$mid?>" onclick="return confirm('确认要删除?');"><strong>删除</strong></a>]</td>
            </tr>
          </form>
        </table>
		
		
	  </div>
</div>
</div>
</div>
		
		
<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>