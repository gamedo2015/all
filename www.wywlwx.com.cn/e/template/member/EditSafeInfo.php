<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='修改密码';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>";
require(ECMS_PATH.'e/template/incfile/top.php');
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/user.css" type="text/css">

<script src="<?=$public_r['newsurl']?>skin/member/images/jquery.js" type="text/javascript"></script>
</head>

<body>

<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>

<div class="fullBox">
<?php
require(ECMS_PATH.'e/template/incfile/menu.php');
?>



<div class="uRightBox">
<div class="minHeight"></div>
<div class="uContent">
<h2>密码安全修改</h2>
<ul class="uTab">
<li class="focus">修改安全信息</li>
<li><a href="../EditInfo/">修改基本资料</a></li>
</ul>
<div class="uTabCon" style="display:;">
<form action="../doaction.php" method="post" enctype="multipart/form-data" name="userinfoform" onsubmit="return checkSubmit();">
<input type=hidden name=enews value=EditSafeInfo>
<table class="uProfile">
<tr>
<th>用户名：</th>
<td><?=$user[username]?></td>
</tr>
<tr>
<th>原密码：</th>
<td><input name="oldpassword" type="password" id="oldpassword" value="" class="uInp1" />
<span style="color:red;">(修改密码或邮箱时需要密码验证)</span></td>
</tr>
<tr>
<th>新密码：</th>
<td><input name="password" type="password" id="password" value="" class="uInp1" />
<span id="_userpwdok">(不想修改请留空)</span></td>
</tr>
<tr>
<th>确认新密码：</th>
<td><input name="repassword" type="password" id="repassword" value="" class="uInp1" />
<span id="_userpwdok2">(不想修改请留空)</span></td>
</tr>
<tr>
<th>E-mail：</th>
<td><input name="email" type="text" id="email" value="<?=$user[email]?>" class="uInp1" />
<span id="_email">(每个邮箱只能注册一个帐号，请勿随意修改)</span></td>
</tr>
<tr>
<th>&nbsp;</th>
<td><input type="submit" class="submitBtn" name="Submit" value="修改信息"></td>
</tr>
</table>
</form>
</div>
</div>
</div>
</div>


<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>