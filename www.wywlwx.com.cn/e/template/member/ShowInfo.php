<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
//oicq
if($addr['oicq'])
{
	$addr['oicq']="<a href='http://wpa.qq.com/msgrd?V=1&amp;Uin=".$addr['oicq']."&amp;Site=".$public_r['sitename']."&amp;Menu=yes' target='_blank'><img src='http://wpa.qq.com/pa?p=1:".$addr['oicq'].":4'  border='0' alt='QQ' />".$addr['oicq']."</a>";
}
//表单
$record="<!--record-->";
$field="<!--field--->";
$er=explode($record,$formr['viewenter']);
$count=count($er);
for($i=0;$i<$count-1;$i++)
{
	$er1=explode($field,$er[$i]);
	if(strstr($formr['filef'],",".$er1[1].","))//附件
	{
		if($addr[$er1[1]])
		{
			$val="<a href='".$addr[$er1[1]]."' target='_blank'>".$addr[$er1[1]]."</a>";
		}
		else
		{
			$val="";
		}
	}
	elseif(strstr($formr['imgf'],",".$er1[1].","))//图片
	{
		if($addr[$er1[1]])
		{
			$val="<img src='".$addr[$er1[1]]."' border=0 width='100' height='100'>";
		}
		else
		{
			$val="";
		}
	}
	elseif(strstr($formr['tobrf'],",".$er1[1].","))//多行文本框
	{
		$val=nl2br($addr[$er1[1]]);
	}
	else
	{
		$val=$addr[$er1[1]];
	}
	$memberinfo.="<tr bgcolor='#FFFFFF'><td height=25 align='right'>".$er1[0].":&nbsp;&nbsp;</td><td>&nbsp;&nbsp;".$val."</td></tr>";
}

$public_diyr['pagetitle']='查看 '.$username.' 的会员资料';
$url="<a href='../../../'>首页</a>&nbsp;>&nbsp;<a href='../cp/'>会员中心</a>&nbsp;>&nbsp;查看会员资料";
require(ECMS_PATH.'e/template/incfile/top.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/user.css" type="text/css">

<script src="<?=$public_r['newsurl']?>skin/member/images/jquery.js" type="text/javascript"></script>
</head>

<body>

<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>

<div class="fullBox">
<?php
require(ECMS_PATH.'e/template/incfile/menu.php');
?>
<div class="uRightBox">
<div class="minHeight"></div>
<div class="uContent">
<h2>查看资料</h2>
<div class="uTabCon uProfile">


<table width="90%" border="0" align="center" cellpadding="8" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#eeeeee">
  <tr> 
    <td height="25" colspan="2">&nbsp;&nbsp;查看 <?=$username?> 的会员资料</td>
  </tr>
  <tr>
    <td height="25" bgcolor="#FFFFFF">&nbsp;</td>
    <td height="25" bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="3" cellspacing="1">
        <tr>
          <td> &nbsp;&nbsp;[ <a href="../msg/AddMsg/?username=<?=$username?>" target="_blank">发短消息</a> 
            ] [ <a href="../friend/add/?fname=<?=$username?>" target="_blank">加为好友</a> 
            ] </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td width='17%' height="25" align="right" bgcolor="#FFFFFF"> 用户名:&nbsp;&nbsp;</td>
    <td width='83%' height="25" bgcolor="#FFFFFF"> &nbsp;&nbsp;
      <?=$username?>
    </td>
  </tr>
  <tr> 
    <td height="25" align="right" bgcolor="#FFFFFF">会员等级:&nbsp;&nbsp;</td>
    <td height="25" bgcolor="#FFFFFF">&nbsp;&nbsp;
      <?=$level_r[$r[groupid]]['groupname']?>
    </td>
  </tr>
  <tr> 
    <td height="25" align="right" bgcolor="#FFFFFF">注册时间:&nbsp;&nbsp;</td>
    <td height="25" bgcolor="#FFFFFF">&nbsp;&nbsp; 
      <?=$registertime?>
    </td>
  </tr>
  <tr> 
    <td height="25" align="right" bgcolor="#FFFFFF"> 邮箱:&nbsp;&nbsp;</td>
    <td height="25" bgcolor="#FFFFFF"> &nbsp;&nbsp;<a href="mailto:<?=$email?>"> 
      <?=$email?>
      </a></td>
  </tr>
  <?=$memberinfo?>
  <tr> 
    <td bgcolor="#FFFFFF">&nbsp;</td>
    <td height="25" bgcolor="#FFFFFF"> &nbsp;&nbsp;
      <input type='button' name='Submit2' value='返回' class="submitBtn" onclick='history.go(-1)'></td>
  </tr>
</table>


</div>
</div>
</div>
</div>




<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>