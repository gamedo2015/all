<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='下载记录';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>";
require(ECMS_PATH.'e/template/incfile/top.php');
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/user.css" type="text/css">

<script src="<?=$public_r['newsurl']?>skin/member/images/jquery.js" type="text/javascript"></script>
</head>

<body>

<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>

<div class="fullBox">
<?php
require(ECMS_PATH.'e/template/incfile/menu.php');
?>
<div class="uRightBox">
<div class="minHeight"></div>
<div class="uContent">
<h2>消费记录</h2>
<div class="uTabCon uProfile1">
<div class="line4"></div>

        <table width="100%" border="0" align="center" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#eeeeee">
          <tr class="header"> 
            <td width="55%" height="35"><div align="center">标题</div></td>
            <td width="16%" height="35"><div align="center">扣除点数</div></td>
            <td width="29%" height="35"><div align="center">时间</div></td>
          </tr>
	<?php
	while($r=$empire->fetch($sql))
	{
		if(empty($class_r[$r[classid]][tbname]))
		{continue;}
		$nr=$empire->fetch1("select title,isurl,titleurl,classid from {$dbtbpre}ecms_".$class_r[$r[classid]][tbname]." where id='$r[id]' limit 1");
		//标题链接
		$titlelink=sys_ReturnBqTitleLink($nr);
		if(!$nr['classid'])
		{
			$nr['title']="此信息已删除";
			$titlelink="#EmpireCMS";
		}
		if($r['online']==0)
		{
			$type='下载';
		}
		elseif($r['online']==1)
		{
			$type='观看';
		}
		elseif($r['online']==2)
		{
			$type='查看';
		}
		elseif($r['online']==3)
		{
			$type='发布';
		}
	?>
          <tr bgcolor="#FFFFFF"> 
            <td height="35">&nbsp;&nbsp;[
              <?=$type?>
              ] &nbsp;<a href='<?=$titlelink?>' target='_blank'> 
              <?=$r[title]?>
              </a> </td>
            <td height="35"><div align="center"> 
                <?=$r[cardfen]?>
              </div></td>
            <td height="35"><div align="center"> 
                <?=date("Y-m-d H:i:s",$r[truetime])?>
              </div></td>
          </tr>
          <?
	}
	?>
          <tr bgcolor="#FFFFFF"> 
            <td height="35" colspan="3"> 
            <div class="epages">  <?=$returnpage?></div>       </td>
          </tr>
        </table>
		
		
</div>
</div>
</div>
</div>		
		
<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>