<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='会员登录';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>";
require(ECMS_PATH.'e/template/incfile/top.php');
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/login.css" type="text/css">
</head>

<body>


<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>




<div class="fullBox">
<div class="fullBoxTop">
<div class="boxTitleReg">会员登录</div>
</div>
<div class="fullBoxMid">
<div class="regBox">
<div class="regLeft">
 <form name="form1" method="post" action="../doaction.php">
    <input type=hidden name=ecmsfrom value="<?=ehtmlspecialchars($_GET['from'])?>">
    <input type=hidden name=enews value=login>
	<input name="tobind" type="hidden" id="tobind" value="<?=$tobind?>">
<table class="loginTab login1Tab">
<tbody><tr>
<th>用户名：</th>
<td>
<input name="username" type="text" id="username" size="30" class="loginInp"  onFocus="this.value=(this.value==&#39;用户名/注册邮箱&#39;)?&#39;&#39;:this.value" onBlur="this.value=(this.value==&#39;&#39;)?&#39;用户名/注册邮箱&#39;:this.value" value="用户名/注册邮箱">
	  	<?php
		if($public_r['regacttype']==1)
		{
		?>
        &nbsp;&nbsp;<a href="../register/regsend.php" target="_blank">帐号未激活？</a>
		<?php
		}
		?>

		</td>
</tr>
<tr>
<th>密 码：</th>
<td>
	  <input name="password" type="password" id="password" size="30" class="loginInp"></td>
</tr>
<tr>
<th>保存时间：</th>
      <td>
		  <input name=lifetime type=radio value=0 checked>
        不保存
	    <input type=radio name=lifetime value=3600>
        一小时 
        <input type=radio name=lifetime value=86400>
        一天 
        <input type=radio name=lifetime value=2592000>
        一个月
<input type=radio name=lifetime value=315360000>
        永久 </td>
</tr>

  <?php
	if($public_r['loginkey_ok'])
	{
	?>
    <tr> 
   <th>验证码：</th>
      <td height="25"><input name="key" type="text" id="key" size="6" class="loginInp loginInp2">
        <img id="vdimgck" align="absmiddle" src="../../ShowKey/?v=login"></td>
    </tr>
    <?php
	}	
	?>
    <tr>
<th>&nbsp;</th>
<td><input type="submit" name="Submit" class="loginBtnBig" value="">
<a href="<?=$public_r['newsurl']?>e/member/GetPassword/">忘记密码？</a></td>
</tr>
</tbody></table>
</form>
</div>
<div class="regRight">
<p class="loginNow"> 我还未注册过，<a href="<?=$public_r['newsurl']?>e/member/register/">立即注册</a><br>
</p>
</div>
</div>
</div>
</div>
<div class="clear"></div>


<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>