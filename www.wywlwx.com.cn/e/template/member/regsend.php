<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='注册激活';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>";
require(ECMS_PATH.'e/template/incfile/top.php');
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/login.css" type="text/css">
</head>

<body>


<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>


<div class="fullBox">
<div class="fullBoxTop">
<div class="boxTitleReg">重发帐号激活邮件</div>
</div>
<div class="fullBoxMid">
<div class="regBox">
<div class="regLeft">
<form name="RegSendForm" method="POST" action="../doaction.php">

<table class="loginTab regTab">
<tr>
<th>用户名：</th>
<td><input type='text' id="username" name="username" maxlength='30' class="loginInp" /></td>
<td><p id="_userid">(*)</p></td>
</tr>
<tr>
<th>密码：</th>
<td><input type='password' id="password" name="password" maxlength='20' class="loginInp" /></td>
<td><p id="passwordTips">(*)</p></td>
</tr>
<tr>
<th>E-Mail：</th>
<td><input name='email' type='text' id='email' maxlength='50' class="loginInp" /></td>
<td><p id="_email">(*)</p></td>
</tr>
<tr>
<th>新接收邮箱：</th>
<td><input value="" id="newemail" name="newemail" type="text" maxlength="20" class="loginInp" /></td>
<td><p id="_userpwdok">(要改变接收邮箱可填写)</p></td>
</tr>
<tr>
<th>验证码：</th>
<td><input type="text" class="loginInp loginInp2" style="width: 50px; text-transform: uppercase;" id="key" name="key" size="6"/>
<img id="vdimgck" align="absmiddle" onclick="this.src=this.src+'?'" style="cursor: pointer;" alt="看不清？点击更换" src="../../ShowKey/?v=regsend"></td>
<td><p id="keyTips"></p></td>
</tr>
<tr>
<th>&nbsp;</th>
<td><input type="submit" name="button"  class="submitBtn" value="提 交" /><input name="enews" type="hidden" id="enews" value="RegSend"></td>
<td>&nbsp;</td>
</tr>
</table>
</form>
</div>
<div class="regRight">
<p class="loginNow"> 我已有账号，<a href="<?=$public_r['newsurl']?>e/member/login/">直接登录</a><br />
<a href="<?=$public_r['newsurl']?>e/memberconnect/?apptype=qq"><img src="<?=$public_r['newsurl']?>skin/member/images/qqlogin.gif" border="0"></a>&nbsp;<br>
<a href="<?=$public_r['newsurl']?>e/memberconnect/?apptype=sina"><img src="<?=$public_r['newsurl']?>skin/member/images/sinalogin.png" border="0"></a>
</p>
</div>
</div>
</div>
</div>

<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>