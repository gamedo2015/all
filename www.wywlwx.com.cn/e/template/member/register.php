<?php
if(!defined('InEmpireCMS'))
{
	exit();
}
?>
<?php
$public_diyr['pagetitle']='会员注册';
$url="<a href=../../../>首页</a>&nbsp;>&nbsp;<a href=../cp/>会员中心</a>";
require(ECMS_PATH.'e/template/incfile/top.php');
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title><?=$thispagetitle?></title>

<link rel="stylesheet" href="<?=$public_r['newsurl']?>skin/member/images/login.css" type="text/css">
<script type='text/javascript'>


var vcity={ 11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",
            21:"辽宁",22:"吉林",23:"黑龙江",31:"上海",32:"江苏",
            33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",
            42:"湖北",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",
            51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",
            63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"
           };

checktheform = function()
{

   var sPhone = document.userinfoform.phone.value 
    if(!(/^1[3|5][0-9]\d{4,8}$/.test(sPhone))){ 
        alert("手机号码不能为空或者手机号码不正确"); 
        document.userinfoform.phone.focus(); 
        return false; 
    } 
	
	
    var card = document.getElementById('card_no').value;
    //是否为空
    if(card === '')
    {
        alert('请输入身份证号，身份证号不能为空');
        document.getElementById('card_no').focus;
        return false;
    }
    //校验长度，类型
    if(isCardNo(card) === false)
    {
        alert('您输入的身份证号码不正确，请重新输入');
        document.getElementById('card_no').focus;
        return false;
    }
	
	
    //检查省份
    if(checkProvince(card) === false)
    {
        alert('您输入的身份证号码不正确,请重新输入');
        document.getElementById('card_no').focus;
        return false;
    }
    //校验生日
    if(checkBirthday(card) === false)
    {
        alert('您输入的身份证号码生日不正确,请重新输入');
        document.getElementById('card_no').focus();
        return false;
    }
    //检验位的检测
    if(checkParity(card) === false)
    {
        alert('您的身份证校验位不正确,请重新输入');
        document.getElementById('card_no').focus();
        return false;
    }
    //alert('OK');
    return true;
};

//检查号码是否符合规范，包括长度，类型
isCardNo = function(card)
{
    //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
    var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
    if(reg.test(card) === false)
    {
        return false;
    }

    return true;
};

//取身份证前两位,校验省份
checkProvince = function(card)
{
    var province = card.substr(0,2);
    if(vcity[province] == undefined)
    {
        return false;
    }
    return true;
};

//检查生日是否正确
checkBirthday = function(card)
{
    var len = card.length;
    //身份证15位时，次序为省（3位）市（3位）年（2位）月（2位）日（2位）校验位（3位），皆为数字
    if(len == '15')
    {
        var re_fifteen = /^(\d{6})(\d{2})(\d{2})(\d{2})(\d{3})$/; 
        var arr_data = card.match(re_fifteen);
        var year = arr_data[2];
        var month = arr_data[3];
        var day = arr_data[4];
        var birthday = new Date('19'+year+'/'+month+'/'+day);
        return verifyBirthday('19'+year,month,day,birthday);
    }
    //身份证18位时，次序为省（3位）市（3位）年（4位）月（2位）日（2位）校验位（4位），校验位末尾可能为X
    if(len == '18')
    {
        var re_eighteen = /^(\d{6})(\d{4})(\d{2})(\d{2})(\d{3})([0-9]|X)$/;
        var arr_data = card.match(re_eighteen);
        var year = arr_data[2];
        var month = arr_data[3];
        var day = arr_data[4];
        var birthday = new Date(year+'/'+month+'/'+day);
        return verifyBirthday(year,month,day,birthday);
    }
    return false;
};

//校验日期
verifyBirthday = function(year,month,day,birthday)
{
    var now = new Date();
    var now_year = now.getFullYear();
    //年月日是否合理
    if(birthday.getFullYear() == year && (birthday.getMonth() + 1) == month && birthday.getDate() == day)
    {
        //判断年份的范围（3岁到100岁之间)
        var time = now_year - year;
        if(time >= 3 && time <= 100)
        {
            return true;
        }
        return false;
    }
    return false;
};

//校验位的检测
checkParity = function(card)
{
    //15位转18位
    card = changeFivteenToEighteen(card);
    var len = card.length;
    if(len == '18')
    {
        var arrInt = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); 
        var arrCh = new Array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'); 
        var cardTemp = 0, i, valnum; 
        for(i = 0; i < 17; i ++) 
        { 
            cardTemp += card.substr(i, 1) * arrInt[i]; 
        } 
        valnum = arrCh[cardTemp % 11]; 
        if (valnum == card.substr(17, 1)) 
        {
            return true;
        }
        return false;
    }
    return false;
};

//15位转18位身份证号
changeFivteenToEighteen = function(card)
{
    if(card.length == '15')
    {
        var arrInt = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); 
        var arrCh = new Array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'); 
        var cardTemp = 0, i;   
        card = card.substr(0, 6) + '19' + card.substr(6, card.length - 6);
        for(i = 0; i < 17; i ++) 
        { 
            cardTemp += card.substr(i, 1) * arrInt[i]; 
        } 
        card += arrCh[cardTemp % 11]; 
        return card;
    }
    return card;
};



</script>

</head>

<body>


<?php
require(ECMS_PATH.'e/template/incfile/header.php');
?>



<div class="fullBox">
<div class="fullBoxTop">
<div class="boxTitleReg">注册会员</div>
</div>
<div class="fullBoxMid">
<div class="regBox">
<div class="regLeft">

  <form name=userinfoform method=post enctype="multipart/form-data" action='../doaction.php' onSubmit='return checktheform()'>
    <input type=hidden name=enews value=register>
	 <input name="groupid" type="hidden" id="groupid" value="<?=$groupid?>">
        <input name="tobind" type="hidden" id="tobind" value="<?=$tobind?>">
        <table class="loginTab regTab">
          <tr>
            <th>用户名：</th>
            <td><input type="text" id="username" name="username" maxlength="30" class="loginInp" /></td>
            <td><p id="_userid">您常用的用户名，6-30个英文字符或数字</p></td>
          </tr>
          <tr>
            <th>设置密码：</th>
            <td><input type="password" id="password" name="password" maxlength="20" class="loginInp" /></td>
            <td><p id="passwordTips">密码6-30位，区分大小写</p></td>
          </tr>
          <tr>
            <th>确认密码：</th>
            <td><input value="" id="repassword" name="repassword" type="password" maxlength="20" class="loginInp" />
            </td>
            <td><p id="_userpwdok">请输入和以上相同的密码</p></td>
          </tr>
		  
		   <tr>
            <th>手机号：</th>
            <td><input value="" id="phone" name="phone" type="text" maxlength="20" class="loginInp" />
            </td>
            <td><p id="_userpwdok">请输入正确的手机号</p></td>
          </tr>

          <tr>
            <th>真实姓名：</th>
            <td><input value="" id="card_no" name="truename" type="text" maxlength="20" class="loginInp" />
            </td>
            <td><p id="_userpwdok">请输入真实姓名</p></td>
          </tr>

	  
          <tr>
            <th>身份证号：</th>
            <td><input value="" id="card_no" name="card_no" type="text" maxlength="20" class="loginInp" />
            </td>
            <td><p id="_userpwdok">请输入正确的身份证号</p></td>
          </tr>

        
  		  
          <tr>
            <th>E-Mail：</th>
            <td><input name="email" type="text" id="email" maxlength="50" class="loginInp" /></td>
            <td><p id="_email">请输入您常用的邮箱，便于忘记密码时取回</p></td>
          </tr>
          <?
	if($public_r['regkey_ok'])
	{
	?>
          <tr>
            <th>验证码：</th>
            <td height="25" bgcolor="#FFFFFF"><input name="key" type="text" id="key" size="6"  class="loginInp loginInp2" />
                <img  border="0" align="absmiddle" src="../../ShowKey/?v=reg" /></td>
          </tr>
          <?
	}	
	?>
          <tr>
            <th>&nbsp;</th>
            <td><input type="submit" name="Submit" id="register" class="regBtnBig" value=""></td>
            <td>&nbsp;</td>
          </tr>
		  <tr>
            <th>&nbsp;</th>
            <td><input type="checkbox" id="checked"  checked="checked"/>
            <u><a href="/about/xieyi.html" target="_blank">同意注册协议</a></u></td>
            <td>&nbsp;</td>
          </tr>
        </table>
  </form>
</div>

  <script type="text/javascript">

var checked=document.getElementById("checked")
var register=document.getElementById("register")
register.onclick=function(){
        if(checked.checked==true){
                }
           else{
		     alert("请同意注册协议")
                return false
                }
        }
</script>


<div class="regRight">
<p class="loginNow"> 我已有账号，<a href="<?=$public_r['newsurl']?>e/member/login/">直接登录</a><br />
</p>
</div>
</div>
</div>
</div>




<?php
require(ECMS_PATH.'e/template/incfile/footer.php');
?>

</body>
</html>