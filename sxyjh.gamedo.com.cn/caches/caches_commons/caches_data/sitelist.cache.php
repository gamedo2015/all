<?php
return array (
  1 => 
  array (
    'siteid' => '1',
    'name' => '神仙伴我江湖路',
    'dirname' => '',
    'domain' => 'http://sxyjh.gamedo.com.cn/',
    'site_title' => '神仙伴我江湖路-神仙有江湖官方网站',
    'keywords' => '神仙有江湖官方网站,神仙,江湖、RPG,半即时制，动作，网游',
    'description' => '《神仙有江湖》是东方神话史诗级手游，穿越仙界而来！历时一年打造，国内顶尖制作团队千万重金打造的神话题材游戏邀你来战',
    'release_point' => '',
    'default_style' => 'default',
    'template' => 'default',
    'setting' => 'array (
  \'upload_maxsize\' => \'2048\',
  \'upload_allowext\' => \'jpg|jpeg|gif|bmp|png|doc|docx|xls|xlsx|ppt|pptx|pdf|txt|rar|zip|swf\',
  \'watermark_enable\' => \'0\',
  \'watermark_minwidth\' => \'300\',
  \'watermark_minheight\' => \'300\',
  \'watermark_img\' => \'statics/images/water//mark.png\',
  \'watermark_pct\' => \'85\',
  \'watermark_quality\' => \'80\',
  \'watermark_pos\' => \'9\',
)',
    'uuid' => 'c4d472c0-1e21-11e5-8602-00163e002072',
    'url' => 'http://sxyjh.gamedo.com.cn/',
  ),
);
?>