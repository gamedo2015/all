<?php defined('IN_PHPCMS') or exit('No permission resources.'); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<title><?php if(isset($SEO['title']) && !empty($SEO['title'])) { ?><?php echo $SEO['title'];?><?php } ?><?php echo $SEO['site_title'];?></title>
<meta name="keywords" content="<?php echo $SEO['keyword'];?>">
<meta name="description" content="<?php echo $SEO['description'];?>">
<link href="<?php echo CSS_PATH;?>gamedo_sxyjh/menu.css" rel="stylesheet" type="text/css">
<link href="<?php echo CSS_PATH;?>gamedo_sxyjh/news.css" rel="stylesheet" type="text/css">
    </head>
<body>
<?php include template("content","header_sxyjh"); ?>
<div class="list_box">
  <div class="list">
   <?php include template("content","left"); ?>
    <div class="right">
    <div class="title">
    首页 ><?php echo catpos($catid);?><?php echo $title;?> 
    </div>
    <div class="zhengwen">
    <div style="margin:0 auto; width:630px;">
    <div class="mingzi"><?php echo $title;?> </div>
    <div class="date"><?php echo date('Y-m-d',strtotime($updatetime));?></div>
    <div class="line"></div>
   <?php echo $content;?>
    </div>
          <p class="f14">
                <strong>上一篇：</strong><a href="<?php echo $previous_page['url'];?>"><?php echo $previous_page['title'];?></a><br />
                <strong>下一篇：</strong><a href="<?php echo $next_page['url'];?>"><?php echo $next_page['title'];?></a>
            </p>
    </div>
    </div>
   </div>
    </div>
<div style=" width:1000px;margin:30px auto 30px; color:#000; font-size:12px;">
  <table border="0" align="center" cellpadding="0" cellspacing="0" width="1000">
    <tr>
      <td align="center">关于掌上纵横 | 联系我们 | 用户协议 | 法律声明</td>
    </tr>
    <tr>
      <td align="center">掌上纵横信息技术（北京）有限公司版权所有   京ICP备11042079-1</td>
    </tr>
    <tr>
      <td align="center">Copyright © 2015 gamedo Co.,Ltd.All Rights reserved.</td>
    </tr>
    <tr>
      <td align="center">地址：北京市海淀区花园路13号129楼210室   联系电话 010-87521340</td>
    </tr>
  </table>
</div>
</body>
</html>
