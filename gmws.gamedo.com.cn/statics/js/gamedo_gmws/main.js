//弹出框效果
$(function(){
	//$("#content a").click(function(){
	//	alert("敬请期待！")
	//	return false;	
	//});
	
	$(".newsTab a").hover(function(){
		var n = $(this).attr("tip");
		$(".newsTab a").removeClass("hover");
		$(this).addClass("hover");
		$(".newsList").hide();
		$(".newsList").eq(n).show();
	});
	$("#btn").click(function(){
		$.simpleDialog({
		   skin:false,
		   content: '#showdiv',
		   id : 'showdiv'
		});						 
	});
	$("#btns").click(function(){
		$.simpleDialog.close('showdiv');
	});
	
	$(".mediaTab span").click(function(){
		var n = $(this).attr("tip");
		var addAry = $(this).attr("addAry");
		$(".mediaTab span").removeClass("hover");
		$(this).addClass("hover");	
		$(".mediaIframe iframe").attr("src",addAry);
	});
	
	if($("#titleName").size() > 0){
		var tips = $("#titleName").attr("tip");
		$(".navBox a").eq(tips).addClass("hover");	
	};
	
	if($("#news_text a").size() > 0){
		cont_txt();
	};
	
	$(".sizeZoom a").click(function(){
		$(this).addClass("red").siblings().removeClass("red");
	});
	
	if($(".textList p").size() > 0){
		$(".textList p:nth-child(5n)").addClass('borbm'); 
		$(".textList p:nth-child(20n)").removeClass('borbm'); 
	};
	
	$(".enjoyList a").click(function(){
		var videoLink = $(this).attr("videoLink");
		$.simpleDialog({
		   skin:false,
		   content: '#showVideo',
		   id : 'showVideo'
		});	
		addVideoPlayer('videoPlay', 640, 480,
            {
                playlist:videoLink,
                autoPlay:true
            }
        );

	});
	$("#closeVideo").click(function(){
		$.simpleDialog.close('showVideo');
	});
	
	$(".menuList").hover(function(){
		$(this).children(".menuShow").show();	
	},function(){
		$(".menuShow").hide();	
	});
});
function sizeZoom(size){
	$(".newstext").css({"font-size":size+"px","line-height":(size*2-2) + "px"});
};

function cont_txt(){
	var textbt = document.getElementById("titleName").innerHTML;
	var news_text = document.getElementById("news_text").getElementsByTagName("a");
	for(var i=0; i<news_text.length; i++){
		if(news_text[i].innerHTML == textbt){
			news_text[i].className = "hover";
		};
	};	
};

//按钮滑过效果
$(document).ready(function(){
	$('.navBox a').css('opacity', 0).hover(function(){
		$(this).stop().fadeTo(400, 1);
	},function(){
		$(this).stop().fadeTo(350, 0);
	});
});

//上下左右滚动
//$('#marquee').marquee({isEqual:false,direction: 'up'});

$("id").click(function(){
	$("body,html").animate({scrollTop:1100},500);				   
});


//获取地址栏ID
function getParam(param) {
	var u=window.location.toString(),
		r=new RegExp("\\?(?:.+&)?" + param + "=(.*?)(?:&.*)?$");
	if(param=="usurl"){
		return u.substring(u.indexOf("usurl=")+6);
	}   
	var m = u.match(r);   
	return m ? decodeURIComponent(m[1]) : "";    
};
//alert(getParam('id'))


//复制按钮回调函数
//addCopyButton('copyButton',200,15,'setClipboard');
function setClipboard(){
	var _string='复制按钮测试_'+Math.random();
	alert('复制内容：'+_string);
	return _string;
}