<?php
return array (
  1 => 
  array (
    'siteid' => '1',
    'name' => '鬼面无双',
    'dirname' => '',
    'domain' => 'http://gmws.gamedo.com.cn/',
    'site_title' => '《鬼面无双》官方网站-鬼面王者风 无双激战启',
    'keywords' => 'ARPG，动作，武侠，三国，无双，打击感',
    'description' => '群雄逐鹿，诸侯争霸，谁能平定这破碎的山河？鬼面杀神横空出世。《鬼面无双》是以中国古代时期为背景制作的角色扮演全民动作手游，三大阵营对战、五大女神侍宠相伴，千套炫酷时装，上古战场大型PVP，无尽副本，数百万玩家同台PK，谁与争锋?现在开始，史诗由你铸就!最火爆的全民动作手游等你来战！',
    'release_point' => '',
    'default_style' => 'default',
    'template' => 'default',
    'setting' => 'array (
  \'upload_maxsize\' => \'2048\',
  \'upload_allowext\' => \'jpg|jpeg|gif|bmp|png|doc|docx|xls|xlsx|ppt|pptx|pdf|txt|rar|zip|swf\',
  \'watermark_enable\' => \'0\',
  \'watermark_minwidth\' => \'300\',
  \'watermark_minheight\' => \'300\',
  \'watermark_img\' => \'statics/images/water//mark.png\',
  \'watermark_pct\' => \'85\',
  \'watermark_quality\' => \'80\',
  \'watermark_pos\' => \'9\',
)',
    'uuid' => '0d1180ed-13f7-11e5-825b-5404a6a0e00f',
    'url' => 'http://gmws.gamedo.com.cn/',
  ),
);
?>