<?php
return array (
  1 => 
  array (
    'catid' => '1',
    'siteid' => '1',
    'type' => '1',
    'modelid' => '0',
    'parentid' => '0',
    'arrparentid' => '0',
    'child' => '1',
    'arrchildid' => '1,2,3,4,5',
    'catname' => '网站介绍',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => '',
    'catdir' => 'about',
    'url' => 'http://gmws.gamedo.com.cn/html/about/',
    'items' => '0',
    'hits' => '0',
    'setting' => 'array (
  \'ishtml\' => \'1\',
  \'template_list\' => \'default\',
  \'page_template\' => \'page\',
  \'meta_title\' => \'\',
  \'meta_keywords\' => \'\',
  \'meta_description\' => \'\',
  \'category_ruleid\' => \'1\',
  \'show_ruleid\' => \'\',
  \'repeatchargedays\' => \'1\',
)',
    'listorder' => '1',
    'ismenu' => '0',
    'sethtml' => '0',
    'letter' => 'wangzhanjieshao',
    'usable_type' => '',
    'create_to_html_root' => NULL,
    'ishtml' => '1',
    'content_ishtml' => NULL,
    'category_ruleid' => '1',
    'show_ruleid' => '',
    'workflowid' => NULL,
    'isdomain' => '0',
  ),
  2 => 
  array (
    'catid' => '2',
    'siteid' => '1',
    'type' => '1',
    'modelid' => '0',
    'parentid' => '1',
    'arrparentid' => '0,1',
    'child' => '0',
    'arrchildid' => '2',
    'catname' => '关于我们',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => 'about/',
    'catdir' => 'aboutus',
    'url' => 'http://gmws.gamedo.com.cn/html/about/aboutus/',
    'items' => '0',
    'hits' => '0',
    'setting' => 'array (
  \'ishtml\' => \'1\',
  \'template_list\' => \'default\',
  \'page_template\' => \'page\',
  \'meta_title\' => \'关于我们\',
  \'meta_keywords\' => \'关于我们\',
  \'meta_description\' => \'关于我们\',
  \'category_ruleid\' => \'1\',
  \'show_ruleid\' => \'\',
  \'repeatchargedays\' => \'1\',
)',
    'listorder' => '1',
    'ismenu' => '1',
    'sethtml' => '0',
    'letter' => 'guanyuwomen',
    'usable_type' => '',
    'create_to_html_root' => NULL,
    'ishtml' => '1',
    'content_ishtml' => NULL,
    'category_ruleid' => '1',
    'show_ruleid' => '',
    'workflowid' => NULL,
    'isdomain' => '0',
  ),
  3 => 
  array (
    'catid' => '3',
    'siteid' => '1',
    'type' => '1',
    'modelid' => '0',
    'parentid' => '1',
    'arrparentid' => '0,1',
    'child' => '0',
    'arrchildid' => '3',
    'catname' => '联系方式',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => 'about/',
    'catdir' => 'contactus',
    'url' => 'http://gmws.gamedo.com.cn/html/about/contactus/',
    'items' => '0',
    'hits' => '0',
    'setting' => 'array (
  \'ishtml\' => \'1\',
  \'template_list\' => \'default\',
  \'page_template\' => \'page\',
  \'meta_title\' => \'联系方式\',
  \'meta_keywords\' => \'联系方式\',
  \'meta_description\' => \'联系方式\',
  \'category_ruleid\' => \'1\',
  \'show_ruleid\' => \'\',
  \'repeatchargedays\' => \'1\',
)',
    'listorder' => '2',
    'ismenu' => '1',
    'sethtml' => '0',
    'letter' => 'lianxifangshi',
    'usable_type' => '',
    'create_to_html_root' => NULL,
    'ishtml' => '1',
    'content_ishtml' => NULL,
    'category_ruleid' => '1',
    'show_ruleid' => '',
    'workflowid' => NULL,
    'isdomain' => '0',
  ),
  4 => 
  array (
    'catid' => '4',
    'siteid' => '1',
    'type' => '1',
    'modelid' => '0',
    'parentid' => '1',
    'arrparentid' => '0,1',
    'child' => '0',
    'arrchildid' => '4',
    'catname' => '版权声明',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => 'about/',
    'catdir' => 'copyright',
    'url' => 'http://gmws.gamedo.com.cn/index.php?m=content&c=index&a=lists&catid=4',
    'items' => '0',
    'hits' => '0',
    'setting' => 'array (
  \'ishtml\' => \'0\',
  \'template_list\' => \'default\',
  \'page_template\' => \'page\',
  \'meta_title\' => \'版权声明\',
  \'meta_keywords\' => \'版权声明\',
  \'meta_description\' => \'版权声明\',
  \'category_ruleid\' => \'6\',
  \'show_ruleid\' => \'\',
  \'repeatchargedays\' => \'1\',
)',
    'listorder' => '3',
    'ismenu' => '1',
    'sethtml' => '0',
    'letter' => 'banquanshengming',
    'usable_type' => '',
    'create_to_html_root' => NULL,
    'ishtml' => '0',
    'content_ishtml' => NULL,
    'category_ruleid' => '6',
    'show_ruleid' => '',
    'workflowid' => NULL,
    'isdomain' => '0',
  ),
  5 => 
  array (
    'catid' => '5',
    'siteid' => '1',
    'type' => '1',
    'modelid' => '0',
    'parentid' => '1',
    'arrparentid' => '0,1',
    'child' => '0',
    'arrchildid' => '5',
    'catname' => '招聘信息',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => 'about/',
    'catdir' => 'hr',
    'url' => 'http://gmws.gamedo.com.cn/html/about/hr/',
    'items' => '0',
    'hits' => '0',
    'setting' => 'array (
  \'ishtml\' => \'1\',
  \'template_list\' => \'default\',
  \'page_template\' => \'page\',
  \'meta_title\' => \'\',
  \'meta_keywords\' => \'\',
  \'meta_description\' => \'\',
  \'category_ruleid\' => \'1\',
  \'show_ruleid\' => \'\',
  \'repeatchargedays\' => \'1\',
)',
    'listorder' => '4',
    'ismenu' => '1',
    'sethtml' => '0',
    'letter' => 'zhaopinxinxi',
    'usable_type' => '',
    'create_to_html_root' => NULL,
    'ishtml' => '1',
    'content_ishtml' => NULL,
    'category_ruleid' => '1',
    'show_ruleid' => '',
    'workflowid' => NULL,
    'isdomain' => '0',
  ),
  9 => 
  array (
    'catid' => '9',
    'siteid' => '1',
    'type' => '0',
    'modelid' => '1',
    'parentid' => '0',
    'arrparentid' => '0',
    'child' => '0',
    'arrchildid' => '9',
    'catname' => '新闻',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => '',
    'catdir' => 'news',
    'url' => 'http://gmws.gamedo.com.cn/news/',
    'items' => '3',
    'hits' => '0',
    'setting' => 'array (
  \'workflowid\' => \'\',
  \'ishtml\' => \'1\',
  \'content_ishtml\' => \'1\',
  \'create_to_html_root\' => \'1\',
  \'template_list\' => \'default\',
  \'category_template\' => \'category\',
  \'list_template\' => \'list_gmws\',
  \'show_template\' => \'show_gmws\',
  \'meta_title\' => \'\',
  \'meta_keywords\' => \'\',
  \'meta_description\' => \'\',
  \'presentpoint\' => \'1\',
  \'defaultchargepoint\' => \'0\',
  \'paytype\' => \'0\',
  \'repeatchargedays\' => \'1\',
  \'category_ruleid\' => \'1\',
  \'show_ruleid\' => \'12\',
)',
    'listorder' => '9',
    'ismenu' => '1',
    'sethtml' => '1',
    'letter' => 'xinwen',
    'usable_type' => '',
    'create_to_html_root' => '1',
    'ishtml' => '1',
    'content_ishtml' => '1',
    'category_ruleid' => '1',
    'show_ruleid' => '12',
    'workflowid' => '',
    'isdomain' => '0',
  ),
  10 => 
  array (
    'catid' => '10',
    'siteid' => '1',
    'type' => '0',
    'modelid' => '1',
    'parentid' => '0',
    'arrparentid' => '0',
    'child' => '0',
    'arrchildid' => '10',
    'catname' => '公告',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => '',
    'catdir' => 'gongg',
    'url' => 'http://gmws.gamedo.com.cn/gongg/',
    'items' => '0',
    'hits' => '0',
    'setting' => 'array (
  \'workflowid\' => \'\',
  \'ishtml\' => \'1\',
  \'content_ishtml\' => \'1\',
  \'create_to_html_root\' => \'1\',
  \'template_list\' => \'default\',
  \'category_template\' => \'category\',
  \'list_template\' => \'list_gmws\',
  \'show_template\' => \'show_gmws\',
  \'meta_title\' => \'\',
  \'meta_keywords\' => \'\',
  \'meta_description\' => \'\',
  \'presentpoint\' => \'1\',
  \'defaultchargepoint\' => \'0\',
  \'paytype\' => \'0\',
  \'repeatchargedays\' => \'1\',
  \'category_ruleid\' => \'1\',
  \'show_ruleid\' => \'12\',
)',
    'listorder' => '10',
    'ismenu' => '1',
    'sethtml' => '1',
    'letter' => 'gonggao',
    'usable_type' => '',
    'create_to_html_root' => '1',
    'ishtml' => '1',
    'content_ishtml' => '1',
    'category_ruleid' => '1',
    'show_ruleid' => '12',
    'workflowid' => '',
    'isdomain' => '0',
  ),
  11 => 
  array (
    'catid' => '11',
    'siteid' => '1',
    'type' => '0',
    'modelid' => '1',
    'parentid' => '0',
    'arrparentid' => '0',
    'child' => '0',
    'arrchildid' => '11',
    'catname' => '活动',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => '',
    'catdir' => 'huod',
    'url' => 'http://gmws.gamedo.com.cn/huod/',
    'items' => '1',
    'hits' => '0',
    'setting' => 'array (
  \'workflowid\' => \'\',
  \'ishtml\' => \'1\',
  \'content_ishtml\' => \'1\',
  \'create_to_html_root\' => \'1\',
  \'template_list\' => \'default\',
  \'category_template\' => \'category\',
  \'list_template\' => \'list_gmws\',
  \'show_template\' => \'show_gmws\',
  \'meta_title\' => \'\',
  \'meta_keywords\' => \'\',
  \'meta_description\' => \'\',
  \'presentpoint\' => \'1\',
  \'defaultchargepoint\' => \'0\',
  \'paytype\' => \'0\',
  \'repeatchargedays\' => \'1\',
  \'category_ruleid\' => \'1\',
  \'show_ruleid\' => \'12\',
)',
    'listorder' => '11',
    'ismenu' => '1',
    'sethtml' => '1',
    'letter' => 'huodong',
    'usable_type' => '',
    'create_to_html_root' => '1',
    'ishtml' => '1',
    'content_ishtml' => '1',
    'category_ruleid' => '1',
    'show_ruleid' => '12',
    'workflowid' => '',
    'isdomain' => '0',
  ),
  12 => 
  array (
    'catid' => '12',
    'siteid' => '1',
    'type' => '0',
    'modelid' => '12',
    'parentid' => '0',
    'arrparentid' => '0',
    'child' => '0',
    'arrchildid' => '12',
    'catname' => '游戏原画',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => '',
    'catdir' => 'yuanhua',
    'url' => 'http://gmws.gamedo.com.cn/yuanhua/',
    'items' => '5',
    'hits' => '0',
    'setting' => 'array (
  \'workflowid\' => \'\',
  \'ishtml\' => \'1\',
  \'content_ishtml\' => \'1\',
  \'create_to_html_root\' => \'1\',
  \'template_list\' => \'default\',
  \'category_template\' => \'category\',
  \'list_template\' => \'list_gmws_pic\',
  \'show_template\' => \'show_gmws\',
  \'meta_title\' => \'\',
  \'meta_keywords\' => \'\',
  \'meta_description\' => \'\',
  \'presentpoint\' => \'1\',
  \'defaultchargepoint\' => \'0\',
  \'paytype\' => \'0\',
  \'repeatchargedays\' => \'1\',
  \'category_ruleid\' => \'1\',
  \'show_ruleid\' => \'12\',
)',
    'listorder' => '12',
    'ismenu' => '1',
    'sethtml' => '1',
    'letter' => 'youxiyuanhua',
    'usable_type' => '',
    'create_to_html_root' => '1',
    'ishtml' => '1',
    'content_ishtml' => '1',
    'category_ruleid' => '1',
    'show_ruleid' => '12',
    'workflowid' => '',
    'isdomain' => '0',
  ),
  13 => 
  array (
    'catid' => '13',
    'siteid' => '1',
    'type' => '0',
    'modelid' => '12',
    'parentid' => '0',
    'arrparentid' => '0',
    'child' => '0',
    'arrchildid' => '13',
    'catname' => '游戏截图',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => '',
    'catdir' => 'jietu',
    'url' => 'http://gmws.gamedo.com.cn/jietu/',
    'items' => '2',
    'hits' => '0',
    'setting' => 'array (
  \'workflowid\' => \'\',
  \'ishtml\' => \'1\',
  \'content_ishtml\' => \'1\',
  \'create_to_html_root\' => \'1\',
  \'template_list\' => \'default\',
  \'category_template\' => \'category\',
  \'list_template\' => \'list_gmws_pic\',
  \'show_template\' => \'show_gmws\',
  \'meta_title\' => \'\',
  \'meta_keywords\' => \'\',
  \'meta_description\' => \'\',
  \'presentpoint\' => \'1\',
  \'defaultchargepoint\' => \'0\',
  \'paytype\' => \'0\',
  \'repeatchargedays\' => \'1\',
  \'category_ruleid\' => \'1\',
  \'show_ruleid\' => \'12\',
)',
    'listorder' => '13',
    'ismenu' => '0',
    'sethtml' => '1',
    'letter' => 'youxijietu',
    'usable_type' => '',
    'create_to_html_root' => '1',
    'ishtml' => '1',
    'content_ishtml' => '1',
    'category_ruleid' => '1',
    'show_ruleid' => '12',
    'workflowid' => '',
    'isdomain' => '0',
  ),
  14 => 
  array (
    'catid' => '14',
    'siteid' => '1',
    'type' => '0',
    'modelid' => '12',
    'parentid' => '0',
    'arrparentid' => '0',
    'child' => '0',
    'arrchildid' => '14',
    'catname' => '首页三张轮播图',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => '',
    'catdir' => 'lunbo',
    'url' => 'http://gmws.gamedo.com.cn/index.php?m=content&c=index&a=lists&catid=14',
    'items' => '3',
    'hits' => '0',
    'setting' => 'array (
  \'workflowid\' => \'\',
  \'ishtml\' => \'0\',
  \'content_ishtml\' => \'0\',
  \'create_to_html_root\' => \'0\',
  \'template_list\' => \'default\',
  \'category_template\' => \'category\',
  \'list_template\' => \'\',
  \'show_template\' => \'\',
  \'meta_title\' => \'\',
  \'meta_keywords\' => \'\',
  \'meta_description\' => \'\',
  \'presentpoint\' => \'1\',
  \'defaultchargepoint\' => \'0\',
  \'paytype\' => \'0\',
  \'repeatchargedays\' => \'1\',
  \'category_ruleid\' => \'6\',
  \'show_ruleid\' => \'16\',
)',
    'listorder' => '14',
    'ismenu' => '1',
    'sethtml' => '0',
    'letter' => 'shouyesanzhanglunbotu',
    'usable_type' => '',
    'create_to_html_root' => '0',
    'ishtml' => '0',
    'content_ishtml' => '0',
    'category_ruleid' => '6',
    'show_ruleid' => '16',
    'workflowid' => '',
    'isdomain' => '0',
  ),
  15 => 
  array (
    'catid' => '15',
    'siteid' => '1',
    'type' => '0',
    'modelid' => '13',
    'parentid' => '0',
    'arrparentid' => '0',
    'child' => '0',
    'arrchildid' => '15',
    'catname' => '首页媒体镶嵌',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => '',
    'catdir' => 'xiangqian',
    'url' => 'http://gmws.gamedo.com.cn/index.php?m=content&c=index&a=lists&catid=15',
    'items' => '38',
    'hits' => '0',
    'setting' => 'array (
  \'workflowid\' => \'\',
  \'ishtml\' => \'0\',
  \'content_ishtml\' => \'0\',
  \'create_to_html_root\' => \'0\',
  \'template_list\' => \'default\',
  \'category_template\' => \'category\',
  \'list_template\' => \'\',
  \'show_template\' => \'\',
  \'meta_title\' => \'\',
  \'meta_keywords\' => \'\',
  \'meta_description\' => \'\',
  \'presentpoint\' => \'1\',
  \'defaultchargepoint\' => \'0\',
  \'paytype\' => \'0\',
  \'repeatchargedays\' => \'1\',
  \'category_ruleid\' => \'6\',
  \'show_ruleid\' => \'16\',
)',
    'listorder' => '15',
    'ismenu' => '1',
    'sethtml' => '0',
    'letter' => 'shouyemeitixiangqian',
    'usable_type' => '',
    'create_to_html_root' => '0',
    'ishtml' => '0',
    'content_ishtml' => '0',
    'category_ruleid' => '6',
    'show_ruleid' => '16',
    'workflowid' => '',
    'isdomain' => '0',
  ),
  16 => 
  array (
    'catid' => '16',
    'siteid' => '1',
    'type' => '0',
    'modelid' => '13',
    'parentid' => '0',
    'arrparentid' => '0',
    'child' => '0',
    'arrchildid' => '16',
    'catname' => '首页媒体',
    'style' => '',
    'image' => '',
    'description' => '',
    'parentdir' => '',
    'catdir' => 'meiti',
    'url' => 'http://gmws.gamedo.com.cn/index.php?m=content&c=index&a=lists&catid=16',
    'items' => '0',
    'hits' => '0',
    'setting' => 'array (
  \'workflowid\' => \'\',
  \'ishtml\' => \'0\',
  \'content_ishtml\' => \'0\',
  \'create_to_html_root\' => \'0\',
  \'template_list\' => \'default\',
  \'category_template\' => \'category\',
  \'list_template\' => \'list_gmws\',
  \'show_template\' => \'show_gmws\',
  \'meta_title\' => \'\',
  \'meta_keywords\' => \'\',
  \'meta_description\' => \'\',
  \'presentpoint\' => \'1\',
  \'defaultchargepoint\' => \'0\',
  \'paytype\' => \'0\',
  \'repeatchargedays\' => \'1\',
  \'category_ruleid\' => \'6\',
  \'show_ruleid\' => \'16\',
)',
    'listorder' => '16',
    'ismenu' => '1',
    'sethtml' => '0',
    'letter' => 'shouyemeiti',
    'usable_type' => '',
    'create_to_html_root' => '0',
    'ishtml' => '0',
    'content_ishtml' => '0',
    'category_ruleid' => '6',
    'show_ruleid' => '16',
    'workflowid' => '',
    'isdomain' => '0',
  ),
);
?>