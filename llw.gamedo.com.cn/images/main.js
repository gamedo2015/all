<!--选项卡-->
function tab(list,listEles,num,contlist,cls){
    list = document.getElementById(list).getElementsByTagName(listEles);
    for(var i=0;i<list.length;i++){
        if(i+1==num){
            list[i].className = 'hot_on';
            document.getElementById(contlist+(i+1)).className = 'show';
        }else{
            list[i].className = '';
            document.getElementById(contlist+(i+1)).className = 'hide';
        }
    }
}

function showHideOrder(obj1,obj2){
    $("#"+obj1+"order").css("display","block");
    $("#"+obj1+"ordertitle").addClass("on");
    $("#"+obj2+"order").css("display","none");
    $("#"+obj2+"ordertitle").removeClass("on");
    $("#ordertype").val(obj1);
}

$(function(){
    //回车键 提交表单
    document.onkeydown = function(e){
        var ev = document.all ? window.event : e;
        if(ev.keyCode==13) {
            btn_log = $("#username").val();
            btn_reg = $("#reg_name").val();
            bind_email = $("#change_email").val();
            conn_bind = $("#conn_name").val();
            if(btn_log!==undefined){
                checkLogin();
            }
            if(btn_reg!==undefined){
                checkReg();
            }
            if(bind_email!==undefined){
                checkChEmail();
            }
            if(conn_bind!==undefined){
                checkConnBind();
            }
        }
    }
    
    //新手卡页面加载时，加载游戏服信息
    product_id=$("#products").val();
    if(product_id!==undefined){
        sers = $("#pro_id_"+product_id).html();
        $("#servers").html(sers);
    }
    
    
    
    //第三方联合登陆用户 绑定帐号
    $("#conn_name").blur(function(){
        val = $("#conn_name").val();
        if(val==""){
            msg = $("#conn_name_empty").val();
            $("#conn_name_msg").html(msg);
        }else{
            patten = /^[a-zA-Z][\w]{5,50}$/;
            if(!patten.test(val)){
                msg = $('#conn_name_error').val();
                $("#conn_name_msg").html(msg);
            }else{
                $.post("/user/checkRegist", {
                    username: val
                },
                function (data){
                    if(data){
                        eval("var json= [" + data + "];");
                        msg = json[0].msg;
                        if(msg){
                            $("#conn_name_msg").html(msg);
                        }else{
                            $("#conn_name_msg").html("<font color=green>√</font>");
                        }
                    }else{
                        $("#conn_name_msg").html("<font color=green>√</font>");
                    }
                });
            }
        }
    });
    $("#conn_pwd").blur(function(){
        val = $("#conn_pwd").val();
        if(val==""){
            msg = $("#conn_pwd_empty").val();
            $("#conn_pwd_msg").html(msg);
        }else{
            len = val.length;
            if(len<6||len>20){
                msg = $('#conn_pwd_error').val();
                $("#conn_pwd_msg").html(msg);
            }else{
                $("#conn_pwd_msg").html("<font color=green>√</font>");
            }
        }
    });
    
    $("#conn_repwd").blur(function(){
        val = $("#conn_repwd").val();
        if(val==""){
            msg = $("#conn_repwd_empty").val();
            $("#conn_repwd_msg").html(msg);
        }else{
            len = val.length;
            if(len<6||len>20){
                msg = $('#conn_repwd_error').val();
                $("#conn_repwd_msg").html(msg);
            }else{
                pwd = $("#conn_pwd").val();
                if(pwd!=val){
                    msg = $('#conn_repwd_pwd_eq').val();
                    $("#conn_repwd_msg").html(msg);
                }else{
                    $("#conn_repwd_msg").html("<font color=green>√</font>");
                }
            }
        }
    });
    $("#conn_email").blur(function(){
        val = $("#conn_email").val();
        if(val==""){
            msg = $("#conn_email_empty").val();
            $("#conn_email_msg").html(msg);
        }else{
            patten = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            if(!patten.test(val)){
                msg = $('#conn_email_error').val();
                $("#conn_email_msg").html(msg);
            }else{
                $.post("/user/checkRegist", {
                    email: val
                },
                function (data){
                    if(data){
                        eval("var json= [" + data + "];");
                        msg = json[0].msg;
                        if(msg){
                            $("#conn_email_msg").html(msg);
                        }else{
                            $("#conn_email_msg").html("<font color=green>√</font>");
                        }
                    }else{
                        $("#conn_email_msg").html("<font color=green>√</font>");
                    }
                });
            }
        }
    });
    
    $("#conn_bind_btn").click(function(){
        checkConnBind();
    });
    
    //登录验证
    $("#username").blur(function(){
        checkUserName();
    });
    $("#password").blur(function(){
        checkPassword();
    });
    $("#vcode").blur(function(){
        checkVCode();
    });
    $("#checklogform").click(function(){
        checkLogin();
    });
    
    //注册
    $("#reg_name").blur(function(){
        val = $("#reg_name").val();
        if(val == ""){
            $("#reg_name_msg").attr("class","sptip_w");
            msg = $('#reg_name_empty').val();
            $("#reg_name_msg").html(msg);
        }else{
            patten = /^[a-zA-Z][\w]{5,50}$/;
            if(!patten.test(val)){
                $("#reg_name_msg").attr("class","sptip_w");
                msg = $('#reg_name_error').val();
                $("#reg_name_msg").html(msg);
            }else{
                $.post("/user/checkRegist", {
                    username: val
                },
                function (data){
                    if(data){
                        eval("var json= [" + data + "];");
                        msg = json[0].msg;
                        if(msg){
                            $("#reg_name_msg").attr("class","sptip_w");
                            $("#reg_name_msg").html(msg);
                        }else{
                            $("#reg_name_msg").attr("class","sptip");
                            $("#reg_name_msg").html("<font color=green>√</font>");
                        }
                    }else{
                        $("#reg_name_msg").attr("class","sptip");
                        $("#reg_name_msg").html("<font color=green>√</font>");
                    }
                });
            }
        }
    });
    $("#reg_pwd").blur(function(){
        val = $("#reg_pwd").val();
        if(val == ""){
            $("#reg_pwd_msg").attr("class","sptip_w");
            msg = $('#reg_pwd_empty').val();
            $("#reg_pwd_msg").html(msg);
        }else{
            len = val.length;
            if(len<6||len>20){
                $("#reg_pwd_msg").attr("class","sptip_w");
                msg = $('#reg_pwd_error').val();
                $("#reg_pwd_msg").html(msg);
            }else{
                $("#reg_pwd_msg").attr("class","sptip");
                $("#reg_pwd_msg").html("<font color=green>√</font>");
            }
        }
    });
    $("#reg_repwd").blur(function(){
        val = $("#reg_repwd").val();
        if(val == ""){
            $("#reg_repwd_msg").attr("class","sptip_w");
            msg = $('#reg_repwd_empty').val();
            $("#reg_repwd_msg").html(msg);
        }else{
            len = val.length;
            if(len<6||len>20){
                $("#reg_repwd_msg").attr("class","sptip_w");
                msg = $('#reg_repwd_error').val();
                $("#reg_repwd_msg").html(msg);
            }else{
                pwd = $("#reg_pwd").val();
                if(pwd!=val){
                    $("#reg_repwd_msg").attr("class","sptip_w");
                    msg = $('#reg_repwd_pwd_eq').val();
                    $("#reg_repwd_msg").html(msg);
                }else{
                    $("#reg_repwd_msg").attr("class","sptip");
                    $("#reg_repwd_msg").html("<font color=green>√</font>");
                }
            }
        }
    });
    
    $("#reg_email").blur(function(){
        val = $("#reg_email").val();
        if(val==""){
            $("#reg_email_msg").attr("class","sptip_w");
            msg = $('#reg_email_empty').val();
            $("#reg_email_msg").html(msg);
        }else{
            patten = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            if(!patten.test(val)){
                $("#reg_email_msg").attr("class","sptip_w");
                msg = $('#reg_email_error').val();
                $("#reg_email_msg").html(msg);
            }else{
                $.post("/user/checkRegist", {
                    email: val
                },
                function (data){
                    if(data){
                        eval("var json= [" + data + "];");
                        msg = json[0].msg;
                        if(msg){
                            $("#reg_email_msg").attr("class","sptip_w");
                            $("#reg_email_msg").html(msg);
                        }else{
                            $("#reg_email_msg").attr("class","sptip");
                            $("#reg_email_msg").html("<font color=green>√</font>");
                        }
                    }else{
                        $("#reg_email_msg").attr("class","sptip");
                        $("#reg_email_msg").html("<font color=green>√</font>");
                    }
                });
            }
        }
    });
    $("#reg_btn").click(function(){
        checkReg();
    });
    $("#change_email").blur(function(){
        email = $("#change_email").val();
        if(email==""){
            msg = $("#change_email_empty").val();
            $("#change_email_msg").attr("class","sptip_w");
            $("#change_email_msg").html(msg);
        }else{
            patten = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            if(!patten.test(email)){
                $("#change_email_msg").attr("class","sptip_w");
                msg = $('#change_email_error').val();
                $("#change_email_msg").html(msg);
            }else{
                $.post("/user/checkEmail", {
                    email: email
                },
                function (data){
                    if(data){
                        eval("var json= [" + data + "];");
                        msg = json[0].msg;
                        if(msg){
                            $("#change_email_msg").attr("class","sptip_w");
                            $("#change_email_msg").html(msg);
                        }else{
                            $("#change_email_msg").attr("class","sptip");
                            $("#change_email_msg").html("<font color=green>√</font>");
                        }
                    }else{
                        $("#change_email_msg").attr("class","sptip");
                        $("#change_email_msg").html("<font color=green>√</font>");
                    }
                });
            }
        }
    })
    $("#change_email_pwd").blur(function(){
        change_email_pwd = $("#change_email_pwd").val();
        if(change_email_pwd==""){
            msg = $("#change_email_pwd_empty").val();
            $("#change_email_pwd_msg").html(msg);
        }else{
            len = change_email_pwd.length;
            if(len<6||len>20){
                msg = $("#change_email_pwd_error").val();
                $("#change_email_pwd_msg").html(msg);
            }
        }
    })
    $("#change_email_btn").click(function(){
        checkChEmail();
    });
});

function getSerByProduct(){
    product_id = $("#products").val();
    sers = $("#pro_id_"+product_id).html();
    $("#servers").html(sers);
}

function checkConnBind(){
    
    conn_name = $("#conn_name").val();
    conn_pwd = $("#conn_pwd").val();
    conn_repwd = $("#conn_repwd").val();
    conn_email = $("#conn_email").val();
        
    if(conn_name==""){
        msg = $("#conn_name_empty").val();
        $("#conn_name_msg").html(msg);
        return false;
    }
    if(conn_pwd==""){
        msg = $("#conn_pwd_empty").val();
        $("#conn_pwd_msg").html(msg);
        return false;
    }
    if(conn_repwd==""){
        msg = $("#conn_repwd_empty").val();
        $("#conn_repwd_msg").html(msg);
        return false;
    }
    if(conn_email==""){
        msg = $("#conn_email_empty").val();
        $("#conn_email_msg").html(msg);
        return false;
    }
        
        
    patten_name = /^[a-zA-Z][\w]{5,50}$/;
    if(!patten_name.test(conn_name)){
        msg = $('#conn_name_error').val();
        $("#conn_name_msg").html(msg);
        return false;
    }
        
    len = conn_pwd.length;
    if(len<6||len>20){
        msg = $('#conn_pwd_error').val();
        $("#conn_pwd_msg").html(msg);
        return false;
    }
        
    relen = conn_repwd.length;
    if(relen<6||relen>20){
        msg = $('#conn_repwd_error').val();
        $("#conn_repwd_msg").html(msg);
        return false;
    }
        
    if(conn_pwd!=conn_repwd){
        msg = $('#conn_repwd_pwd_eq').val();
        $("#conn_repwd_msg").html(msg);
        return false;
    }
        
    patten_email = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if(!patten_email.test(conn_email)){
        msg = $('#conn_email_error').val();
        $("#conn_email_msg").html(msg);
        return false;
    }
        
    $.post("/user/checkRegist", {
        username: conn_name,
        email:conn_email
    },
    function (data){
        if(data){
            eval("var json= [" + data + "];");
            msg = json[0].msg;
            if(msg){
                eval("var json= [" + data + "];");
                $("#"+json[0].name+"_msg").attr("class","sptip_w");
                $("#"+json[0].name+"_msg").html(json[0].msg);
                return false;
            }else{
                $("#conn_name_msg").html("<font color=green>√</font>");
                $("#conn_bind_account").submit();
            }
        }else{
            $("#conn_name_msg").html("<font color=green>√</font>");
            $("#conn_bind_account").submit();
        }
    });
            
}

function checkChEmail(){
    email = $("#change_email").val();
    chemailpwd = $("#change_email_pwd").val();
    if(chemailpwd == ""){
        msg = $("#change_email_pwd_empty").val();
        $("#change_email_pwd_msg").html(msg);
        return false;
    }
    if(email==""){
        msg = $("#change_email_empty").val();
        $("#change_email_msg").attr("class","sptip_w");
        $("#change_email_msg").html(msg);
        return false;
    }
    len = change_email_pwd.length;
    if(len<6||len>20){
        msg = $("#change_email_pwd_error").val();
        $("#change_email_pwd_msg").html(msg);
        return false;
    }
            
    patten = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if(!patten.test(email)){
        $("#change_email_msg").attr("class","sptip_w");
        msg = $('#change_email_error').val();
        $("#change_email_msg").html(msg);
        return false;
    }
    
    $.post("/user/checkEmail", {
        email: email
    },
    function (data){
        if(data){
            eval("var json= [" + data + "];");
            msg = json[0].msg;
            if(msg){
                $("#change_email_msg").attr("class","sptip_w");
                $("#change_email_msg").html(msg);
                return false;
            }else{
                $("#change_email_msg").attr("class","sptip");
                $("#change_email_msg").html("<font color=green>√</font>");
                $("#chemail").submit();
            }
        }else{
            $("#change_email_msg").attr("class","sptip");
            $("#change_email_msg").html("<font color=green>√</font>");
            $("#chemail").submit();
        }
    });
}

function checkReg(){
    reg_name = $("#reg_name").val();
    reg_pwd = $("#reg_pwd").val();
    reg_repwd = $("#reg_repwd").val();
    reg_email = $("#reg_email").val();
    
    //非空验证
    if(reg_name==""){
        $("#reg_name_msg").attr("class","sptip_w");
        msg = $('#reg_name_empty').val();
        $("#reg_name_msg").html(msg);
        return false;
    }else{
    //$("#reg_name_msg").html("");
    }
        
    if(reg_pwd==""){
        $("#reg_pwd_msg").attr("class","sptip_w");
        msg = $('#reg_pwd_empty').val();
        $("#reg_pwd_msg").html(msg);
        return false;
    }else{
    //$("#reg_pwd_msg").html("");
    }
        
    if(reg_repwd==""){
        $("#reg_repwd_msg").attr("class","sptip_w");
        msg = $('#reg_repwd_empty').val();
        $("#reg_repwd_msg").html(msg);
        return false;
    }else{
    //$("#reg_repwd_msg").html("");
    }
        
    if(reg_email==""){
        $("#reg_email_msg").attr("class","sptip_w");
        msg = $('#reg_email_empty').val();
        $("#reg_email_msg").html(msg);
        return false;
    }else{
    //$("#reg_email_msg").html("");
    }
    
    //用户名格式
    patten_name = /^[a-zA-Z][\w]{5,50}$/;
    if(!patten_name.test(reg_name)){
        $("#reg_name_msg").attr("class","sptip_w");
        msg = $('#reg_name_error').val();
        $("#reg_name_msg").html(msg);
        return false;
    }
   
    //密码长度6-20
    len_pwd = reg_pwd.length;
    if(len_pwd<6||len_pwd>20){
        $("#reg_pwd_msg").attr("class","sptip_w");
        msg = $('#reg_pwd_error').val();
        $("#reg_pwd_msg").html(msg);
        return false;
    }
    
    //确认密码长度6-20
    len_repwd = reg_repwd.length;
    if(len_repwd<6||len_repwd>20){
        $("#reg_repwd_msg").attr("class","sptip_w");
        msg = $('#reg_repwd_error').val();
        $("#reg_repwd_msg").html(msg);
        return false;
    }
    
    //密码和确认密码是否相等
    if(reg_pwd!=reg_repwd){
        $("#reg_repwd_msg").attr("class","sptip_w");
        msg = $('#reg_repwd_pwd_eq').val();
        $("#reg_repwd_msg").html(msg);
        return false;
    }
    
    
    //ajax验证用户名，邮箱是否存在
    $.post("/user/checkRegist", {
        username: reg_name,
        email: reg_email
    },
    function (data){
        if(data){
            eval("var json= [" + data + "];");
            $("#"+json[0].name+"_msg").attr("class","sptip_w");
            $("#"+json[0].name+"_msg").html(json[0].msg);
            return false;
        }else{
            $("#reg_name_msg").attr("class","sptip");
            $("#reg_name_msg").html("<font color=green>√</font>");
            
            
            $("#reg_pwd_msg").attr("class","sptip");
            $("#reg_pwd_msg").html("<font color=green>√</font>");
            
            
            $("#reg_repwd_msg").attr("class","sptip");
            $("#reg_repwd_msg").html("<font color=green>√</font>");
            
            
            $("#reg_email_msg").attr("class","sptip");
            $("#reg_email_msg").html("<font color=green>√</font>");
            
            //是否同意协议条款
            isagree = $("#agree").attr("checked");
            if(!$("#agree").attr("checked")){
                msg = $("#agree_checked").val();
                $("#agree_msg").html(msg);
                return false;
            }else{
                $("#agree_msg").html("");
                $("#reg_form").submit();
            }
            
        }
    });
    
}

function checkLogin(){
    username = $("#username").val();
    password = $("#password").val();
    vcode = $("#vcode").val();
    if(username==""){
        msg = $("#username_empty").val();
        $("#username_msg").html(msg);
        return false;
    }
    if(password==""){
        msg = $("#password_empty").val();
        $("#password_msg").html(msg);
        return false;
    }
    if(vcode==""){
        msg = $("#vcode_empty").val();
        $("#vcode_msg").html(msg);
        return false;
    }
    $.post("/user/ajaxchecklogin", {
        /*
        username: username,
        password: password,
         */
        vcode: vcode
    },
    function (msg){
        if(msg){
            $("#vcode_msg").html(msg);
            return false;
        }else{
            $("#checklog").submit();
        }
    });
}


function checkVCode(){
    vcode = $("#vcode").val();
    if(vcode==""){
        msg = $("#vcode_empty").val();
        $("#vcode_msg").html(msg);
    }else{
        $.post("/user/ajaxchecklogin", {
            vcode: vcode
        },
        function (msg){
            if(msg){
                $("#vcode_msg").html(msg);
            }else{
                $("#vcode_msg").html("");
            }
        });
    }
}

function checkPassword(){
    password = $("#password").val();
    if(password==""){
        msg = $("#password_empty").val();
        $("#password_msg").html(msg);
    }else{
        $("#password_msg").html("");
    }
}

function checkUserName(){
    username = $("#username").val();
    if(username==""){
        msg = $("#username_empty").val();
        $("#username_msg").html(msg);
    }else{
        $("#username_msg").html("");
    /*
        $.post("/user/ajaxchecklogin", {
            username: username
        },
        function (msg){
            if(msg){
                $("#username_msg").html(msg);
            }else{
                $("#username_msg").html("");
            }
        });
         */
    }
}

<!-- 充值页面select验证 -->
function doPay(cid){
    if(cid==4){
        if(checkPay('server') && checkPay('card_no')){
            return true;
        }else{
            return false;
        }
    }else{
        if(checkPay('server')){
            if (cid==6) //弹出提示层
            {
                sl('block',1);
            }
            return true;
        }else{
            return false;
        }
    }
        
}

function doRecharge(cid){
    if(cid==4){
        if(checkPay('card_no') && checkPay('card_pin')){
            return true;
        }else{
            return false;
        }
    }
}


function checkPay(id){
    var val	= $('#'+id).val();
    if(!val){
        msg = $("#"+id+"note").val();
        $("#"+id+"msg").html(msg);
        $("#"+id+"msg").css("color","red");
        return false;
    }else{
        $("#"+id+"msg").html("");
        $("#"+id+"msg").css("color","");
        return true;
    }
}

function doBill(){
    if($("#issend").val()){//是否捐贈發票
        if(checkBill('name') && checkBill('idcard') && checkBill('phone') && checkBill('email') && checkBill('zipcode') && checkBill('address')){
            if($("#billtype").val() == 1){
                if(checkBill('bill_code')){
                    return true;
                }else{
                    return false;
                }
            }else{
                return true;
            }
        }else{
            return false;
        }
    }else{
        return true;
    }
}
function checkBill(id){
    val = $("#"+id).val();
    val = val.replace(/(^\s+)|(\s+$)/g,"");//删除空格
    if(id == 'email'){
        if(val){
            //验证邮箱格式
            patten = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            if(!patten.test(val)){
                $("#msg_"+id+" span").html("電子郵件信箱格式錯誤");
                $("#msg_"+id).css('display','block');
                return false;
            //邮箱格式错误
            }else{
                $("#msg_"+id).css('display','none');
                return true;
            }
        }else{
            $("#msg_"+id).css('display','none');
            return true;
        }
    }else{
        if(val){//判断是否为空
            if(id=='bill_code'){
                if(val.length != 8){
                    $("#msg_"+id+" span").html("統一編號為8位數組");
                    $("#msg_"+id).css('display','block');
                    return false;
                }else{
                    $("#msg_"+id+" span").html("統一編號必填");
                    $("#msg_"+id).css('display','none');
                    return true;
                }
            }else{
                $("#msg_"+id).css('display','none');
                return true;
            }
        }else{
            $("#msg_"+id).css('display','block');
            return false;
        }
    }
}

<!--轮播-->
$(function() {
	try{
		if(wid){
			return false;
		}
	}catch(e){
		
		
	}
    $(".slides_container").fadeIn(250);
    $('#slides').slides({
        preload: true,
        preloadImage: '/static/images/loading.gif',
        play: 6000,
        pause: 2000,
        hoverPause: true,
        animationStart: function(current){
            /*
                        $(".pagination li").each(function(){
                                var m = $(this).index()+1;
                                var n = current+1;
                                if(m==n){
                                        $(this).addClass("c"+n+"on");
                                }else{
                                        $(this).removeClass("c"+m+"on");
                                }
                        });
                        */
            $('.caption').animate({
                bottom:-25
            },100);
            if (window.console && console.log) {
                console.log('animationStart on slide: ', current);
            };
        },
        animationComplete: function(current){
            $('.caption').animate({
                bottom:0
            },200);
            if (window.console && console.log) {
                console.log('animationComplete on slide: ', current);
            };
        },
        slidesLoaded: function() {
            $('.caption').animate({
                bottom:0
            },200);
        }
    });
    $(".pagination li:first").addClass("c1on");    
});