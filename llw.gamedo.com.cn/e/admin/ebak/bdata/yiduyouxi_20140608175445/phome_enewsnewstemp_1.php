<?php
@include("../../inc/header.php");

/*
		SoftName : EmpireBak
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

E_D("DROP TABLE IF EXISTS `phome_enewsnewstemp`;");
E_C("CREATE TABLE `phome_enewsnewstemp` (
  `tempid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tempname` varchar(60) NOT NULL DEFAULT '',
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `temptext` mediumtext NOT NULL,
  `showdate` varchar(50) NOT NULL DEFAULT '',
  `modid` smallint(6) NOT NULL DEFAULT '0',
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tempid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8");
E_D("replace into `phome_enewsnewstemp` values('1','默认新闻内容模板','0','<!DOCTYPE>\r\n<html>\r\n<head>\r\n<meta http-equiv=\\\\\"Content-Type\\\\\" content=\\\\\"text/html; charset=utf-8\\\\\" />\r\n<title>[!--pagetitle--]</title>\r\n<meta name=\\\\\"keywords\\\\\" content=\\\\\"[!--pagekey--]\\\\\" />\r\n<meta name=\\\\\"description\\\\\" content=\\\\\"[!--pagedes--]\\\\\" />\r\n<link href=\\\\\"/images/style.css\\\\\" rel=\\\\\"stylesheet\\\\\" type=\\\\\"text/css\\\\\">\r\n<link href=\\\\\"/images/common.css\\\\\" rel=\\\\\"stylesheet\\\\\" type=\\\\\"text/css\\\\\">\r\n<script language=\\\\\"javascript\\\\\" type=\\\\\"text/javascript\\\\\" src=\\\\\"/images/jquery-1.7.1.min.js\\\\\" ></script>\r\n<script language=\\\\\"javascript\\\\\" type=\\\\\"text/javascript\\\\\" src=\\\\\"/images/slide.min.js\\\\\" ></script>\r\n<script language=\\\\\"javascript\\\\\" type=\\\\\"text/javascript\\\\\" src=\\\\\"/images/main.js\\\\\" ></script>\r\n<script language=\\\\\"javascript\\\\\" type=\\\\\"text/javascript\\\\\" src=\\\\\"/images/ajax.js\\\\\" ></script>\r\n<script type=\\\\\"text/javascript\\\\\" src=\\\\\"/images/bxCarousel.js\\\\\"></script>\r\n</head>\r\n<body>\r\n<div class=\\\\\"wrap\\\\\">\r\n    [!--temp.header--]\r\n    <div class=\\\\\"box clear\\\\\">\r\n            	<div class=\\\\\"part_left2\\\\\">\r\n				[!--temp.list_left--]\r\n			\r\n        </div>       \r\n		 <div class=\\\\\"part_right2\\\\\">\r\n        	<div class=\\\\\"invo_part\\\\\">                             \r\n                <div class=\\\\\"invo_guide\\\\\">\r\n                	 [!--newsnav--]\r\n                </div>      \r\n			</div>\r\n            \r\n            <div class=\\\\\"pros_part\\\\\">\r\n            	<div class=\\\\\"posi_tit\\\\\">\r\n                	<h4>[!--title--]</h4>\r\n                    <p>[!--newstime--]</p>\r\n                </div>\r\n                \r\n                <div class=\\\\\"posi_text\\\\\">\r\n                    [!--newstext--]\r\n                </div>\r\n            </div>\r\n        </div>\r\n            </div>\r\n  [!--temp.footer--] \r\n</div>\r\n\r\n[!--temp.right--]\r\n</body>\r\n</html>\r\n','Y-m-d','1','0');");
E_D("replace into `phome_enewsnewstemp` values('10','默认广告内容模板','0','<meta http-equiv=\\\\\"refresh\\\\\" content=\\\\\"0; url=[!--titlepic--]\\\\\">','m-d','9','0');");

@include("../../inc/footer.php");
?>