<?php /* Smarty version 2.6.25, created on 2015-06-24 16:13:16
         compiled from product_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'formaturl', 'product_list.tpl', 40, false),)), $this); ?>
<?php $this->assign('seotitle', $this->_tpl_vars['article']->seotitle); ?>
<?php $this->assign('seokeywords', $this->_tpl_vars['article']->seokeywords); ?>
<?php $this->assign('seodescription', $this->_tpl_vars['article']->seodescription); ?>
<?php $this->assign('seotitle', "产品中心"); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!--main start-->

<!-- 1 产品 , 2 新闻 , 3 关于 , 4 活动  , 5 招聘 -->
<style type="text/css">
<?php echo '
	body {
'; ?>

		<?php $this->assign('indexImgList', $this->_tpl_vars['navdata']->TakePageBg()); ?>
		<?php $_from = $this->_tpl_vars['indexImgList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['img']):
?>
			<?php if ($this->_tpl_vars['img']->cid == '1'): ?>
				background-image: url(<?php echo $this->_tpl_vars['siteurl']; ?>
<?php echo $this->_tpl_vars['img']->src; ?>
);
			<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
<?php echo '
	}
'; ?>

</style>

	<div class="page">
		<h1>所有游戏</h1>
		<div class="page_content">
			<div class="post fn_clear">
				<div class="content">
					<div class="cate fn_clear">
						<span class="name">
							平&nbsp;&nbsp;台：
						</span>
						<span class="fn_left tag fn_clear">
							<?php if (( count ( $this->_tpl_vars['categorylist'] ) > 0 )): ?>
								<?php $this->assign('categorylist', $this->_tpl_vars['categorydata']->GetCategoryList(0,'product',999)); ?>
									<?php $_from = $this->_tpl_vars['categorylist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['categoryinfo']):
?>
											<!-- <a href="javascript:;">
												<b></b><?php echo $this->_tpl_vars['categoryinfo']->name; ?>

											</a> -->
											<a href="<?php echo formaturl(array('type' => 'category','siteurl' => $this->_tpl_vars['siteurl'],'name' => $this->_tpl_vars['categoryinfo']->filename), $this);?>
">
												<b></b><?php echo $this->_tpl_vars['categoryinfo']->name; ?>

											</a>
									<?php endforeach; endif; unset($_from); ?>
								<?php else: ?>
							<?php endif; ?>
						</span>
					</div>
					<div class="cate fn_clear">
						<span class="name">
							类&nbsp;&nbsp;型：
						</span>
						<span class="fn_left tag fn_clear">
							<a href="javascript:;" id="ios">
								<b></b>IOS苹果
							</a>
							<a href="javascript:;" id="android">
								<b></b>Android安卓
							</a>
							<a href="javascript:;" id="web">
								<b></b>Web页游
							</a>
							<a href="javascript:;" id="sb">
								<b></b>Symbian游戏
							</a>
							<a href="javascript:;" id="java">
								<b></b>Java游戏
							</a>
							<a href="javascript:;" id="pc">
								<b></b>单机游戏
							</a>
						</span>
					</div>
					
				</div>
				<div class="post_content">
					<div class="game_list fn_clear">
						<ul class="game_list_ul fn_clear">
							<?php $this->assign('newproductlist', $this->_tpl_vars['productdata']->TakeProductList($this->_tpl_vars['product']->cid,0,99)); ?>
							<?php $_from = $this->_tpl_vars['newproductlist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['productinfo']):
?>
								<?php if ($this->_tpl_vars['productinfo']->status == 'ok'): ?>
									<li>
										<a href="<?php echo formaturl(array('type' => 'product','siteurl' => $this->_tpl_vars['siteurl'],'name' => $this->_tpl_vars['productinfo']->filename), $this);?>
" target="_blank">
											<img src="<?php echo $this->_tpl_vars['siteurl']; ?>
<?php echo $this->_tpl_vars['productinfo']->thumb; ?>
" title="<?php echo $this->_tpl_vars['productinfo']->name; ?>
" alt="<?php echo $this->_tpl_vars['productinfo']->name; ?>
"/>
										</a>
										<h5><a href="<?php echo formaturl(array('type' => 'product','siteurl' => $this->_tpl_vars['siteurl'],'name' => $this->_tpl_vars['productinfo']->filename), $this);?>
"><?php echo $this->_tpl_vars['productinfo']->name; ?>
</a></h5>
										<p class="tags" >
											<?php echo $this->_tpl_vars['productinfo']->tags; ?>

										</p>
									</li>
								<?php endif; ?>
							<?php endforeach; endif; unset($_from); ?>
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<!--main end-->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>