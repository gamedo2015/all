<?php /* Smarty version 2.6.25, created on 2015-06-24 16:13:16
         compiled from header.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title><?php echo $this->_tpl_vars['seotitle']; ?>
 - <?php echo $this->_tpl_vars['sitename']; ?>
</title>
<?php if ($this->_tpl_vars['seokeywords'] != "-" && $this->_tpl_vars['seokeywords'] != ""): ?>
<meta name="keywords" content="<?php echo $this->_tpl_vars['seokeywords']; ?>
" />
<?php endif; ?>
<?php if ($this->_tpl_vars['seodescription'] != "-" && $this->_tpl_vars['seodescription'] != ""): ?>
<meta name="description" content="<?php echo $this->_tpl_vars['seodescription']; ?>
" />
<?php endif; ?>
<meta name="copyright" content="copyright ganline.com all rights reserved." />
<script src="<?php echo $this->_tpl_vars['siteurl']; ?>
/templets/<?php echo $this->_tpl_vars['templets']->directory; ?>
/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $this->_tpl_vars['siteurl']; ?>
/templets/<?php echo $this->_tpl_vars['templets']->directory; ?>
/js/main.js" type="text/javascript"></script>
<link href="<?php echo $this->_tpl_vars['siteurl']; ?>
/templets/<?php echo $this->_tpl_vars['templets']->directory; ?>
/css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!--header start-->
	<div class="header">
		<div class="header_nav">
			<div class="nav_content fn_clear">
				<a href="<?php echo $this->_tpl_vars['siteurl']; ?>
" class="logo">
					<img src="<?php echo $this->_tpl_vars['siteurl']; ?>
/templets/<?php echo $this->_tpl_vars['templets']->directory; ?>
/img/logo_1.png" alt="掌上纵横">
				</a>
				<ul class="nav_ul">
					<li>
						<a href="<?php echo $this->_tpl_vars['siteurl']; ?>
">首页</a>
					</li>
					<li <?php 
							if(strpos($_SERVER['REQUEST_URI'],'game.php') !==false || strpos($_SERVER['REQUEST_URI'],'product.php') !==false)
								echo "class='on'";
						 ?> >
						<a href="<?php echo $this->_tpl_vars['siteurl']; ?>
/game.php">产品</a>
					</li>
					<li <?php 
							if(strpos($_SERVER['REQUEST_URI'],'category/news/') !==false)
								echo "class='on'";
						 ?> >
						<a href="<?php echo $this->_tpl_vars['siteurl']; ?>
/category/news/">资讯</a>
					</li>
					<li <?php 
							if(strpos($_SERVER['REQUEST_URI'],'category/activity/') !==false)
								echo "class='on'";
						 ?> >
						<a href="<?php echo $this->_tpl_vars['siteurl']; ?>
/category/activity/">活动</a>
					</li>
<!--					<li>
						<a href="<?php echo $this->_tpl_vars['siteurl']; ?>
/register.php">用户注册</a>
					</li>-->
					<li style="width:150px;">
						<a href="http://www.gamedo.com.cn">返回官网首页</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!--menu end-->