{assign var="seotitle" value=$titlekeywords}
{assign var="seokeywords" value=$metakeywords}
{assign var="seodescription" value=$metadescription}
{include file="header.tpl"}
<!--main start-->
<!-- <p>{$sitecopy}</p>
{php}
	echo $this->_tpl_vars['sitecopy'];
{/php}  -->

<div class="index">
	<div class="header_banner">
		<div class="banner_img">
			<div class="homeShow x-imgbox">
				<div style="z-index: 888;" class="homeHead">
					<ul class="fix num">
						
					</ul>
					<div class="arrows">
						<a href="javascript:;" class="left"> <i>上一张</i>
						</a>
						<a href="javascript:;" class="right"> <i>下一张</i>
						</a>
					</div>
				</div>
				<ul id="slides" class="banner index_banner big-img" >
					{assign var="indexImgList" value=$navdata->TakeLanPic()}
					{foreach from=$indexImgList item=img}
						<li class="slide" style="background-image: url({$siteurl}{$img->fileaddress}); z-index: 9;">
							<div class="w1000">
								<a href="{$img->url}" target="_blank" class="game_link" title="{$img->title}"></a>
							</div>
						</li>
					{/foreach}
				</ul>
			</div>
		</div>
	</div>
	<div class="index_main_content">
		<div class="fn_clear">
			<div class="w310">
				<div class="fn_clear h3">
					<h3>热门游戏</h3>
					<a href="{$siteurl}/game.php" class="more">更多&gt;</a>
				</div>
				<ul class="game_list">
					{assign var="productlist" value=$navdata->TakeHot(0,0,3)}
					{foreach from=$productlist item=productinfo}
						<li class="thumb">
							<a href="{formaturl type="product" siteurl=$siteurl name=$productinfo->filename}" target="_blank">
								<img src="{$siteurl}{$productinfo->thumb}" title="{$productinfo->name}" alt="{$productinfo->name}" width="280" height="173" />
							</a>
							<h5>
								<a href="{formaturl type="product" siteurl=$siteurl name=$productinfo->filename}" target="_blank">{$productinfo->name}</a>
							</h5>
						</li>
					{/foreach}
				</ul>
			</div>
			<div class="w680">
				<div class="fn_clear h3">
					<h3>资讯</h3>
					<a href="{formaturl type="category" siteurl=$siteurl name="news"}" class="more">更多&gt;</a>
				</div>
				<div class="main_news">
					{assign var="newslist" value=$navdata->TakeHotArticle("news",0,1)}
					{foreach from=$newslist item=newsinfo}
						<a href="{formaturl type="article" siteurl=$siteurl name=$newsinfo->filename}" title="{$newsinfo->title}" target="_blank" class="img_center">
							<img src="{$siteurl}{$newsinfo->imgurl}" title="{$newsinfo->title}" alt="{$newsinfo->title}">
						</a>
						<h5>
							<a href="{formaturl type="article" siteurl=$siteurl name=$newsinfo->filename}" title="{$newsinfo->title}" target="_blank">{$newsinfo->title|truncate:54:"":true}</a>
							<div class="time">
								{$newsinfo->adddate}
							</div>
						</h5>
						<P>
							{$newsinfo->content|replace:"&nbsp;":""|replace:"&ldquo;":""|replace:"&rdquo;":""|strip_tags|truncate:280:"...":true}
						</P>
					{/foreach}
				</div>
				<div class="two_news fn_clear">
					{assign var="newslist" value=$navdata->TakeHotArticle("news",1,1)}
					{foreach from=$newslist item=newsinfo}
						<div class="w315 fn_left">
							<a href="{formaturl type="article" siteurl=$siteurl name=$newsinfo->filename}" title="{$newsinfo->title}" target="_blank"  class="img_center">
								<img src="{$siteurl}{$newsinfo->imgurl}" title="{$newsinfo->title}" alt="{$newsinfo->title}" width="310" height="102">
							</a>
							<h5>
								<a href="{formaturl type="article" siteurl=$siteurl name=$newsinfo->filename}" title="{$newsinfo->title}" target="_blank">{$newsinfo->title|truncate:42:"":true}</a>
								
							</h5>
							<P>
								{$newsinfo->content|replace:"&nbsp;":""|strip_tags|truncate:80:"...":true}
							</P>
						</div>
					{/foreach}
					{assign var="newslist" value=$navdata->TakeHotArticle("news",2,1)}
					{foreach from=$newslist item=newsinfo}
						<div class="w315 fn_right">
							<a href="{formaturl type="article" siteurl=$siteurl name=$newsinfo->filename}" title="{$newsinfo->title}" target="_blank"  class="img_center">
								<img src="{$siteurl}{$newsinfo->imgurl}" title="{$newsinfo->title}" alt="{$newsinfo->title}" width="310" height="102">
							</a>
							<h5>
								<a href="{formaturl type="article" siteurl=$siteurl name=$newsinfo->filename}" title="{$newsinfo->title}" target="_blank">{$newsinfo->title|truncate:42:"":true}</a>
								
							</h5>
							<P>
								{$newsinfo->content|replace:"&nbsp;":""|strip_tags|truncate:80:"...":true}
							</P>
						</div>
					{/foreach}
				</div>
			</div>
		</div>
		<div class="all_game index_all_game fn_clear">
			<div class="fn_clear h3">
				<h3>所有游戏</h3>
				<a href="{$siteurl}/game.php" class="more">更多&gt;</a>
			</div>
			<div class="all_game_list">
				<ul class="fn_clear">
					{assign var="productlist" value=$productdata->TakeProductList(0,0,6)}
					{foreach from=$productlist item=productinfo }
						<li class="thumb">
							<a href="{formaturl type="product" siteurl=$siteurl name=$productinfo->filename}" target="_blank">
								<img src="{$siteurl}{$productinfo->productico}" title="{$productinfo->name}" alt="{$productinfo->name}"/>
							</a>
							<div class="info">
								<h5>
									<a href="{formaturl type="product" siteurl=$siteurl name=$productinfo->filename}" target="_blank">{$productinfo->name}</a>
								</h5>
								<P>
									{$productinfo->content|strip_tags|truncate:60:"...":true}
								</P>
							</div>
						</li>
					{/foreach}
				</ul>
			</div>
		</div>
		{include file="footer.tpl"}
	</div>
</div>

<!--header end-->
