<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>{$seotitle} - {$sitename}</title>
{if $seokeywords != "-" && $seokeywords != ""}
<meta name="keywords" content="{$seokeywords}" />
{/if}
{if $seodescription != "-" && $seodescription != ""}
<meta name="description" content="{$seodescription}" />
{/if}
<meta name="copyright" content="copyright ganline.com all rights reserved." />
<script src="{$siteurl}/templets/{$templets->directory}/js/jquery.min.js" type="text/javascript"></script>
<script src="{$siteurl}/templets/{$templets->directory}/js/main.js" type="text/javascript"></script>
<link href="{$siteurl}/templets/{$templets->directory}/css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!--header start-->
	<div class="header">
		<div class="header_nav">
			<div class="nav_content fn_clear">
				<a href="{$siteurl}" class="logo">
					<img src="{$siteurl}/templets/{$templets->directory}/img/logo_1.png" alt="掌上纵横">
				</a>
				<ul class="nav_ul">
					<li>
						<a href="{$siteurl}">首页</a>
					</li>
					<li {php}
							if(strpos($_SERVER['REQUEST_URI'],'game.php') !==false || strpos($_SERVER['REQUEST_URI'],'product.php') !==false)
								echo "class='on'";
						{/php} >
						<a href="{$siteurl}/game.php">产品</a>
					</li>
					<li {php}
							if(strpos($_SERVER['REQUEST_URI'],'category/news/') !==false)
								echo "class='on'";
						{/php} >
						<a href="{$siteurl}/category/news/">资讯</a>
					</li>
					<li {php}
							if(strpos($_SERVER['REQUEST_URI'],'category/activity/') !==false)
								echo "class='on'";
						{/php} >
						<a href="{$siteurl}/category/activity/">活动</a>
					</li>
<!--					<li>
						<a href="{$siteurl}/register.php">用户注册</a>
					</li>-->
					<li style="width:150px;">
						<a href="http://www.gamedo.com.cn">返回官网首页</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!--menu end-->
