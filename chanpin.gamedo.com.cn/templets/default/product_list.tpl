{assign var="seotitle" value=$article->seotitle}
{assign var="seokeywords" value=$article->seokeywords}
{assign var="seodescription" value=$article->seodescription}
{assign var="seotitle" value="产品中心"}
{include file="header.tpl"}
<!--main start-->

<!-- 1 产品 , 2 新闻 , 3 关于 , 4 活动  , 5 招聘 -->
<style type="text/css">
{literal}
	body {
{/literal}
		{assign var="indexImgList" value=$navdata->TakePageBg()}
		{foreach from=$indexImgList item=img}
			{if $img->cid eq "1"}
				background-image: url({$siteurl}{$img->src});
			{/if}
		{/foreach}
{literal}
	}
{/literal}
</style>

	<div class="page">
		<h1>所有游戏</h1>
		<div class="page_content">
			<div class="post fn_clear">
				<div class="content">
					<div class="cate fn_clear">
						<span class="name">
							平&nbsp;&nbsp;台：
						</span>
						<span class="fn_left tag fn_clear">
							{if (count($categorylist) > 0)}
								{assign var="categorylist" value=$categorydata->GetCategoryList(0,"product",999)}
									{foreach from=$categorylist item=categoryinfo}
											<!-- <a href="javascript:;">
												<b></b>{$categoryinfo->name}
											</a> -->
											<a href="{formaturl type="category" siteurl=$siteurl name=$categoryinfo->filename}">
												<b></b>{$categoryinfo->name}
											</a>
									{/foreach}
								{else}
							{/if}
						</span>
					</div>
					<div class="cate fn_clear">
						<span class="name">
							类&nbsp;&nbsp;型：
						</span>
						<span class="fn_left tag fn_clear">
							<a href="javascript:;" id="ios">
								<b></b>IOS苹果
							</a>
							<a href="javascript:;" id="android">
								<b></b>Android安卓
							</a>
							<a href="javascript:;" id="web">
								<b></b>Web页游
							</a>
							<a href="javascript:;" id="sb">
								<b></b>Symbian游戏
							</a>
							<a href="javascript:;" id="java">
								<b></b>Java游戏
							</a>
							<a href="javascript:;" id="pc">
								<b></b>单机游戏
							</a>
						</span>
					</div>
					
				</div>
				<div class="post_content">
					<div class="game_list fn_clear">
						<ul class="game_list_ul fn_clear">
							{assign var="newproductlist" value=$productdata->TakeProductList($product->cid,0,99)}
							{foreach from=$newproductlist item=productinfo}
								{if $productinfo->status eq "ok"}
									<li>
										<a href="{formaturl type="product" siteurl=$siteurl name=$productinfo->filename}" target="_blank">
											<img src="{$siteurl}{$productinfo->thumb}" title="{$productinfo->name}" alt="{$productinfo->name}"/>
										</a>
										<h5><a href="{formaturl type="product" siteurl=$siteurl name=$productinfo->filename}">{$productinfo->name}</a></h5>
										<p class="tags" >
											{$productinfo->tags}
										</p>
									</li>
								{/if}
							{/foreach}
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<!--main end-->
{include file="footer.tpl"}