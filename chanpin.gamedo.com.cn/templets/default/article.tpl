{assign var="seotitle" value=$article->seotitle}
{assign var="seokeywords" value=$article->seokeywords}
{assign var="seodescription" value=$article->seodescription}
{include file="header.tpl"}
<!--main start-->

	{literal}
	<style type="text/css">
		body {
	{/literal}
			{assign var="indexImgList" value=$navdata->TakePageBg()}
			{foreach from=$indexImgList item=img}
				{if $img->cid eq "2"}
					background-image: url({$siteurl}{$img->src});
				{/if}
			{/foreach}
	{literal}
		}
	{/literal}
	</style>


<div class="page">
	<h1>新闻中心</h1>
	<div class="page_content news_page">
		<div class="main">
			<div class="width640">
				<!-- {if $article->imgurl neq "-"}
					<img src="{$siteurl}{$article->imgurl}" title="{$newsinfo->title}" alt="{$newsinfo->title}">
				{/if} -->
				<div>
					<h1 class="fn_clear">
						<span class="fn_left">
							{$article->title}
						</span>
						<span class="time fn_right">
						发布时间:{$article->adddate}
					</span></h1>
					<div class="content">
						{$article->content}
						
						<div class="page_number">
							{assign var=prevarticle value=$articledata->GetPrevArticle($article)}
						{if $prevarticle->title != ""}
								<span class="margin">
									上一篇:
									<a href="{formaturl type="article" siteurl=$siteurl name=$prevarticle->filename}">{$prevarticle->title}</a>
								</span>
							{/if}
						{assign var=nextarticle value=$articledata->GetNextArticle($article)}
						{if $nextarticle->title != ""}
								下一篇:
								<a href="{formaturl type="article" siteurl=$siteurl name=$nextarticle->filename}">{$nextarticle->title}</a>
							{/if}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--main end-->
{include file="footer.tpl"}