{assign var="seotitle" value=$article->seotitle}
{assign var="seokeywords" value=$article->seokeywords}
{assign var="seodescription" value=$article->seodescription}
{assign var="seotitle" value="产品中心"}
{include file="header.tpl"}
<!--main start-->
<style type="text/css">
{literal}
	body {
{/literal}
		background-image: url({$siteurl}/templets/{$templets->directory}/pic/bg/bg02.jpg);
{literal}
	}
{/literal}
</style>

	<div class="page">
		<h1>{$category->name}</h1>
		<div class="page_content ">
			<div class="post">
				<div class="cate fn_clear">
					{assign var="cat" value=$categorydata->GetCategory($category->pid)}<!--获取当前分类的父分类-->
						<a href="{$siteurl}/game.php">平&nbsp;&nbsp;台：</a> > 
					{if $cat!=""}<!--判断是否存在父分类-->
						<a href="{formaturl type='category' siteurl=$siteurl name=$cat->filename}">{$cat->name}</a>
					<!--输出父分类-->
					{/if}
					{$category->name}<!--输出当前分类-->
				</div>
				<div class="cate fn_clear">
					<span class="name">
						类&nbsp;&nbsp;型：
					</span>
					<span class="fn_left tag fn_clear">
						<a href="javascript:;" id="ios">
							<b></b>IOS苹果
						</a>
						<a href="javascript:;" id="android">
							<b></b>Android安卓
						</a>
						<a href="javascript:;" id="web">
							<b></b>Web页游
						</a>
						<a href="javascript:;" id="sb">
							<b></b>Symbian游戏
						</a>
						<a href="javascript:;" id="java">
							<b></b>Java游戏
						</a>
						<a href="javascript:;" id="pc">
							<b></b>单机游戏
						</a>
					</span>
				</div>
				<div class="game_list fn_clear">
					{if (count($subcategory) > 0)}
					{if ($curpage == 1)}
					<!--sub_category-->
						<h2>{$category->name}下的分类</h2>
						<div class="content">
							<ul>
								{foreach from=$subcategory item=cat}
								<li><a href="{formaturl type="category" siteurl=$siteurl name=$cat->filename}">{$cat->name}</a></li>				
								{/foreach}
							</ul>
						</div>
					<!--/sub_category-->
				
					{/if}	
					{/if}
						{if ($total > 0)}
							<ul class="game_list_ul fn_clear">
							{foreach from=$productlist item=productinfo}
								<li>
									<a href="{formaturl type="product" siteurl=$siteurl name=$productinfo->filename}" target="_blank">
										<img src="{$siteurl}{$productinfo->thumb}" title="{$productinfo->name}" alt="{$productinfo->name}"/>
									</a>
									<h5><a href="{formaturl type="product" siteurl=$siteurl name=$productinfo->filename}">{$productinfo->name}</a></h5>
									<p class="tags" >
										{$productinfo->tags}
									</p>
								</li>
							{/foreach}
							</ul>
						
						{else}
						该分类下暂时没有内容
						{/if}
				</div>
			</div>
		</div>
		
	</div>
	<!--main end-->
{include file="footer.tpl"}