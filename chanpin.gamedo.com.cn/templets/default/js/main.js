$(function(){
	//幻灯片计数
	$(".header_banner #slides li").each(function(){
		$(".index .homeHead .num").append('<li class="menuItem fleft"><a href="javascript:void(0)"></a></li>');
		$(".index .homeHead .num li").eq(0).addClass("on");
	});
	$(".index #slides .slide:first").show();
	$(".banner .text .close").click(function(){
		$(this).parents(".banner").fadeOut(300);
	});
	$(".main_img .tab li,.goods_page .goods_info .option .size dd").click(function(){
		$(this).addClass("on").siblings().removeClass("on");
	});

	game_list();

	//首页轮播
	var totWidth = 0;
	var positions = new Array();
	var pwidth = $(".homeShow").width();
	$(".slide").width(pwidth);
	$('#slides .slide').each(function(i) {
		positions[i] = totWidth;
		var f = pwidth - 960;
		totWidth += (960 + f);
		if (!$(this).width()) {
			return false
		}
	});
	//窗口调整尺寸时
	$(window).resize(function() {
		totWidth = 0;
		positions = new Array();
		pwidth = $(".homeShow").width();
		$(".slide").width(pwidth);
		$('#slides .slide').each(function(i) {
			positions[i] = totWidth;
			var f = pwidth - 960;
			totWidth += (960 + f);
			if (!$(this).width()) {
				return false
			}
		});
		$('#slides').width(totWidth).stop().animate({
			marginLeft: -positions[0] + 'px'
		}, 0)
	});
	$('#slides').width(totWidth);
	indexImgPlay();
});
var indexImgPlay = function() {
	//变量
	$box = $(".x-imgbox");
	$ul = $box.find(".big-img");
	$li = $ul.find("li");
	$left = $box.find("a.left");
	$right = $box.find("a.right");
	$i = $li.length;
	$cur = 1;
	$num = $box.find(".num").find("li");

	//动画时间
	$time = 800;

	//计时器时间
	$movTime = 3000;

	$li.eq(0).css("z-index","9");
	//定义自动滚动
	var $mov;

	//右
	$right.click(function() {
		if (!$li.is(":animated")) {
			if ($cur < $i) {
				$li.eq($cur).fadeIn($time,function(){
					$cur++;
					$num.eq($cur - 1).addClass("on").siblings().removeClass("on");
				}).siblings().fadeOut($time);
			} if ($cur == $i) {
				$li.eq(0).fadeIn($time,function(){
					$cur = 1;
					$num.eq($cur - 1).addClass("on").siblings().removeClass("on");
				}).siblings().fadeOut($time);
			};
		};
	});
	//左
	$left.click(function() {
		if (!$li.is(":animated")) {
			if ($cur > 1) {
				$li.eq($cur - 2).fadeIn($time,function(){
					$cur--;
					$num.eq($cur - 1).addClass("on").siblings().removeClass("on");
				}).siblings().fadeOut($time);
			} if ($cur == 1) {
				$li.eq($i - 1).fadeIn($time,function(){
					$cur = $i;
					$num.eq($cur - 1).addClass("on").siblings().removeClass("on");
				}).siblings().fadeOut($time);
			};
		};
	});

	//点击num图标
	$num.click(function() {
		$ind = $(this).index();
		$li.eq($ind).fadeIn($time, function() {
			$cur = $ind + 1;
		}).siblings().fadeOut($time);
		$(this).addClass("on").siblings().removeClass("on");
	});

	$box.live("mouseenter", function() {
		//移入时清除计时器
		clearInterval($mov);
	}).live("mouseleave", function() {
		//计时器
		$mov = setInterval(function() {
			$right.trigger("click");
		}, $movTime);
	}).trigger('mouseleave');
};

//列表页筛选
var game_list = function(){
	// $list = $(".game_list_ul");

	$(".page .cate .tag a").click(function(){
		$(this).toggleClass("on");
		$sys = $(this).attr("id");
		$sys = "#" + $sys;
		$(this).siblings("a").removeClass("on");
		// console.log($sys);
		
		$(".game_list_ul li").each(function(){
			$(this).fadeOut(300);
			// $(this).find($sys).hide();
			if($($(this).find($sys)).length>0){
				$(this).fadeIn(300);
				// console.log(2)
			}else{
				// console.log(3)
				$(this).fadeOut(300);
			}
		});

	});

	

	$(function(){
		$(".old_img").remove();
	});
};

