<?php
require_once 'sqlite.php';
header("Content-type: text/html; charset=utf-8");



if(isset($_GET['preview'])) {
	$aid = $_GET['aid'];

	$query = db::query(
		'SELECT * FROM article WHERE aid = :aid',
		array(
			':aid' => $aid
		)
	);
	echo $query[0]['content'];
}

if(isset($_POST['save'])) {
	$aid = $_POST['aid'];
	$content = $_POST['content'];

	db::exec(
		'INSERT INTO article (content, aid) VALUES (:content, :aid)',
		array(
			':aid' => $aid,
			':content' => $content,
		)
	);
	db::exec(
		'UPDATE article SET content = :content WHERE aid = :aid',
		array(
			':aid' => $aid,
			':content' => $content,
		)
	);



	$query = db::query(
		'SELECT * FROM article WHERE aid = :aid',
		array(
			':aid' => $aid
		)
	);
	echo $query[0]['content'];
}


?>