<?php
require_once 'admin.inc.php';
require_once '../include/lantern.class.php';
$delId = $_GET["delId"];
$lanterndata = new Lantern;
$lanterncount = $lanterndata->GetLanternList();
$lanternlist = $lanterndata->TakeLanternList(0,100);
$lanterninfo = $lanterndata->GetLantern($delId);

//删除
if(!empty($delId)) {
	//确定图片路径
	$imgAddress = "..".$lanterninfo->fileaddress;
	@unlink($imgAddress);
	$sql = "delete from yiqi_lantern where kid='$delId'";
	$result = $yiqi_db->query(CheckSql($sql));
	if($result==1)
	{
		echo "
<script>alert('删除成功！');location.href='lanternM.php';</script>
";
	}
	else
	{
	   echo "
<script>alert('删除失败！请联系管理员.');location.href:lanternM.php;</script>
";
	}
}
?>
<?php
$adminpagetitle = "幻灯片管理";
include("admin.header.php");?>
<div class="main_body">
	<form id="sform" action="lantern.php" method="post">
		<input id="action" type="hidden" name="action" value="insert" />
		<h3>
			幻灯片管理：
			<a href="lantern.php" style="color:#FF0000;">添加</a>
			<a href="lanternM.php" style="color:#FF0000;">管理</a>
		</h3>
		<table class="inputform" cellpadding="1" cellspacing="1">
			<tr class="th">
				<td>编号</td>
				<td>标题</td>
				<td>图片</td>
				<td>状态</td>
				<td>操作</td>
			</tr>
			<?php
			$url = getset("siteurl")->value;
if(count($lanterncount)>
			0)
{
	foreach($lanternlist as $lanterninfo)
	{
	   if($lanterninfo->status=="ok") 
	   {
		   $lanterninfoStatus = "生效";
	   }
	   else
	   {
		   $lanterninfoStatus = "失效";
	   }
	   echo "
			<tr>
				".
			"
				<td>$lanterninfo->displayorder</td>
				".
			"
				<td>$lanterninfo->title</td>
				".
			"
				<td><img src='$url$lanterninfo->fileaddress' style='width:200px; height:100px;'></td>
				".
			"
				<td>$lanterninfoStatus</td>
				".
			"
				<td>
					<a href=\"lanternE.php?nId=$lanterninfo->kid\">编辑</a>
					|
					<a href=\"lanternM.php?delId=$lanterninfo->kid\" onclick=\"confirm('确定要删除吗？');\">删除</a>
				</td>
				".		
			"
			</tr>
			";
	} 
}
?>
		</table>

		<div class="inputsubmit" style="padding-left:90px;">
			<input id="submitbtn" type="submit" class="subtn" value="提交" />
		</div>
	</form>
</div>

</div>
<script type="text/javascript">
$(function(){
	var formoptions = {
		beforeSubmit: function() {
			$("#submitbtn").val("正在处理...");
			$("#submitbtn").attr("disabled","disabled");
		},
		success: function (msg) {
			alert(msg);
			if(msg == "指定关键词添加成功！")
				$("#sform").resetForm();
			$("#submitbtn").val("提交");
			$("#submitbtn").attr("disabled","");
		}
	};
	$("#sform").ajaxForm(formoptions);
});
</script>

<?php include("admin.footer.php");?></div>

</body>

</html>