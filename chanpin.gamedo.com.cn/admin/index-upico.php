<?php
$checkregular = "false";
require_once 'admin.inc.php';
?>
<?php
$adminpagetitle = "管理首页";
include("admin.header.php");?>

	<!-- php 上传多图 -->
	<h3>上传成功</h3>
	<?php
		session_start();
		$hide = $_POST['hidden'];
		if($hide == $_SESSION['conn']) {
		?>
			<?php 
				$url = getset("siteurl")->value;

				$d = date("ymdhis");
				$img = $_FILES['img'];
				if ($img) {
					//文件存放目录，和本php文件同级
					$dir = dirname(dirname(__FILE__));
					$i = 0;
					foreach ($img['tmp_name'] as $value) {
						$filename = $img['name'][$i];
						if ($value) {
							$savepath = "{$dir}/uploads/ico/{$d}{$i}.jpg";
							// echo $savepath . '<br>';
							$state = move_uploaded_file($value, $savepath);
							//如果上传成功，预览
							if ($state) {
								echo "<img src='$url/uploads/ico/{$d}{$i}.jpg' alt='{$filename}' />\r\n";
							}
						}
						$i++;
					}
				}
			?>
		<?php 
		} else {
		?>
			非法操作，请<a  class="go_back">返回</a>
		<?php 
		}
		session_destroy();
	?>
<!-- END -->
</div>

<?php include("admin.footer.php");?></div>

</body>

</html>