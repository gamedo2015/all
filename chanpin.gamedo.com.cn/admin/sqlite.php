<?php

class db {
	const db_file = 'sqlite.db';

	public static function conn() {
		return new PDO('sqlite:' . self::db_file);
	}
	public static function query($statem, $param) {
		$pdo = self::conn();
		$prepare = $pdo->prepare($statem);
		$prepare->execute($param);
		$prepare->setFetchMode(PDO::FETCH_ASSOC);
		return $prepare->fetchAll();
	}
	public static function exec($statem, $param) {
		$pdo = self::conn();
		$prepare = $pdo->prepare($statem);
		$prepare->execute($param);
		return $prepare->rowCount();
	}
}

?>