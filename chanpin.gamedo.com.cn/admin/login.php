<?php
require_once '../include/common.inc.php';
require_once '../include/user.class.php';

session_start();
$action = $_POST["action"];
if($action=="save")
{
	$username = $_POST["username"];
	$userpass = $_POST["password"];    
	
	if(function_exists("imagejpeg"))
	{
		if (empty($_SESSION['captcha']) || trim(strtolower($_POST['capcode'])) != $_SESSION['captcha'])    
		{
			ShowMsg("验证码错误,请重新输入");
			exit();
		}
	}
	$userdata = new User;
	$existuser = $userdata->ExistUserPassword($username,$userpass);
	if($existuser == 1)
	{
		$userinfo = $userdata->GetUserByName($username);
		session_start();
		$_SESSION["adminid"] = $userinfo->uid;
		header("location:index.php");
	}
	else
	{
		ShowMsg("用户名或密码错误");exit();
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>橄榄传媒 | LINECMS 网站内容管理系统 - 用户登录</title>
<script type="text/javascript" src="../images/jq.js" ></script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body class="login_page">
	<form action="" method="post">
		<div class="login">
			<div class="login-body">
				<div class="login_con fn_clear">
					<div class="login_logo fn_left">
						<a href="http://ganline.com" target="_blank">
							<img class="login_img" src="img/admin-login-logo.gif" alt="LINECMS | 网站内容管理系统">
						</a>
					</div>
					<div class="default-form fn_right">
						<h2>用户登录</h2>
						<div class="item">
							<label class="zh">帐号：</label>
							<div class="input input_bg" id="username" >
								<input class="txt" type="text" name="username"/>
							</div>
						</div>
						<div class="item">
							<label class="zh">密码</label>
							<div class="input input_bg" id="password" >
								<input class="txt" type="password" name="password" />
							</div>
						</div>
						<div class="item capcode">
								<?php 
									if(function_exists("imagejpeg"))
									{ 
										echo '
												<label class="label">验证码</label>
							<div class="capcode-con">
								<input class="txt" type="text" name="capcode" /><img style="cursor:pointer" src="../captcha/captcha.php" onclick="$(this).attr(\'src\',\'../captcha/captcha.php?d=\'+Date())" />
							</div>
							';
									}?>
						</div>
						<div class="input login_btn">
							<input type="hidden" name="action" value="save"/>
							<input class="btn" type="submit" value="提交" />
						</div>
					</div>
				</div>
			</div>
			<P class="info">
				技术支持：<a href="http://ganline.com/" target="_blank">三门峡橄榄广告有限责任公司</a>  Copyright © 2013-<?php echo date('Y',time()); ?> <a href="http://ganline.com/" target="_blank">Ganline.com</a> All rights reserved <br>
				如果您忘记了您的密码，请联系 <a class="mail" href="mailto:sales@ganline.com?subject=我遗忘了我的网站密码&amp;body=请填写您的联系方式和附带着贵司的网站资料...">售后部门</a>
			</P>
		</div>
	</form>
</body>
</html>
