<?php
require_once 'admin.inc.php';
require_once '../include/lantern.class.php';

$nId = $_GET["nId"];
$lanterndata = new Lantern;
$lanterninfo = $lanterndata->GetLantern($nId);

$action = $_POST["action"];
if($action == "update")
{
	$adtitle		= $_POST["adtitle"];
	$adurl			= $_POST["adurl"];
	//$productthumb	=  "-";
	$adstatus		= $_POST["adstatus"];
	$adorder		= $_POST["adorder"];
	
	if(empty($adtitle))
	{
		$adtitle = "-";
	}
	if(empty($adurl))
	{
		$adurl = "http://".$_SERVER["SERVER_NAME"]."/".str_replace("admin/lantern.php","",$_SERVER["PHP_SELF"]);
	}
	if(!is_numeric($adorder)||empty($adorder))
	{
		$adorder = 0;
	}
	if(!empty($_FILES["productthumb"]["name"]))
	{
		require_once("../include/upload.class.php");
		$filedirectory = YIQIROOT."/uploads/image";
		$filename = date("ymdhis");
		$filetype = $_FILES['productthumb']['type'];
		$upload = new Upload;
		$upload->set_max_size(1800000); 
		$upload->set_directory($filedirectory);
		$upload->set_tmp_name($_FILES['productthumb']['tmp_name']);
		$upload->set_file_size($_FILES['productthumb']['size']);
		$upload->set_file_ext($_FILES['productthumb']['name']); 
		$upload->set_file_type($filetype); 
		$upload->set_file_name($filename); 	    
		$upload->start_copy(); 	    
		if($upload->is_ok())
		{
			$productthumb = "/uploads/image/".$filename.'.'.$upload->user_file_ext;
		}
		else
		{
			exit($upload->error());
		}
		$sql = "update yiqi_lantern set `title`='$adtitle',`url`='$adurl',`fileaddress`='$productthumb',`status`='$adstatus',`displayorder`='$adorder' where kid='$nId';";
	}else
	{
		$sql = "update yiqi_lantern set `title`='$adtitle',`url`='$adurl',`status`='$adstatus',`displayorder`='$adorder' where kid='$nId';";
	}	
	$result = $yiqi_db->query(CheckSql($sql));
	if($result==1)
	{
		exit("幻灯片编辑成功！");
	}
	else
	{
		exit("幻灯片编辑失败,请与管理员联系！");
	}
}
?>
<?php
$adminpagetitle = "幻灯片管理";
include("admin.header.php");?>
<div class="main_body">
	<form id="sform" action="lanternE.php?nId=<?= $nId;?>
		" method="post">
		<input id="action" type="hidden" name="action" value="update" />
		<h3>
			幻灯片编辑：
			<a href="lantern.php" style="color:#FF0000;">添加</a>
			<a href="lanternM.php" style="color:#FF0000;">管理</a>
		</h3>
		<table class="inputform" cellpadding="1" cellspacing="1">
			<tr>
				<td class="label">广告标题</td>
				<td class="input">
					<input type="text" class="txt" name="adtitle" value="<?php echo $lanterninfo->title;?>" /></td>
			</tr>
			<tr>
				<td class="label">广告地址</td>
				<td class="input">
					<input type="text" class="txt" name="adurl" value="<?php echo $lanterninfo->url;?>" /></td>
			</tr>
			<tr>
				<td class="label">广告图片</td>
				<td class="input">
					<input type="file" class="txt" style="width:280px;" name="productthumb" />
				</td>
			</tr>
			<tr>
				<td class="label">是否生效</td>
				<td class="input">
					<input name="adstatus" type="radio" value="ok" <?php if($lanterninfo->
					status=="ok") echo "checked";?>> 生效
					<input name="adstatus" type="radio" value="hide" <?php if($lanterninfo->status=="hide") echo "checked";?>> 失效</td>
			</tr>
			<tr>
				<td class="label">广告排序</td>
				<td class="input">
					<input type="text" class="txt" name="adorder" value="<?php echo $lanterninfo->displayorder;?>" /></td>
			</tr>
		</table>

		<div class="inputsubmit">
			<input id="submitbtn" type="submit" class="subtn" value="提交" />
		</div>
	</form>
</div>

</div>
<script type="text/javascript">
$(function(){
	var formoptions = {
		beforeSubmit: function() {
			$("#submitbtn").val("正在处理...");
			$("#submitbtn").attr("disabled","disabled");
		},
		success: function (msg) {
			alert(msg);
			if(msg == "指定关键词添加成功！")
				$("#sform").resetForm();
			$("#submitbtn").val("提交");
			$("#submitbtn").attr("disabled","");
		}
	};
	$("#sform").ajaxForm(formoptions);
});
</script>

<?php include("admin.footer.php");?></div>

</body>

</html>