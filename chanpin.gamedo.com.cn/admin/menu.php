<div class="menu_dl">
	<dl id="iteam-home" class="frap sole">
		<dt>
			<a href="../" target="_blank">回到网站首页</a>
			<i class="ico"></i>
		</dt>
	</dl>
	<dl id="iteam-lantern">
		<dt>
			<a href="javascript:;">焦点横幅</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'lanternM') !==false)
				echo "class='on'"; ?> >
			<a href="lanternM.php" >横幅管理</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'lantern.php') !==false)
				echo "class='on'"; ?> >
			<a href="lantern.php">添加图片</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'lanternE') !==false)
						echo "class='on'"; ?> >
			<a href="lanternE.php">幻灯片编辑</a>
		</dd>
	</dl>
	<dl id="iteam-article">
		<dt >
			<a href="javascript:;">文章管理</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'article-add') !==false)
						echo "class='on'"; ?> >
			<a href="article-add.php">添加文章</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'article.php') !==false)
						echo "class='on'"; ?> >
			<a href="article.php">文章列表</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'category.php?type=article') !==false)
						echo "class='on'"; ?> >
			<a href="category.php?type=article">文章分类</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'category-add.php?type=article') !==false)
						echo "class='on'"; ?> >
			<a href="category-add.php?type=article">添加分类</a>
		</dd>
	</dl>
	<dl id="iteam-product">
		<dt >
			<a href="javascript:;">游戏产品管理</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'product-add') !==false)
						echo "class='on'"; ?> >
			<a href="product-add.php?imgurl=none">添加游戏</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'product.php') !==false)
						echo "class='on'"; ?> >
			<a href="product.php">游戏管理</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'category.php?type=product') !==false)
						echo "class='on'"; ?> >
			<a href="category.php?type=product">分类管理</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'category-add.php?type=product') !==false)
						echo "class='on'"; ?> >
			<a href="category-add.php?type=product">添加新分类</a>
		</dd>
	</dl>
	<dl>
		<dt >
			<a href="javascript:;">静态内容管理</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'company-option.php') !==false) echo "class='on'"; ?> >
			<a href="company-option.php">关于我们</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'background.php') !==false) echo "class='on'"; ?> >
			<a href="background.php">图片墙背景</a>
		</dd>
	</dl>
	<dl id="iteam-comments" class="frap" style="display:none">
		<dt >
			<a href="javascript:;">留言管理</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'comments') !==false)
						echo "class='on'"; ?> >
			<a href="comments.php">留言列表</a>
		</dd>
	</dl>
	<dl id="iteam-links" class="frap">
		<dt>
			<a href="javascript:;">友情链接</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'link') !==false)
						echo "class='on'"; ?> >
			<a href="link.php">链接列表</a>
		</dd>
	</dl>
	<dl id="iteam-users" class="frap" >
		<dt >

			<a href="javascript:;">用户管理</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'users') !==false)
						echo "class='on'"; ?> >
			<a href="users.php">用户列表</a>
		</dd>
		<dd style="display: none;" <?php if(strpos($_SERVER['REQUEST_URI'],'user-add') !==false)
						echo "class='on'"; ?> >
			<a href="user-add.php">添加用户</a>
		</dd>
	</dl>
	<dl id="iteam-website" class="frap">
		<dt >
			<a href="javascript:;">网站设置</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'option') !==false)
						echo "class='on'"; ?> >
			<a href="option.php">基本设置</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'option-seo') !==false)
						echo "class='on'"; ?> >
			<a href="option-seo.php">SEO设置</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'option-url') !==false)
						echo "class='on'"; ?> >
			<a href="option-url.php">URL重写</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'settings') !==false)
						echo "class='on'"; ?> >
			<a href="settings.php">变量管理</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'option-html') !==false)
						echo "class='on'"; ?> >
			<a href="option-html.php">生成HTML</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'templets') !==false)
						echo "class='on'"; ?> >
			<a href="templets.php">模板管理</a>
		</dd>
	</dl>
	<dl id="iteam-company">
		<dt>
			<a href="javascript:;">公司资料</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'company-option') !==false)
						echo "class='on'"; ?> >
			<a href="company-option.php">公司简介</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'company-contact') !==false)
						echo "class='on'"; ?> >
			<a href="company-contact.php">联系方式</a>
		</dd>
	</dl>
	<dl id="iteam-db" class="frap" style="display:none;">
		<dt>

			<a href="javascript:;">数据管理</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'dbbackup') !==false)
						echo "class='on'"; ?> >
			<a href="dbbackup.php">数据备份</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'dbrestore') !==false)
						echo "class='on'"; ?> >
			<a href="dbrestore.php">数据恢复</a>
		</dd>
	</dl>
	<dl id="iteam-keywords" class="frap" style="display:none;">
		<dt headerindex="8h">
			<a href="javascript:;">关键词管理</a>
			<i class="ico"></i>
		</dt>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'keywords') !==false)
						echo "class='on'"; ?> >
			<a href="keywords.php">关键词列表</a>
		</dd>
		<dd <?php if(strpos($_SERVER['REQUEST_URI'],'keyword-add') !==false)
						echo "class='on'"; ?> >
			<a href="keyword-add.php">添加关键词</a>
		</dd>
	</dl>
</div>

<?php
/*
require_once 'admin.inc.php';

$regularlist = GetRegular();

if(count($regularlist) >
0)
{
	foreach($regularlist as $regularinfo)
	{
		$subregularlist = GetRegular($regularinfo->rid);
		if(count($subregularlist) > 0)
		{
			echo "
<dt class=\"submenuheader\" style=\"cursor:pointer;\">
	<img src=\"../images/plus.gif\" alt=\"查看子分类\" title=\"查看子分类\"/>

	<a href=\"$regularinfo->value\">$regularinfo->name</a>
</dt>
";
			echo "
<dd>
	<div class=\"submenu\">
		";
			foreach($subregularlist as $subregular)
			{
				if($subregular->status == 'ok')
				{
					if(checkregular($subregular->rid))
					{
						echo "
		<li>
			<a href=\"$subregular->value\">$subregular->name</a>
		</li>
		";
					}
				}
			}
			echo "
	</div>
</dd>
";
		}
		else
		{
			echo "
<dt>
	<img src=\"../images/plus.gif\" />

	<a href=\"$regularinfo->value\">$regularinfo->name</a>
</dt>
";
		}
	}
}

function GetRegular($pid=0)
{
	global $yiqi_db;
	$sql = "SELECT * FROM yiqi_regular WHERE pid = '$pid' order by displayorder";
	return $yiqi_db->get_results(CheckSql($sql));
}
*/
?>
<script type="text/javascript" src="../images/ddaccordion.js"></script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "submenuheader",contentclass: "submenu",
	revealtype: "click",
	mouseoverdelay: 200,
	collapseprev: true, 
	defaultexpanded: [],
	onemustopen: false, 
	animatedefault: false,
	persiststate: true, 
	toggleclass: ["", ""],
	togglehtml: ["suffix", "", ""], 
	animatespeed: "fast", 
	oninit:function(headers, expandedindices){ 
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ 
		//do nothing
	}
});
</script>