<?php
	require_once 'admin.inc.php';

	$cid			= $_POST["cid"];
	$productthumb	=  "-";
	// $productthumb	= $_POST["productthumb"];
	$name			= $_POST["name"];

	//进入页面时获取url参数，如果没有cid=时，说明没有上传图片，可以执行下面的上传方法
	$url_cid = $_GET["cid"];
	$websiteurl = getset("siteurl")->value;

	// echo $url_cid;

	if ($url_cid) {
		$d = date("ymdhis");
		$img = $_FILES['img'];
		if ($img) {
			//文件存放目录，和本php文件同级
			$dir = dirname(dirname(__FILE__));
			$i = 0;
			foreach ($img['tmp_name'] as $value) {
				$filename = $img['name'][$i];
				if ($value) {
					$savepath = "{$dir}/uploads/bg/{$d}{$i}.jpg";
					// echo $savepath . '<br>';
					$state = move_uploaded_file($value, $savepath);
					if ($state) {
						$bg_url= "/uploads/bg/" . $d . $i . ".jpg" ;
						// echo "<img width='200' height='100' src='" . $websiteurl . $bg_url . "'>";
						// echo 
					}
				}
				$i++;
			}
		}
	} else {
		// echo "2";
	}

	// echo $cid . " / " . $productthumb . " / " . $name;

	// $sql = "update into yiqi_background (cid,name,src) values ('$cid','$name','$productthumb');";

	$sql = "update yiqi_background set `src`='$bg_url' where cid='$cid';";

	$result = $yiqi_db->query(CheckSql($sql));

	//----------------------读取数据----------------------
	// $action = $_POST["action"];
	
?>

<?php
$adminpagetitle = "背景墙管理";
include("admin.header.php");?>
<script type="text/javascript">
	$(function(){
		// $url = window.location.href;
		//构造方法获取url参数
		(function($){
			$.getUrlParam = function(name) {
				//构造一个含有目标参数的正则表达式对象
				var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
				//匹配目标参数
				var r = window.location.search.substr(1).match(reg);
				//返回参数值
				if (r!=null) return unescape(r[2]); return null;
			}
		})(jQuery);
		var $num = $.getUrlParam('cid');
		$("table form").eq($num - 1).show();
		// $("table .img").eq($num - 1).attr("src","<?php echo $websiteurl . $bg_url; ?>");
	});
</script>

<style type="text/css">
	.bg_td { text-align: center;}
	.bg_td a { color:red}
</style>
<script type="text/javascript">
	$(function(){
		$(".go").click(function(){
			$(this).parents(".bg_td").find("form").show();
		});
	});
</script>
<div class="main_body">
	<h3>图片背景管理：</h3>
	<table class="inputform" cellpadding="1" cellspacing="1">
		<tbody>
			<tr class="th">
				<td>编号</td>
				<td>作用页面</td>
				<td>图片</td>
				<td>状态</td>
				<td>操作</td>
			</tr>
			<tr id="1">
				<td>
					1
				</td>
				<td>产品
					
				</td>
				<td>
					<a href="<?php echo $websiteurl ?>/game.php" target="_blank">
						<?php
							$sql = "SELECT * FROM yiqi_background WHERE cid = 1";
						?>
						<img class="img" width="300" height="200" class="old_img" src="<?php echo $websiteurl . $yiqi_db->get_row(CheckSql($sql))->src;?>">
					</a>
				</td>
				<td colspan="2"  class="bg_td">
					<a href="<?php echo $websiteurl ?>/admin/background.php?cid=1#1" class="go">修改此背景</a>
					<form style="display:none;" accept="background.php" method="post" enctype="multipart/form-data">
						<input type="hidden" name="cid" class="cid" value="1">
						<input type="hidden" name="name" value="产品">
						<input type="file" name="img[]" />
						<input type="submit" class="up_ico_btn" name="" value="上传背景墙图" />
					</form>
				</td>
			</tr>
			<tr id="2">
				<td>2</td>
				<td>新闻
					<input type="hidden" name="name" value="新闻">
				</td>
				<td>
					<a href="<?php echo $websiteurl ?>/category/news/" target="_blank">
						<?php
							$sql = "SELECT * FROM yiqi_background WHERE cid = 2";
						?>
						<img class="img" width="300" height="200" class="old_img" src="<?php echo $websiteurl . $yiqi_db->get_row(CheckSql($sql))->src;?>">
					</a>
				</td>
				<td colspan="2"  class="bg_td">
					<a href="<?php echo $websiteurl ?>/admin/background.php?cid=2#2" class="go">修改此背景</a>
					<form style="display:none;" accept="background.php" method="post" enctype="multipart/form-data">
						<input type="hidden" name="cid" class="cid" value="2">
						<input type="hidden" name="name" value="产品">
						<input type="file" name="img[]" />
						<input type="submit" class="up_ico_btn" name="" value="上传背景墙图" />
					</form>
				</td>

			</tr>
			<tr id="3">
				<td>3</td>
				<td>关于
					<input type="hidden" name="name" value="关于">
				</td>
				<td>
					<a href="<?php echo $websiteurl ?>/about.php" target="_blank">
						<?php
							$sql = "SELECT * FROM yiqi_background WHERE cid = 3";
						?>
						<img class="img" width="300" height="200" class="old_img" src="<?php echo $websiteurl . $yiqi_db->get_row(CheckSql($sql))->src;?>">
					</a>
				</td>
				<td colspan="2"  class="bg_td">
					<a href="<?php echo $websiteurl ?>/admin/background.php?cid=3#3" class="go">修改此背景</a>
					<form style="display:none;" accept="background.php" method="post" enctype="multipart/form-data">
						<input type="hidden" name="cid" class="cid" value="3">
						<input type="hidden" name="name" value="产品">
						<input type="file" name="img[]" />
						<input type="submit" class="up_ico_btn" name="" value="上传背景墙图" />
					</form>
				</td>

			</tr>
			<tr id="4">
				<td>4</td>
				<td>活动<input type="hidden" name="name" value="活动"></td>
				<td>
					<a href="<?php echo $websiteurl ?>/category/activity/" target="_blank">
						<?php
							$sql = "SELECT * FROM yiqi_background WHERE cid = 4";
						?>
						<img class="img" width="300" height="200" class="old_img" src="<?php echo $websiteurl . $yiqi_db->get_row(CheckSql($sql))->src;?>">
					</a>
				</td>
				<td colspan="2"  class="bg_td">
					<a href="<?php echo $websiteurl ?>/admin/background.php?cid=4#4" class="go">修改此背景</a>
					<form style="display:none;" accept="background.php" method="post" enctype="multipart/form-data">
						<input type="hidden" name="cid" class="cid" value="4">
						<input type="hidden" name="name" value="产品">
						<input type="file" name="img[]" />
						<input type="submit" class="up_ico_btn" name="" value="上传背景墙图" />
					</form>
				</td>

			</tr>
			<tr id="5">
				<td>5</td>
				<td>招聘<input type="hidden" name="name" value="招聘"></td>
				<td>
					<a href="<?php echo $websiteurl ?>/category/jobs/" target="_blank">
						<?php
							$sql = "SELECT * FROM yiqi_background WHERE cid = 5";
						?>
						<img class="img" width="300" height="200" class="old_img" src="<?php echo $websiteurl . $yiqi_db->get_row(CheckSql($sql))->src;?>">
					</a>
				</td>
				<td colspan="2"  class="bg_td">
					<a href="<?php echo $websiteurl ?>/admin/background.php?cid=5#5" class="go">修改此背景</a>
					<form style="display:none;" accept="background.php" method="post" enctype="multipart/form-data">
						<input type="hidden" name="cid" class="cid" value="5">
						<input type="hidden" name="name" value="产品">
						<input type="file" name="img[]" />
						<input type="submit" class="up_ico_btn" name="" value="上传背景墙图" />
					</form>
				</td>

			</tr>
		</tbody>
	</table>
</div>

<!-- END -->
</div>

<?php include("admin.footer.php");?></div>

</body>

</html>