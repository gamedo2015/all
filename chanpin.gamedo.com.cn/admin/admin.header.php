<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $adminpagetitle;?></title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link href="../images/style.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<!-- js -->
<script type="text/javascript" src="../images/jq.js" ></script>
<!-- <script type="text/javascript" src="js/jquery-1.8.0.min.js" ></script> -->
<script type="text/javascript" src="js/admin.min.js" ></script>
<!--  -->
<script type="text/javascript" src="../images/yiqimenu.js" ></script>
<script type="text/javascript" src="../images/load.js" ></script>
<script type="text/javascript" src="../images/jquery.form.js"></script>
<!-- js END -->
</head>
<?php $websiteurl = getset("siteurl")->value; ?>
<body>

<div class="wrap admin_page">
	<div class="header fn_clear">
		<div class="fn_left">
			<a href="http://ganline.com" target="_blank" class="logo">
				<img src="img/admin-logo.gif" alt="LINECMS 橄榄传媒网站内容管理系统">
			</a>
			<i>
				<?php echo $adminuserinfo->username;?>，欢迎使用 LINECMS
			</i>
		</div>
		<div class="fn_right admin_btn">
			<a class="a1" href="cleancache.php">清除缓存</a><a class="a2" href="logout.php">注销</a>
		</div>
	</div>
	<div class="admin_nav">
		当前位置：<a href="index.php">后台</a> > <?php echo $adminpagetitle;?>
	</div>
	<div class="main admin_content">
		<div class="admin_menu" id="main_nav">
			<?php include("menu.php"); ?>
		</div>
