<?php
require_once 'data.class.php';

//自定义模块
class Navigate
{	
	//TakeNavigateList
	function TakeLanPic()
	{
		global $yiqi_db;
		return $yiqi_db->get_results(CheckSql("select * from yiqi_lantern where status = 'ok'"));
	}
	

	//获得背景图
	function TakePageBg()
	{
		global $yiqi_db;
		return $yiqi_db->get_results(CheckSql("select * from yiqi_background"));
	}

	//抓取首页热门商品
	function TakeHot($cid=0,$skip=0,$take=10,$orderby="adddate desc",$all=false)
	{
	    global $yiqi_db;
	    $categorydata = new Category();	    
    	$exist = $categorydata->ExistCategory($cid);
		if($exist == 1)
		{
			$cids = array($cid);
			$cids = array_merge($cids,$categorydata->GetSubCategoryIDs($cid));			
			$cids = implode(',', $cids);
			if($all)
			{
				return $yiqi_db->get_results(CheckSql("select * from yiqi_product where cid in ($cids) order by $orderby limit $skip,$take "));
			}
			else
			{
				return $yiqi_db->get_results(CheckSql(sprintf("select * from yiqi_product where cid in ($cids) and adddate <= '%s' order by $orderby limit $skip,$take ",date("Y-m-d H:i:s"))));
			}
		}
		else
		{
			if($all)
			{
				return $yiqi_db->get_results(CheckSql("select * from yiqi_product order by $orderby limit $skip,$take"));
			}
			else
			{
				//抓取status状态等于ok的商品
				return $yiqi_db->get_results(CheckSql(sprintf("select * from yiqi_product where adddate <= '%s' and status='ok' and hot='ok' order by $orderby limit $skip,$take",date("Y-m-d H:i:s"))));
			}
		}
	}

	//抓首页热门新闻
	    function TakeHotArticle($name="",$skip=0,$take=10,$orderby="adddate desc",$all=false)
		{
		    global $yiqi_db;
		    $categorydata = new Category();	    
	    	$exist = $categorydata->ExistFilename($name);
			if($exist == 1)
			{
			    $category = $categorydata->GetCategoryByName($name);
				$cids = array($category->cid);
				$cids = array_merge($cids,$categorydata->GetSubCategoryIDs($category->cid));			
				$cids = implode(',', $cids);
				if($all)
				{
					return $yiqi_db->get_results(CheckSql("select * from yiqi_article where cid in ($cids) order by $orderby limit $skip,$take "));
				}
				else
				{
					return $yiqi_db->get_results(CheckSql(sprintf("select * from yiqi_article where cid in ($cids) and adddate <= '%s' and hot='ok' order by $orderby limit $skip,$take ",date("Y-m-d H:i:s"))));
				}
			}
		}
		
}
?>