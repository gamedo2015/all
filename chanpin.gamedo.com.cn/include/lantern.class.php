<?php
require_once("data.class.php");

class Lantern
{
	function GetLantern($nid=0)
	{
		global $yiqi_db;
		if($this->ExistLantern($nid)==1)
		{
			$sql = "select * from yiqi_lantern where kid = '$nid' limit 1";
			return $yiqi_db->get_row(CheckSql($sql));
		}
		else
		{
			return null;
		}
	}
	
	function ExistLantern($nid)
	{
		global $yiqi_db;
		$sql = "select * from yiqi_lantern where kid = '$nid' limit 1";	    
		$exist = $yiqi_db->query(CheckSql($sql));
		if($exist == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}	    
	}
	
	function GetLanternList()
	{
		global $yiqi_db;
		return $yiqi_db->get_results(CheckSql("select * from yiqi_lantern order by displayorder asc"));
	}
	
	function TakeLanternList($skip=0,$take=10)
	{
		global $yiqi_db;
		return $yiqi_db->get_results(CheckSql("select * from yiqi_lantern order by displayorder asc limit $skip,$take"));		
	}
}
?>