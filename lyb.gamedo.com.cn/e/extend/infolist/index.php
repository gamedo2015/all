<?php
require('../../class/connect.php');        //引入数据库配置文件和公共函数文件
require('../../class/db_sql.php');        //引入数据库操作文件
require('../../data/dbcache/class.php');	//引入栏目缓存文件
require '../'.LoadLang("pub/fun.php");	//引入分页语言包文件
$link=db_connect();                //连接MYSQL
$empire=new mysqlquery();        //声明数据库操作类
$editor=1; //声明目录层次

//-------- 分页参数 --------
$page=(int)$_GET['page'];
$start=0;
$line=10;	//每页显示记录数
$page_line=8;	//每页显示分页链接数
$offset=$page*$line;	//总偏移量

//-------- 查询SQL --------
//取得信息总数
$totalquery="select count(*) as total from {$dbtbpre}ecms_info where checked=1";
$num=$empire->gettotal($totalquery);
//select查询SQL
$query="select * from {$dbtbpre}ecms_info where checked=1";
$query.=" order by newstime desc limit $offset,$line";
$sql=$empire->query($query);

$listpage=page1($num,$line,$page_line,$start,$page,$search);//取得分页导航
require('template/index.temp.php'); //导入模板文件

db_close();                        //关闭MYSQL链接
$empire=null;                        //注消操作类变量
?>