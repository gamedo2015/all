<?php
@include("../../inc/header.php");

/*
		SoftName : EmpireBak
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

E_D("DROP TABLE IF EXISTS `phome_ecms_news`;");
E_C("CREATE TABLE `phome_ecms_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ttid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `onclick` int(10) unsigned NOT NULL DEFAULT '0',
  `plnum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `totaldown` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newspath` char(20) NOT NULL DEFAULT '',
  `filename` char(36) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` char(20) NOT NULL DEFAULT '',
  `firsttitle` tinyint(1) NOT NULL DEFAULT '0',
  `isgood` tinyint(1) NOT NULL DEFAULT '0',
  `ispic` tinyint(1) NOT NULL DEFAULT '0',
  `istop` tinyint(1) NOT NULL DEFAULT '0',
  `isqf` tinyint(1) NOT NULL DEFAULT '0',
  `ismember` tinyint(1) NOT NULL DEFAULT '0',
  `isurl` tinyint(1) NOT NULL DEFAULT '0',
  `truetime` int(10) unsigned NOT NULL DEFAULT '0',
  `lastdotime` int(10) unsigned NOT NULL DEFAULT '0',
  `havehtml` tinyint(1) NOT NULL DEFAULT '0',
  `groupid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userfen` smallint(5) unsigned NOT NULL DEFAULT '0',
  `titlefont` char(14) NOT NULL DEFAULT '',
  `titleurl` char(200) NOT NULL DEFAULT '',
  `stb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fstb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `restb` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `keyboard` char(80) NOT NULL DEFAULT '',
  `title` char(100) NOT NULL DEFAULT '',
  `newstime` int(10) unsigned NOT NULL DEFAULT '0',
  `titlepic` char(120) NOT NULL DEFAULT '',
  `ftitle` char(120) NOT NULL DEFAULT '',
  `smalltext` char(255) NOT NULL DEFAULT '',
  `diggtop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `newstime` (`newstime`),
  KEY `ttid` (`ttid`),
  KEY `firsttitle` (`firsttitle`),
  KEY `isgood` (`isgood`),
  KEY `ispic` (`ispic`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8");
E_D("replace into `phome_ecms_news` values('6','5','0','33','0','0','','6','1','admin','0','0','0','0','0','0','0','1416968943','1416969843','1','0','0','','/pc/cms/e/action/ShowInfo.php?classid=5&id=6','1','1','1','','《琅琊榜》将改变古装剧格局获360无死角点赞','1416968908','','《琅琊榜》将改变古装剧格局 获360无死角点赞','近日，改编自海宴同名人气小说的新剧《琅琊榜》公布了最新制作的21分钟版情节长片花。这部胡歌、刘涛、王凯、黄维德等领衔出演，由导演孔笙、李雪及制片人侯鸿亮带领的金牌制作','0');");
E_D("replace into `phome_ecms_news` values('5','2','0','50','0','0','','5','1','admin','0','0','0','0','0','0','0','1416968065','1416969813','1','0','0','','/pc/cms/e/action/ShowInfo.php?classid=2&id=5','1','1','1','','《琅琊榜》亮相戛纳电视节海外发行全面开启','1416967027','','《琅琊榜》亮相戛纳电视节 海外发行全面开启','10月13日，作为国际视听产品交易盛会的法国戛纳秋季电视节如期开幕，由孔笙、李雪合力执导，侯鸿亮制片，同名人气小说作者海宴亲任编剧，胡歌、刘涛、王凯、黄维德等实力明星联袂出演','0');");

@include("../../inc/footer.php");
?>