﻿(function($){
$.fn.extend({
	Scroll:function(opt,callback){
		// 参数初始化
		if(!opt) var opt={};
		var _btnUp = $("#next");//Shawphy:向上按钮
		var _btnDown = $("#prev");//Shawphy:向下按钮
		var timerID;
		var _this=this.eq(0).find("ul:first");
		var itemW=_this.find("li:first").width(), //获取列表项宽度
		line=opt.line?parseInt(opt.line,10):parseInt(this.width()/itemW,10), //每次滚动的行数，默认为一屏，即父容器高度
		speed=opt.speed?parseInt(opt.speed,1000):3000; //卷动速度，数值越大，速度越慢（毫秒）
		timer=opt.timer //?parseInt(opt.timer,10):3000; //滚动的时间间隔（毫秒）
		line = 1;
		if(line==0) line=1;
		//alert(line);
		var upHeight=0-line*itemW;
		
		// 滚动函数
		//alert(this.width()/itemW)
		var scrollUp=function(){
			_btnUp.unbind("click",scrollUp); //Shawphy:取消向上按钮的函数绑定
			_this.animate({
			marginLeft:upHeight
			},speed,function(){
				for(i=1;i<=line;i++){
				_this.find("li:first").appendTo(_this);
				}
				_this.css({marginLeft:0});
				_btnUp.bind("click",scrollUp); //Shawphy:绑定向上按钮的点击事件
			});
		}
		//Shawphy:向下翻页函数
		var scrollDown=function(){
			_btnDown.unbind("click",scrollDown);
			for(i=1;i<=line;i++){
				_this.find("li:last").show().prependTo(_this);
			}
			_this.css({marginLeft:upHeight});
			_this.animate({
			marginLeft:0
			},speed,function(){
			_btnDown.bind("click",scrollDown);
			});
		}
		//Shawphy: 自动播放
		var autoPlay = function(){
			if(timer)timerID = window.setInterval(scrollUp,timer);
		};
		var autoStop = function(){
			if(timer)window.clearInterval(timerID);
		};
		
		// 鼠标事件绑定
		_this.hover(autoStop,autoPlay).mouseout();
		_btnUp.click(scrollUp).hover(autoStop,autoPlay);//Shawphy:向上向下鼠标事件绑定
		_btnDown.click(scrollDown).hover(autoStop,autoPlay);
	}     
})
})(jQuery);