$(function(){
	$('.downBox span').css('opacity', 0).hover(function(){
		$(this).css('height',220).stop().fadeTo(400, 1);
	},function(){
		$(this).css('height',67).stop().fadeTo(350, 0);
	});
	
	$(".newsTab a").hover(function(){
		var n = $(this).attr("tip");
		$(".newsTab a").removeClass("hover");
		$(this).addClass("hover");
		$(".newsView").hide();
		$(".newsView").eq(n).fadeTo(500,1);
	});
	
	$(".mediaList").hover(function(){
		$(".mediaView").fadeTo(400, 1);							   
	},function(){
		$(".mediaView").fadeTo(350, 0);		
	});
	
	$(".sizeZoom a").click(function(){
		$(this).addClass("red").siblings().removeClass("red");
	});
});

function sizeZoom(size){
	$(".newsFont").css({"font-size":size+"px","line-height":(size*2-2) + "px"});
};
