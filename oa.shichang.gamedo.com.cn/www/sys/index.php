<?php 
/**
 * The sys app router file of RanZhi.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv11.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     RanZhi
 * @version     $Id: index.php 2748 2015-04-17 05:53:13Z daitingting $
 * @link        http://www.ranzhico.com
 */
$appName = 'sys';
include '../../framework/loader.php';
