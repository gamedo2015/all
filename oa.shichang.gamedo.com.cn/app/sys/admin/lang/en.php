<?php
/**
 * The English file of admin module of RanZhi.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv11.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     admin 
 * @version     $Id: en.php 2508 2015-01-26 08:32:52Z chujilu $
 * @link        http://www.ranzhico.com
 */
$lang->admin->shortcuts = new stdclass();

$lang->admin->shortcuts->createUser  = 'User';
$lang->admin->shortcuts->company     = 'Company';
$lang->admin->shortcuts->createEntry = 'Application';
