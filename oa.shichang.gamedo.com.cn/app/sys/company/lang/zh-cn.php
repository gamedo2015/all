<?php
/**
 * The zh-cn file of company module of RanZhi.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv11.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     company 
 * @version     $Id: zh-cn.php 2508 2015-01-26 08:32:52Z chujilu $
 * @link        http://www.ranzhico.com
 */
$lang->company->common  = "公司";

$lang->company->name    = "公司名称";
$lang->company->desc    = "公司简介";
$lang->company->content = "公司介绍";

$lang->company->setBasic = "设置基本信息";
