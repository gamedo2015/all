<?php
/**
 * The view file of task module of RanZhi.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv11.html)
 * @author      Yidong Wang <yidong@cnezsoft.com>
 * @package     task
 * @version     $Id$
 * @link        http://www.ranzhico.com
 */
?>
<?php include $app->getModuleRoot() . 'common/view/header.html.php'; ?>
<?php if ($mode) js::set('mode', $mode); ?>

<?php $this->loadModel('project')->setMenu($projects, $projectID, "$orderBy"); ?>

<li id='bysearchTab'>
    <?php echo html::a('#', "<i class='icon-search icon'></i>" . $lang->search->common) ?>

</li>



<?php if ($kkk !== 'my') { ?>

    <div class='row with-menu page-content'>
        <div class='panel'>
            <table class='table table-hover table-striped tablesorter table-data' id='taskList'>
                <thead>
                    <tr class='text-center'>
                        <?php $vars = "projectID=$projectID&mode={$mode}&orderBy=%s&recTotal={$pager->recTotal}&recPerPage={$pager->recPerPage}&pageID={$pager->pageID}"; ?>
                        <th class='w-60px'><?php commonModel::printOrderLink('id', $orderBy, $vars, $lang->task->id); ?></th>
                        <th class='w-40px'> <?php commonModel::printOrderLink('pri', $orderBy, $vars, $lang->task->lblPri); ?></th>
                        <th>                <?php commonModel::printOrderLink('name', $orderBy, $vars, $lang->task->name); ?></th>
                        <th class='w-100px'><?php commonModel::printOrderLink('deadline', $orderBy, $vars, $lang->task->deadline); ?></th>
                        <th class='w-80px'> <?php commonModel::printOrderLink('assignedTo', $orderBy, $vars, $lang->task->assignedTo); ?></th>
                        <th class='w-90px'> <?php commonModel::printOrderLink('status', $orderBy, $vars, $lang->task->status); ?></th>
                        <th class='w-100px visible-lg'><?php commonModel::printOrderLink('createdDate', $orderBy, $vars, $lang->task->createdDate); ?></th>
                        <th class='w-90px visible-lg'> <?php commonModel::printOrderLink('consumed', $orderBy, $vars, $lang->task->consumedAB . $lang->task->lblHour); ?></th>
                        <th class='w-110px visible-lg'> <?php commonModel::printOrderLink('left', $orderBy, $vars, $lang->task->left . $lang->task->lblHour); ?></th>
                        <th><a href='?m=task&f=browse&projectID=<?php echo $projectID; ?>&mode=date'>日历</a></th>
                        <th><?php echo $lang->actions; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tasks as $task): ?>
                        <tr class='text-center' data-url='<?php echo $this->createLink('task', 'view', "taskID=$task->id"); ?>'>
                            <td><?php echo $task->id; ?></td>
                            <td><span class='active pri pri-<?php echo $task->pri; ?>'><?php echo $lang->task->priList[$task->pri]; ?></span></td>
                            <td class='text-left'><?php echo $task->name; ?></td>
                            <td><?php echo $task->deadline; ?></td>
                            <td><?php if (isset($users[$task->assignedTo])) echo $users[$task->assignedTo]; ?></td>
                            <td><?php echo zget($lang->task->statusList, $task->status); ?></td>
                            <td class='visible-lg'><?php echo substr($task->createdDate, 0, 10); ?></td>
                            <td class='visible-lg'><?php echo $task->consumed; ?></td>
                            <td class='visible-lg'><?php echo $task->left; ?></td>
                            <td><?php $this->task->buildOperateMenu($task); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot><tr><td colspan='10'><?php $pager->show(); ?></td></tr></tfoot>
            </table>
        </div>
    </div>
<?php }else { ?>

    <div class='row with-menu page-content'>
        <div class='panel'>
            <table border="1" width="100%"  cellspacing="0px"  align="center" >
                <tr>
                    <td  align="center" colspan="<?php echo $tianshu + 1; ?>">
                        <div style="text-align:center;font-size:20px">
                            <a href ="?m=task&f=browse&projectID=<?php echo $projectID; ?>&mode=date&nian=<?php echo $nian - 1; ?>&yue=<?php echo $yue; ?>">上一年</a>
                            <a href ="?m=task&f=browse&projectID=<?php echo $projectID; ?>&mode=date&nian=<?php echo $nian + 1; ?>&yue=<?php echo $yue; ?>">下一年</a>
                            &nbsp;&nbsp;&nbsp;
                            当前时间:<?php echo $nian . "年" . $yue . "月" . $ri . '日' . $hour . ':' . $min . ':' . $miao; ?>&nbsp;&nbsp;&nbsp;
                            <a href ="?m=task&f=browse&projectID=<?php echo $projectID; ?>&mode=date&nian=<?php echo $nian; ?>&yue=<?php
                            if ($yue - 1 <= 0) {
                                echo '12';
                            } else {
                                echo $yue - 1;
                            }
                            ?>">
                                   <?php
                                   if ($yue - 1 <= 0) {
                                       echo '<' . '12';
                                   } else {
                                       echo '<' . ($yue - 1);
                                   }
                                   ?>月
                            </a>

                            <a href ="?m=task&f=browse&projectID=<?php echo $projectID; ?>&mode=date&nian=<?php echo $nian; ?>&yue=<?php
                            if ($yue + 1 > 12) {
                                echo '1';
                            } else {
                                echo $yue + 1;
                            }
                            ?>">
                                   <?php
                                   if ($yue + 1 > 12) {
                                       echo '1';
                                   } else {
                                       echo $yue + 1;
                                   }
                                   ?>月&gt;
                            </a>
                        </div>
                        <div >
                            项目成员:<?php
                                   foreach ($taskUsers as $taskUser) {
                                       echo $taskUser->realname . ',';
                                   }
                                   ?>
                        </div>
                    </td>
                </tr>


                <tr>
                    <td align="center" >执行人</td>
                    <?php
                    $week = array(
                        "0" => "(日)",
                        "1" => "(一)",
                        "2" => "(二)",
                        "3" => "(三)",
                        "4" => "(四)",
                        "5" => "(五)",
                        "6" => "(六)"
                    );
                    $i = 1;
                    for ($i = 1; $i <= $tianshu; $i++) {
                        ?>
                        <?php $w = $nian . '-' . $yue . '-' . $i;
                        $num = date('w', strtotime($w)); ?>
                        <td align="center" <?php if ($num == 0 || $num == 6) {
                            echo "style='background-color:#71AFF7'";
                        } ?> >

                    <?php echo $i . '<br>' . $week[$num]; ?>
                        </td>  
                <?php } ?>
                </tr>

                <?php
                foreach ($taskLists as $taskList) {
                    $begin = date('Y-m-d', strtotime($taskList->createdDate));
                    $end = date('Y-m-d', strtotime($taskList->finishedDate));
                    $oa_end = date('Y-m-d', strtotime($taskList->deadline));

                    $begin_m = date('m', strtotime($taskList->createdDate));
                    $end_m = date('m', strtotime($taskList->finishedDate));
                    $oa_m = date('m', strtotime($taskList->deadline));


                    $begin_d = date('d', strtotime($taskList->createdDate));
                    $end_d = date('d', strtotime($taskList->finishedDate));
                    $oa_d = date('d', strtotime($taskList->deadline));


                    if ($begin_d['0'] == 0) {
                        $begin_d['0'] = ' ';
                    }
                    if ($end_d ['0'] == 0) {
                        $end_d ['0'] = ' ';
                    }
                    $begin_d = (int) $begin_d;
                    $end_d = (int) $end_d;

                    if ($taskList->finishedBy == '') {
                        ?>
                        <tr>
                            <td align="center" >|进行中|<?php echo '<span style=font-weight:bold>' . $taskList->assignedTo . '</span>' . '<br>' . '任务名称:' . $taskList->name . '<a href=/oa/www/oa/index.php?m=task&f=view&taskID=' . $taskList->id . '>[详细] </a>' . '<br>' . '开始-截止:' . $begin . '~' . $taskList->deadline; ?></td>


                            <?php
                            if ($oa_m == $begin_m) {

                                for ($k = 1; $k <= $tianshu; $k++) {
                                    ?>
                                    <td align="center"  <?php if ($k >= $begin_d && $k <= $oa_d) {
                        echo "style='background-color:#B5B5B5'";
                    } ?>><?php echo $k; ?></td>
                                    <?php
                                }
                            } else {
                                if($yue == $begin_m){
                                    for ($k = 1; $k <= $tianshu; $k++) {
                                ?>        
                               <td align="center"  <?php if ($k >= $begin_d) {
                        echo "style='background-color:#B5B5B5'";
                    } ?>><?php echo $k; ?></td>         
                                    
                              <?php      }
                                    
                                }else if ($yue == $oa_m){
                                   for ($k = 1; $k <= $tianshu; $k++) {
                              ?>      
                               <td align="center"  <?php if ($k <= $oa_d) {
                        echo "style='background-color:#B5B5B5'";
                    } ?>><?php echo $k; ?></td>
                               
                                <?php  
                                   }   
                                }else if($yue <$oa_m){
                                    for ($k = 1; $k <= $tianshu; $k++) {
                              ?>  
                                <td align="center"  
                       style='background-color:#B5B5B5'
                    ><?php echo $k; ?></td>
                               
                              <?php          
                                    } 
             
                                }
                           
                        }
                        ?>   
                        </tr> 
                            <?php
                        } else {
                            ?>
                        <tr>
                            <td align="center" >|完成|  <?php echo '<span style=font-weight:bold>' . $taskList->realname . '</span>' . '<br>' . '任务名称:' . $taskList->name . '<a href=/oa/www/oa/index.php?m=task&f=view&taskID=' . $taskList->id . '>[详细] </a>' . '<br>' . '开始-截止:' . $begin . '~' . $taskList->deadline;
                            ; ?></td>
                            <?php if ($begin == $end) { ?>
                                <!------一天完成了------>
                <?php for ($k = 1; $k <= $tianshu; $k++) { ?>
                                    <td align="center"  <?php if ($k == $begin_d) {
                        echo "style='background-color:red'";
                    } ?>><?php echo $k; ?></td>
                                <?php } ?>

                            <?php } else { ?>

                                <?php
                                if ($begin_m == $end_m) {

                                    for ($k = 1; $k <= $tianshu; $k++) {
                                        ?>       

                                        <td align="center"  <?php if ($k >= $begin_d && $k <= $end_d) {
                            echo "style='background-color:#AFD367'";
                        } ?>><?php echo $k; ?></td>

                                        <?php
                                    }
                                } else {
                                    if ($yue == $begin_m) {
                                        for ($k = 1; $k <= $tianshu; $k++) {
                                            ?>   

                                            <td align="center"  <?php if ($k >= $begin_d) {
                                echo "style='background-color:#AFD367'";
                            } ?>><?php echo $k; ?></td>

                                    <?php
                                    }
                                }else if($yue == $end_m){
                                    
                                    for($k = 1; $k <= $tianshu; $k++){
                                        ?>
                                   
                   <td align="center"  <?php if ($k <= $end_m) {
                        echo "style='background-color:#AFD367'";
                            } ?>><?php echo $k; ?></td>
                                   
                   <?php
                                    }
                                      
                                    
                                }else if($yue < $end_m){
                                    
                                 for($k = 1; $k <= $tianshu; $k++){
                                     
                                    ?> 
                              <td align="center" style='background-color:#AFD367'> <?php echo $k; ?></td>      
                                     
                        <?php             
                                 }   
                                    
                                    
                                }
                           }
                ?>
            <?php } ?>
                        </tr> 

            <?php
        }
    }
    ?>
            </table>
        </div>
    </div>
<?php } ?>
<?php include $app->getModuleRoot() . 'common/view/footer.html.php'; ?>
