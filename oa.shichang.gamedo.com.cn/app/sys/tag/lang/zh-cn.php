<?php
/**
 * The tag module zh-cn file of RanZhi.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv11.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     tag
 * @version     $Id: zh-cn.php 2508 2015-01-26 08:32:52Z chujilu $
 * @link        http://www.ranzhico.com
 */
if(!isset($lang->tag)) $lang->tag = new stdclass();
$lang->tag->common = '关键词';
$lang->tag->rank   = '权重';
$lang->tag->link   = '链接';
$lang->tag->search = '搜索';

$lang->tag->admin     = '关键词管理';
$lang->tag->editLink  = '编辑链接';
$lang->tag->inputLink = '请输入链接';
