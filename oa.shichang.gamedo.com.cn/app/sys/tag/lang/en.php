<?php
/**
 * The tag module en file of RanZhi.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv11.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     tag
 * @version     $Id: en.php 2508 2015-01-26 08:32:52Z chujilu $
 * @link        http://www.ranzhico.com
 */
if(!isset($lang->tag)) $lang->tag = new stdclass();
$lang->tag->common = 'keywords';
$lang->tag->rank   = 'Rank';
$lang->tag->link   = 'link';
$lang->tag->search = 'Search';

$lang->tag->admin     = 'Manage keywords';
$lang->tag->editLink  = 'Edit Link';
$lang->tag->inputLink = 'Please input link';
