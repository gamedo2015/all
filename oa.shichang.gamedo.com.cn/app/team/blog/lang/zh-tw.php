<?php
/**
 * The article category zh-tw file of RanZhi.
 *
 * @copyright   Copyright 2009-2015 青島易軟天創網絡科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv11.html)
 * @author      Xiying Guan <guanxiying@xirangit.com>
 * @package     blog
 * @version     $Id: zh-tw.php 2508 2015-01-26 08:32:52Z chujilu $
 * @link        http://www.ranzhico.com
 */
$lang->blog->common = "博客";

$lang->blog->latestArticles = "最新博文";

$lang->blog->index  = "博客首頁";
$lang->blog->create = "添加博客";
$lang->blog->edit   = "編輯博客";
$lang->blog->view   = "查看博客";
$lang->blog->delete = "刪除博客";
