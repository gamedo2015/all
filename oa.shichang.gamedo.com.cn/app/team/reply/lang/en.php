<?php
/**
 * The reply module zh-cn file of RanZhi.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv11.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     reply
 * @version     $Id: en.php 2508 2015-01-26 08:32:52Z chujilu $
 * @link        http://www.ranzhico.com
 */
$lang->reply = new stdclass();
$lang->reply->common      = 'Reply';
$lang->reply->id          = 'Id';
$lang->reply->list        = 'Replies';
$lang->reply->content     = 'Content';
$lang->reply->author      = 'Author';
$lang->reply->files       = 'Files:';
$lang->reply->createdDate = 'Date';
$lang->reply->admin       = 'Reply';

$lang->reply->edit = 'Edit';
