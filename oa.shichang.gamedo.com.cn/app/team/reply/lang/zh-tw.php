<?php
/**
 * The reply module zh-tw file of RanZhi.
 *
 * @copyright   Copyright 2009-2015 青島易軟天創網絡科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv11.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     reply
 * @version     $Id: zh-tw.php 2508 2015-01-26 08:32:52Z chujilu $
 * @link        http://www.ranzhico.com
 */
$lang->reply = new stdclass();
$lang->reply->common      = '回貼';
$lang->reply->id          = '編號';
$lang->reply->list        = '回帖列表';
$lang->reply->content     = '內容';
$lang->reply->author      = '作者';
$lang->reply->files       = '附件：';
$lang->reply->createdDate = '回覆時間';
$lang->reply->admin       = '回帖列表';

$lang->reply->edit = '編輯回帖';
